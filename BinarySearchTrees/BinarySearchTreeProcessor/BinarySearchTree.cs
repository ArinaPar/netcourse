using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace BinarySearchTreeProcessor
{
    public abstract class BinarySearchTree<TItem> : ISerializable, IXmlSerializable, IEnumerable<TItem> where TItem : IComparable<TItem>
    {
        [XmlElement(ElementName = "Node")]
        public Node<TItem>? Root { get; private set; }
        
        public bool IsEmpty => Root == null;

        protected BinarySearchTree()
        {

        }

        protected BinarySearchTree(IEnumerable<TItem> collection)
        {
            AddRange(collection);
        }

        protected BinarySearchTree(SerializationInfo info, StreamingContext context)
        {
            Root = (Node<TItem>?)info.GetValue("Root", typeof(Node<TItem>));
        }

        public abstract IEnumerator<TItem> GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(TItem data)
        {
            if (IsEmpty)
            {
                Root = new Node<TItem>(data);
            }
            else
            {
                Node<TItem> parent = SeekParent(data, Root);
                switch (Compare(data, parent))
                {
                    case -1:
                        parent.LeftChild = new Node<TItem>(data);
                        break;
                    case 1:
                        parent.RightChild = new Node<TItem>(data);
                        break;
                    default:
                        throw new InvalidOperationException();
                }
            }
        }

        public void AddRange(IEnumerable<TItem> data)
        {
            foreach (var d in data)
                Add(d);
        }

        public Node<TItem>? Find(TItem data)
        {
            return Find(data, out _);
        }

        public TItem GetMax()
        {
            if (IsEmpty)
                throw new ArgumentOutOfRangeException();

            return SeekMax(Root).Data;
        }

        public TItem GetMin()
        {
            if (IsEmpty)
                throw new ArgumentOutOfRangeException();

            return SeekMin(Root).Data;
        }

        public bool Remove(TItem data)
        {
            if (!IsEmpty)
            {
                if (TryFind(data, out Node<TItem>? nodeToRemove, out Node<TItem>? parent))
                {
                    if (nodeToRemove.LeftChild == null ^ nodeToRemove.RightChild == null)
                    {
                        RemoveNodeHavingOneChild(nodeToRemove, parent);
                    }
                    else if (nodeToRemove.LeftChild == null)
                    {
                        RemoveNodeHavingNoChildren(nodeToRemove, parent);
                    }
                    else
                    {
                        RemoveNodeHavingBothChildren(nodeToRemove, parent);
                    }

                    return true;
                }
            }

            return false;
        }

        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Root", Root);
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            using (var enumerator = GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    sb.Append(enumerator.Current);
                    sb.Append(Environment.NewLine);
                }

                if (sb.Length > 0)
                {
                    sb.Length -= 2;
                }
            }

            return sb.ToString();
        }

        protected abstract Node<TItem> SeekParent(TItem data, Node<TItem>? current);

        protected abstract bool TryFind(TItem data, out Node<TItem>? result, out Node<TItem>? parent);

        protected Node<TItem>? Find(TItem data, out Node<TItem>? parent)
        {
            parent = null;
            if (!IsEmpty && TryFind(data, out Node<TItem>? result, out parent))
            {
                return result;
            }

            throw new ArgumentOutOfRangeException(nameof(data));
        }

        protected int Compare(TItem data, Node<TItem> node)
        {
            return data.CompareTo(node.Data);
        }

        private Node<TItem> SeekMax(Node<TItem> node)
        {
            return node.RightChild == null ? node : SeekMax(node.RightChild);
        }

        private Node<TItem>? SeekMin(Node<TItem>? node)
        {
            return node?.LeftChild == null ? node : SeekMin(node.LeftChild);
        }

        private void RemoveNodeHavingNoChildren(Node<TItem> nodeToRemove, Node<TItem>? parent)
        {
            if (parent != null)
            {
                if (nodeToRemove.Equals(parent.LeftChild))
                {
                    parent.LeftChild = null;
                }
                else
                {
                    parent.RightChild = null;
                }
            }
            else
            {
                Root = null;
            }
        }

        private void RemoveNodeHavingOneChild(Node<TItem>? nodeToRemove, Node<TItem>? parent)
        {
            if (parent != null)
            {
                if (Equals(nodeToRemove, parent.LeftChild))
                {
                    parent.LeftChild = nodeToRemove?.LeftChild ?? nodeToRemove?.RightChild;
                }
                else
                {
                    parent.RightChild = nodeToRemove?.LeftChild ?? nodeToRemove?.RightChild;
                }
            }
            else
            {
                Root = Root?.LeftChild ?? Root?.RightChild;
            }
        }

        private void RemoveNodeHavingBothChildren(Node<TItem>? nodeToRemove, Node<TItem> parent)
        {
            Node<TItem>? next = SeekMin(nodeToRemove?.RightChild);
            Remove(next.Data);
            next.LeftChild = nodeToRemove?.LeftChild;

            if (nodeToRemove.Equals(Root))
            {
                Root = next;
            }
            else if (nodeToRemove?.CompareTo(parent.LeftChild.Data) == 0)
            {
                parent.LeftChild = next;
            }
            else
            {
                parent.RightChild = next;
            }
        }

        public XmlSchema? GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            reader.ReadStartElement();
            var xmlSerializer = new XmlSerializer(typeof(Node<TItem>));
            Root = (Node<TItem>)xmlSerializer.Deserialize(reader);
        }

        public void WriteXml(XmlWriter writer)
        {
            var xmlSerializer = new XmlSerializer(typeof(Node<TItem>));
            xmlSerializer.Serialize(writer, Root);
        }
    }
}
