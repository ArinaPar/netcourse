﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BinarySearchTreeProcessor
{
    [Serializable]
    public class IterativeTree<TItem> : BinarySearchTree<TItem> where TItem : IComparable<TItem>
    {
        public IterativeTree()
        {

        }

        public IterativeTree(IEnumerable<TItem> collection) : base(collection)
        {

        }

        protected IterativeTree(SerializationInfo info, StreamingContext context) : base(info, context)
        {

        }

        public override IEnumerator<TItem> GetEnumerator()
        {
            var nodes = new Stack<Node<TItem>>();
            Node<TItem>? current = Root;

            while (current != null)
            {
                nodes.Push(current);

                while (current.LeftChild != null)
                {
                    nodes.Push(current = current.LeftChild);
                }

                yield return nodes.Pop().Data;

                while (current.RightChild == null && nodes.Count != 0)
                {
                    yield return (current = nodes.Pop()).Data;
                }

                current = current.RightChild;
            }
        }

        protected override Node<TItem> SeekParent(TItem data, Node<TItem> current)
        {
            var isFound = false;
            Node<TItem>? parent = null;

            while (!isFound)
            {
                switch (Compare(data, current))
                {
                    case -1:
                        if (current.LeftChild == null)
                        {
                            parent = current;
                            isFound = true;
                        }
                        else
                        {
                            current = current.LeftChild;
                        }
                        break;
                    case 1:
                        if (current.RightChild == null)
                        {
                            parent = current;
                            isFound = true;
                        }
                        else
                        {
                            current = current.RightChild;
                        }
                        break;
                    default:
                        throw new InvalidOperationException();
                }
            }

            return parent;
        }

        protected override bool TryFind(TItem data, out Node<TItem>? result, out Node<TItem>? parent)
        {
            Node<TItem>current = Root;
            result = null;
            parent = null;

            while (result == null)
            {
                switch (Compare(data, current))
                {
                    case 0:
                        result = current;
                        break;
                    case -1:
                        parent = current;
                        current = current.LeftChild;
                        if (current == null)
                            return false;
                        break;
                    case 1:
                        parent = current;
                        current = current.RightChild;
                        if (current == null)
                            return false;
                        break;
                    default:
                        return false;
                }
            }

            return true;
        }
    }
}
