﻿using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace BinarySearchTreeProcessor
{
    [Serializable]
    public class Node<TData> : IComparable<TData> where TData : IComparable<TData>
    {
        [XmlElement(ElementName = "LeftChild")]
        public Node<TData>? LeftChild { get; set; }

        [XmlElement (ElementName = "RightChild")]
        public Node<TData>? RightChild { get; set; }

        [XmlElement (ElementName = "Data")]
        public TData Data { get; set; }

        public Node()
        {

        }

        public Node(TData data)
        {
            Data = data;
        }

        public int CompareTo(TData? other)
        {
            return other?.CompareTo(Data) ?? 1;
        }

        public override string? ToString()
        {
            return Data.ToString();
        }

        public override bool Equals(object? obj)
        {
            return Equals(obj as Node<TData>);
        }

        protected bool Equals(Node<TData>? other)
        {
            return !ReferenceEquals(null, other) && GetHashCode() == other.GetHashCode();
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(LeftChild, RightChild, Data);
        }
    }
}
