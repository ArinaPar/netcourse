﻿using System;
using System.Collections.Generic;

namespace BinarySearchTreeProcessor
{
    public class RecursiveTree<TItem> : BinarySearchTree<TItem> where TItem : IComparable<TItem>
    {
        public RecursiveTree()
        {

        }

        public RecursiveTree(IEnumerable<TItem> collection) : base(collection)
        {

        }

        public override IEnumerator<TItem> GetEnumerator()
        {
            return RecursionYield(Root).GetEnumerator();
        }

        protected override Node<TItem> SeekParent(TItem data, Node<TItem>? current)
        {
            switch (Compare(data, current))
            {
                case -1:
                    return current.LeftChild == null ? current : SeekParent(data, current.LeftChild);
                case 1:
                    return current.RightChild == null ? current : SeekParent(data, current.RightChild);
                default:
                    throw new InvalidOperationException();
            }
        }

        protected override bool TryFind(TItem data, out Node<TItem>? result, out Node<TItem>? parent)
        {
            parent = null;
            return Find(data, Root, ref parent, out result);
        }

        private bool Find(TItem data, Node<TItem>? current, ref Node<TItem>? parent, out Node<TItem> result)
        {
            result = null;

            if (current != null)
            {
                switch (Compare(data, current))
                {
                    case 0:
                        result = current;
                        return true;
                    case -1:
                        parent = current;
                        return Find(data, current.LeftChild, ref parent, out result);
                    case 1:
                        parent = current;
                        return Find(data, current.RightChild, ref parent, out result);
                    default:
                        throw new InvalidOperationException();
                }
            }

            return false;
        }

        private IEnumerable<TItem> RecursionYield(Node<TItem>? current)
        {
            if (current == null)
            {
                yield break;
            }

            foreach (var ret in RecursionYield(current.LeftChild))
            {
                yield return ret;
            }

            yield return current.Data;

            foreach (var rec in RecursionYield(current.RightChild))
            {
                yield return rec;
            }
        }
    }
}
