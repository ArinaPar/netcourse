using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using BinarySearchTreeProcessor;

namespace BinarySearchTreeTests
{
    [TestFixture]
    public abstract class BinaryTreeTests<TItem> where TItem : IComparable<TItem>
    {
        public abstract BinarySearchTree<TItem> CreateEmptyTree();

        public abstract BinarySearchTree<TItem> CreateTree(IEnumerable<TItem> collection);

        [Test]
        public void TestIsEmpty_EmptyTree_True()
        {
            Assert.IsTrue(CreateEmptyTree().IsEmpty);
        }

        public virtual void TestIsEmpty_NotEmptyTree_False(IEnumerable<TItem> values)
        {
            Assert.IsFalse(CreateTree(values).IsEmpty);
        }

        public virtual TItem TestGetRoot_SimpleValues_CorrectResult(IEnumerable<TItem> values)
        {
            BinarySearchTree<TItem> tree = CreateTree(values);

            return tree.Root.Data;
        }

        public virtual void TestGetEnumerator_SimpleValues_CorrectResult(IEnumerable<TItem> values)
        {
            BinarySearchTree<TItem> tree = CreateTree(values);

            Assert.AreEqual(values.OrderBy(i => i).ToArray(), tree.ToArray());
        }

        public virtual IEnumerable<TItem> TestAdd_SimpleValues_CorrectResult(IEnumerable<TItem> values, TItem data)
        {
            BinarySearchTree<TItem> tree = CreateTree(values);

            tree.Add(data);

            return tree.ToArray();
        }

        public virtual void TestAdd_ExistingValue_ExceptionThrown(IEnumerable<TItem> values)
        {
            BinarySearchTree<TItem> tree = CreateTree(values);

            foreach (var value in values)
            {
                Assert.Throws<InvalidOperationException>(() => tree.Add(value));
            }
        }

        public virtual void TestAdd_NullValue_ExceptionThrown(IEnumerable<TItem> values)
        {
            BinarySearchTree<TItem> tree = CreateTree(values);
            TItem value = default;

            if (value == null)
                Assert.Throws<NullReferenceException>(() => tree.Add(value));
        }

        public virtual IEnumerable<TItem> TestAddRange_SimpleValues_CorrectResult(IEnumerable<TItem> values, IEnumerable<TItem> range)
        {
            BinarySearchTree<TItem> tree = CreateTree(values);

            tree.AddRange(range);

            return tree.ToArray();
        }

        public virtual void TestAddRange_NullRange_ExceptionThrown(IEnumerable<TItem> values)
        {
            BinarySearchTree<TItem> tree = CreateTree(values);
            IEnumerable<TItem> range = null;

            Assert.Throws<NullReferenceException>(() => tree.AddRange(range));
        }

        public virtual TItem TestGetMax_SimpleValues_CorrectResult(IEnumerable<TItem> values)
        {
            return CreateTree(values).GetMax();
        }

        public virtual void TestGetMax_EmptyTree_ExceptionThrown()
        {
            BinarySearchTree<TItem> emptyTree = CreateEmptyTree();

            Assert.Throws<ArgumentOutOfRangeException>(() => emptyTree.GetMax());
        }

        public virtual TItem TestGetMin_SimpleValues_CorrectResult(IEnumerable<TItem> values)
        {
            return CreateTree(values).GetMin();
        }

        public virtual void TestGetMin_EmptyTree_ExceptionThrown()
        {
            BinarySearchTree<TItem> emptyTree = CreateEmptyTree();

            Assert.Throws<ArgumentOutOfRangeException>(() => emptyTree.GetMin());
        }

        public virtual void TestFind_ExistingData_CorrectResult(IEnumerable<TItem> values)
        {
            var tree = CreateTree(values);
            foreach (var v in values)
            {
                Assert.AreEqual(v, tree.Find(v).Data);
            }
        }

        public virtual void TestFind_NotExistingData_ExceptionThrown(IEnumerable<TItem> values, TItem data)
        {
            BinarySearchTree<TItem> tree = CreateTree(values);

            Assert.Throws<ArgumentOutOfRangeException>(() => tree.Find(data));
        }

        public virtual IEnumerable<TItem> TestRemove_ExistingValue_Success(IEnumerable<TItem> values, TItem data)
        {
            BinarySearchTree<TItem> tree = CreateTree(values);

            Assert.IsTrue(tree.Remove(data));
            return tree;
        }

        public virtual IEnumerable<TItem> TestRemove_NotExistingValue_Fail(IEnumerable<TItem> values, TItem data)
        {
            BinarySearchTree<TItem> tree = CreateTree(values);

            Assert.IsFalse(tree.Remove(data));
            return tree.ToArray();
        }
    }
}