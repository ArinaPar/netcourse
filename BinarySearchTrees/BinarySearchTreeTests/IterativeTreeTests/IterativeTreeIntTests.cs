﻿using System.Collections.Generic;
using NUnit.Framework;
using BinarySearchTreeTests.TestCaseSources;

namespace BinarySearchTreeTests.IterativeTreeTests
{
    public class IterativeTreeIntTests : IterativeTreeTests<int>
    {
        [TestCaseSource(typeof(BinaryTreeIntTestCaseSource), nameof(BinaryTreeIntTestCaseSource.NodeValuesCases))]
        public override void TestIsEmpty_NotEmptyTree_False(IEnumerable<int> values)
        {
            base.TestIsEmpty_NotEmptyTree_False(values);
        }

        [TestCaseSource(typeof(BinaryTreeIntTestCaseSource), nameof(BinaryTreeIntTestCaseSource.GetRootCases))]
        public override int TestGetRoot_SimpleValues_CorrectResult(IEnumerable<int> values)
        {
            return base.TestGetRoot_SimpleValues_CorrectResult(values);
        }

        [TestCaseSource(typeof(BinaryTreeIntTestCaseSource), nameof(BinaryTreeIntTestCaseSource.NodeValuesCases))]
        public override void TestGetEnumerator_SimpleValues_CorrectResult(IEnumerable<int> values)
        {
            base.TestGetEnumerator_SimpleValues_CorrectResult(values);
        }

        [Test]
        public override void TestGetMin_EmptyTree_ExceptionThrown()
        {
            base.TestGetMin_EmptyTree_ExceptionThrown();
        }

        [TestCaseSource(typeof(BinaryTreeIntTestCaseSource), nameof(BinaryTreeIntTestCaseSource.GetMinCases))]
        public override int TestGetMin_SimpleValues_CorrectResult(IEnumerable<int> values)
        {
            return base.TestGetMin_SimpleValues_CorrectResult(values);
        }

        [Test]
        public override void TestGetMax_EmptyTree_ExceptionThrown()
        {
            base.TestGetMax_EmptyTree_ExceptionThrown();
        }

        [TestCaseSource(typeof(BinaryTreeIntTestCaseSource), nameof(BinaryTreeIntTestCaseSource.GetMaxCases))]
        public override int TestGetMax_SimpleValues_CorrectResult(IEnumerable<int> values)
        {
            return base.TestGetMax_SimpleValues_CorrectResult(values);
        }

        [TestCaseSource(typeof(BinaryTreeIntTestCaseSource), nameof(BinaryTreeIntTestCaseSource.AddCases))]
        public override IEnumerable<int> TestAdd_SimpleValues_CorrectResult(IEnumerable<int> values, int data)
        {
            return base.TestAdd_SimpleValues_CorrectResult(values, data);
        }

        [TestCaseSource(typeof(BinaryTreeIntTestCaseSource), nameof(BinaryTreeIntTestCaseSource.NodeValuesCases))]
        public override void TestAdd_ExistingValue_ExceptionThrown(IEnumerable<int> values)
        {
            base.TestAdd_ExistingValue_ExceptionThrown(values);
        }

        [TestCaseSource(typeof(BinaryTreeIntTestCaseSource), nameof(BinaryTreeIntTestCaseSource.NodeValuesCases))]
        public override void TestAdd_NullValue_ExceptionThrown(IEnumerable<int> values)
        {
            base.TestAdd_NullValue_ExceptionThrown(values);
        }

        [TestCaseSource(typeof(BinaryTreeIntTestCaseSource), nameof(BinaryTreeIntTestCaseSource.AddRangeCases))]
        public override IEnumerable<int> TestAddRange_SimpleValues_CorrectResult(IEnumerable<int> values, IEnumerable<int> range)
        {
            return base.TestAddRange_SimpleValues_CorrectResult(values, range);
        }

        [TestCaseSource(typeof(BinaryTreeIntTestCaseSource), nameof(BinaryTreeIntTestCaseSource.NodeValuesCases))]
        public override void TestAddRange_NullRange_ExceptionThrown(IEnumerable<int> values)
        {
            base.TestAddRange_NullRange_ExceptionThrown(values);
        }

        [TestCaseSource(typeof(BinaryTreeIntTestCaseSource), nameof(BinaryTreeIntTestCaseSource.NodeValuesCases))]
        public override void TestFind_ExistingData_CorrectResult(IEnumerable<int> values)
        {
            base.TestFind_ExistingData_CorrectResult(values);
        }

        [TestCaseSource(typeof(BinaryTreeIntTestCaseSource), nameof(BinaryTreeIntTestCaseSource.FindNotExistingDataCases))]
        public override void TestFind_NotExistingData_ExceptionThrown(IEnumerable<int> values, int data)
        {
            base.TestFind_NotExistingData_ExceptionThrown(values, data);
        }

        [TestCaseSource(typeof(BinaryTreeIntTestCaseSource), nameof(BinaryTreeIntTestCaseSource.RemoveExistingValueCases))]
        public override IEnumerable<int> TestRemove_ExistingValue_Success(IEnumerable<int> values, int data)
        {
            return base.TestRemove_ExistingValue_Success(values, data);
        }

        [TestCaseSource(typeof(BinaryTreeIntTestCaseSource), nameof(BinaryTreeIntTestCaseSource.RemoveNotExistingValueCases))]
        public override IEnumerable<int> TestRemove_NotExistingValue_Fail(IEnumerable<int> values, int data)
        {
            return base.TestRemove_NotExistingValue_Fail(values, data);
        }
    }
}
