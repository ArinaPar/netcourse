﻿using System.Collections.Generic;
using NUnit.Framework;
using BinarySearchTreeTests.TestCaseSources;

namespace BinarySearchTreeTests.IterativeTreeTests
{
    public class IterativeTreeStringTests : IterativeTreeTests<string>
    {
        [TestCaseSource(typeof(BinaryTreeStringTestCaseSource), nameof(BinaryTreeStringTestCaseSource.NodeValuesCases))]
        public override void TestIsEmpty_NotEmptyTree_False(IEnumerable<string> values)
        {
            base.TestIsEmpty_NotEmptyTree_False(values);
        }

        [TestCaseSource(typeof(BinaryTreeStringTestCaseSource), nameof(BinaryTreeStringTestCaseSource.GetRootCases))]
        public override string TestGetRoot_SimpleValues_CorrectResult(IEnumerable<string> values)
        {
            return base.TestGetRoot_SimpleValues_CorrectResult(values);
        }

        [TestCaseSource(typeof(BinaryTreeStringTestCaseSource), nameof(BinaryTreeStringTestCaseSource.NodeValuesCases))]
        public override void TestGetEnumerator_SimpleValues_CorrectResult(IEnumerable<string> values)
        {
            base.TestGetEnumerator_SimpleValues_CorrectResult(values);
        }

        [Test]
        public override void TestGetMin_EmptyTree_ExceptionThrown()
        {
            base.TestGetMin_EmptyTree_ExceptionThrown();
        }

        [TestCaseSource(typeof(BinaryTreeStringTestCaseSource), nameof(BinaryTreeStringTestCaseSource.GetMinCases))]
        public override string TestGetMin_SimpleValues_CorrectResult(IEnumerable<string> values)
        {
            return base.TestGetMin_SimpleValues_CorrectResult(values);
        }

        [Test]
        public override void TestGetMax_EmptyTree_ExceptionThrown()
        {
            base.TestGetMax_EmptyTree_ExceptionThrown();
        }

        [TestCaseSource(typeof(BinaryTreeStringTestCaseSource), nameof(BinaryTreeStringTestCaseSource.GetMaxCases))]
        public override string TestGetMax_SimpleValues_CorrectResult(IEnumerable<string> values)
        {
            return base.TestGetMax_SimpleValues_CorrectResult(values);
        }

        [TestCaseSource(typeof(BinaryTreeStringTestCaseSource), nameof(BinaryTreeStringTestCaseSource.AddCases))]
        public override IEnumerable<string> TestAdd_SimpleValues_CorrectResult(IEnumerable<string> values, string data)
        {
            return base.TestAdd_SimpleValues_CorrectResult(values, data);
        }

        [TestCaseSource(typeof(BinaryTreeStringTestCaseSource), nameof(BinaryTreeStringTestCaseSource.NodeValuesCases))]
        public override void TestAdd_ExistingValue_ExceptionThrown(IEnumerable<string> values)
        {
            base.TestAdd_ExistingValue_ExceptionThrown(values);
        }

        [TestCaseSource(typeof(BinaryTreeStringTestCaseSource), nameof(BinaryTreeStringTestCaseSource.NodeValuesCases))]
        public override void TestAdd_NullValue_ExceptionThrown(IEnumerable<string> values)
        {
            base.TestAdd_NullValue_ExceptionThrown(values);
        }

        [TestCaseSource(typeof(BinaryTreeStringTestCaseSource), nameof(BinaryTreeStringTestCaseSource.AddRangeCases))]
        public override IEnumerable<string> TestAddRange_SimpleValues_CorrectResult(IEnumerable<string> values, IEnumerable<string> range)
        {
            return base.TestAddRange_SimpleValues_CorrectResult(values, range);
        }

        [TestCaseSource(typeof(BinaryTreeStringTestCaseSource), nameof(BinaryTreeStringTestCaseSource.NodeValuesCases))]
        public override void TestAddRange_NullRange_ExceptionThrown(IEnumerable<string> values)
        {
            base.TestAddRange_NullRange_ExceptionThrown(values);
        }

        [TestCaseSource(typeof(BinaryTreeStringTestCaseSource), nameof(BinaryTreeStringTestCaseSource.NodeValuesCases))]
        public override void TestFind_ExistingData_CorrectResult(IEnumerable<string> values)
        {
            base.TestFind_ExistingData_CorrectResult(values);
        }

        [TestCaseSource(typeof(BinaryTreeStringTestCaseSource), nameof(BinaryTreeStringTestCaseSource.FindNotExistingDataCases))]
        public override void TestFind_NotExistingData_ExceptionThrown(IEnumerable<string> values, string data)
        {
            base.TestFind_NotExistingData_ExceptionThrown(values, data);
        }

        [TestCaseSource(typeof(BinaryTreeStringTestCaseSource), nameof(BinaryTreeStringTestCaseSource.RemoveExistingValueCases))]
        public override IEnumerable<string> TestRemove_ExistingValue_Success(IEnumerable<string> values, string data)
        {
            return base.TestRemove_ExistingValue_Success(values, data);
        }

        [TestCaseSource(typeof(BinaryTreeStringTestCaseSource), nameof(BinaryTreeStringTestCaseSource.RemoveNotExistingValueCases))]
        public override IEnumerable<string> TestRemove_NotExistingValue_Fail(IEnumerable<string> values, string data)
        {
            return base.TestRemove_NotExistingValue_Fail(values, data);
        }
    }
}
