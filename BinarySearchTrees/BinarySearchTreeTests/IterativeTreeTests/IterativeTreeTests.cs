using System;
using System.Collections.Generic;
using BinarySearchTreeProcessor;

namespace BinarySearchTreeTests.IterativeTreeTests
{
    public abstract class IterativeTreeTests<TItem> : BinaryTreeTests<TItem> where TItem : IComparable<TItem>
    {
        public override BinarySearchTree<TItem> CreateTree(IEnumerable<TItem> collection)
        {
            return new IterativeTree<TItem>(collection);
        }

        public override BinarySearchTree<TItem> CreateEmptyTree()
        {
            return new IterativeTree<TItem>();
        }
    }
}