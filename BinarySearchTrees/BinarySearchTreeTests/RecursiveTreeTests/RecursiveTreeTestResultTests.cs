﻿using System.Collections.Generic;
using NUnit.Framework;
using BinarySearchTreeTests.TestCaseSources;
using Entities;

namespace BinarySearchTreeTests.RecursiveTreeTests
{
    public class RecursiveTreeTestResultTests : RecursiveTreeTests<TestResult<int>>
    {
        [TestCaseSource(typeof(BinaryTreeTestResultTestCaseSource), nameof(BinaryTreeTestResultTestCaseSource.NodeValuesCases))]
        public override void TestIsEmpty_NotEmptyTree_False(IEnumerable<TestResult<int>> values)
        {
            base.TestIsEmpty_NotEmptyTree_False(values);
        }

        [TestCaseSource(typeof(BinaryTreeTestResultTestCaseSource), nameof(BinaryTreeTestResultTestCaseSource.GetRootCases))]
        public override TestResult<int> TestGetRoot_SimpleValues_CorrectResult(IEnumerable<TestResult<int>> values)
        {
            return base.TestGetRoot_SimpleValues_CorrectResult(values);
        }

        [TestCaseSource(typeof(BinaryTreeTestResultTestCaseSource), nameof(BinaryTreeTestResultTestCaseSource.NodeValuesCases))]
        public override void TestGetEnumerator_SimpleValues_CorrectResult(IEnumerable<TestResult<int>> values)
        {
            base.TestGetEnumerator_SimpleValues_CorrectResult(values);
        }

        [Test]
        public override void TestGetMin_EmptyTree_ExceptionThrown()
        {
            base.TestGetMin_EmptyTree_ExceptionThrown();
        }

        [TestCaseSource(typeof(BinaryTreeTestResultTestCaseSource), nameof(BinaryTreeTestResultTestCaseSource.GetMinCases))]
        public override TestResult<int> TestGetMin_SimpleValues_CorrectResult(IEnumerable<TestResult<int>> values)
        {
            return base.TestGetMin_SimpleValues_CorrectResult(values);
        }

        [Test]
        public override void TestGetMax_EmptyTree_ExceptionThrown()
        {
            base.TestGetMax_EmptyTree_ExceptionThrown();
        }

        [TestCaseSource(typeof(BinaryTreeTestResultTestCaseSource), nameof(BinaryTreeTestResultTestCaseSource.GetMaxCases))]
        public override TestResult<int> TestGetMax_SimpleValues_CorrectResult(IEnumerable<TestResult<int>> values)
        {
            return base.TestGetMax_SimpleValues_CorrectResult(values);
        }

        [TestCaseSource(typeof(BinaryTreeTestResultTestCaseSource), nameof(BinaryTreeTestResultTestCaseSource.AddCases))]
        public override IEnumerable<TestResult<int>> TestAdd_SimpleValues_CorrectResult(IEnumerable<TestResult<int>> values, TestResult<int> data)
        {
            return base.TestAdd_SimpleValues_CorrectResult(values, data);
        }

        [TestCaseSource(typeof(BinaryTreeTestResultTestCaseSource), nameof(BinaryTreeTestResultTestCaseSource.NodeValuesCases))]
        public override void TestAdd_ExistingValue_ExceptionThrown(IEnumerable<TestResult<int>> values)
        {
            base.TestAdd_ExistingValue_ExceptionThrown(values);
        }

        [TestCaseSource(typeof(BinaryTreeTestResultTestCaseSource), nameof(BinaryTreeTestResultTestCaseSource.NodeValuesCases))]
        public override void TestAdd_NullValue_ExceptionThrown(IEnumerable<TestResult<int>> values)
        {
            base.TestAdd_NullValue_ExceptionThrown(values);
        }

        [TestCaseSource(typeof(BinaryTreeTestResultTestCaseSource), nameof(BinaryTreeTestResultTestCaseSource.AddRangeCases))]
        public override IEnumerable<TestResult<int>> TestAddRange_SimpleValues_CorrectResult(IEnumerable<TestResult<int>> values, IEnumerable<TestResult<int>> range)
        {
            return base.TestAddRange_SimpleValues_CorrectResult(values, range);
        }

        [TestCaseSource(typeof(BinaryTreeTestResultTestCaseSource), nameof(BinaryTreeTestResultTestCaseSource.NodeValuesCases))]
        public override void TestAddRange_NullRange_ExceptionThrown(IEnumerable<TestResult<int>> values)
        {
            base.TestAddRange_NullRange_ExceptionThrown(values);
        }

        [TestCaseSource(typeof(BinaryTreeTestResultTestCaseSource), nameof(BinaryTreeTestResultTestCaseSource.NodeValuesCases))]
        public override void TestFind_ExistingData_CorrectResult(IEnumerable<TestResult<int>> values)
        {
            base.TestFind_ExistingData_CorrectResult(values);
        }

        [TestCaseSource(typeof(BinaryTreeTestResultTestCaseSource), nameof(BinaryTreeTestResultTestCaseSource.FindNotExistingDataCases))]
        public override void TestFind_NotExistingData_ExceptionThrown(IEnumerable<TestResult<int>> values, TestResult<int> data)
        {
            base.TestFind_NotExistingData_ExceptionThrown(values, data);
        }

        [TestCaseSource(typeof(BinaryTreeTestResultTestCaseSource), nameof(BinaryTreeTestResultTestCaseSource.RemoveExistingValueCases))]
        public override IEnumerable<TestResult<int>> TestRemove_ExistingValue_Success(IEnumerable<TestResult<int>> values, TestResult<int> data)
        {
            return base.TestRemove_ExistingValue_Success(values, data);
        }

        [TestCaseSource(typeof(BinaryTreeTestResultTestCaseSource), nameof(BinaryTreeTestResultTestCaseSource.RemoveNotExistingValueCases))]
        public override IEnumerable<TestResult<int>> TestRemove_NotExistingValue_Fail(IEnumerable<TestResult<int>> values, TestResult<int> data)
        {
            return base.TestRemove_NotExistingValue_Fail(values, data);
        }
    }
}
