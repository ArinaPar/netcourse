﻿using System;
using System.Collections.Generic;
using BinarySearchTreeProcessor;

namespace BinarySearchTreeTests.RecursiveTreeTests
{
    public abstract class RecursiveTreeTests<TItem> : BinaryTreeTests<TItem> where TItem : IComparable<TItem>
    {
        public override BinarySearchTree<TItem> CreateTree(IEnumerable<TItem> collection)
        {
            return new RecursiveTree<TItem>(collection);
        }

        public override BinarySearchTree<TItem> CreateEmptyTree()
        {
            return new RecursiveTree<TItem>();
        }
    }
}
