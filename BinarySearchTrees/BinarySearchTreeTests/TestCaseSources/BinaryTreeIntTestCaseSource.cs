﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BinarySearchTreeProcessor;
using NUnit.Framework;

namespace BinarySearchTreeTests.TestCaseSources
{
    internal static class BinaryTreeIntTestCaseSource
    {
        internal static IEnumerable<IEnumerable<int>> NodeValuesCases
        {
            get
            {
                yield return new List<int> { 1, 2, 3, 4, 5, 6 };
                yield return new List<int> { 2, 3, 5, 23, 7, 9, 54 };
                yield return new List<int> { 100, 1039, 439, 101, 32, 564, 345, 87, 5, 7865, 34, 1, 4 };
                yield return new List<int> { -34, -35, -36, -37, -38, -39, -40, -41, -42, -43, -44 };
                yield return new List<int> { 1 };
                yield return new List<int> { 1, -10 };
                yield return new List<int> { 6, 5, 4, 3, 2, 1 };
            }
        }

        internal static IEnumerable GetMinCases
        {
            get
            {
                yield return new TestCaseData(Enumerable.Range(1, 100)).Returns(1);
                yield return new TestCaseData(new List<int> { 100, 1039, 439, 101, 32, 564, 345, 87, 5, 7865, 34, 1, 4 }).Returns(1);
                yield return new TestCaseData(new List<int> { -34, -35, -36, -37, -38, -39, -40, -41, -42, -43, -44 }).Returns(-44);
                yield return new TestCaseData(new List<int> { 1 }).Returns(1);
                yield return new TestCaseData(new List<int> { 1, -10 }).Returns(-10);
            }
        }

        internal static IEnumerable GetMaxCases
        {
            get
            {
                yield return new TestCaseData(Enumerable.Range(1, 100)).Returns(100);
                yield return new TestCaseData(new List<int> { 100, 1039, 439, 101, 32, 564, 345, 87, 5, 7865, 34, 1, 4 }).Returns(7865);
                yield return new TestCaseData(new List<int> { -34, -35, -36, -37, -38, -39, -40, -41, -42, -43, -44 }).Returns(-34);
                yield return new TestCaseData(new List<int> { 1 }).Returns(1);
                yield return new TestCaseData(new List<int> { 1, -10 }).Returns(1);
            }
        }

        internal static IEnumerable GetRootCases
        {
            get
            {
                yield return new TestCaseData(Enumerable.Range(1, 100)).Returns(new Node<int>(1).Data);
                yield return new TestCaseData(new List<int> { 100, 1039, 439, 101, 32, 564, 345, 87, 5, 7865, 34, 1, 4 }).Returns(new Node<int>(100).Data);
                yield return new TestCaseData(new List<int> { -34, -35, -36, -37, -38, -39, -40, -41, -42, -43, -44 }).Returns(new Node<int>(-34).Data);
                yield return new TestCaseData(new List<int> { 1 }).Returns(new Node<int>(1).Data);
                yield return new TestCaseData(new List<int> { 1, -10 }).Returns(new Node<int>(1).Data);
            }
        }

        internal static IEnumerable AddCases
        {
            get
            {
                yield return new TestCaseData(new List<int> { 1, 2, 3, 4, 5, 6 }, 7).Returns(new List<int> { 1, 2, 3, 4, 5, 6, 7 });
                yield return new TestCaseData(new List<int> { 100, 1039, 439, 101, 32, 564, 345, 87, 5, 7865, 34, 1, 4 }, 0).Returns(new List<int> { 0, 1, 4, 5, 32, 34, 87, 100, 101, 345, 439, 564, 1039, 7865 });
                yield return new TestCaseData(new List<int> { -34, -35, -36, -37, -38, -39, -40, -41, -42, -43, -44 }, 100).Returns(new List<int> { -44, -43, -42, -41, -40, -39, -38, -37, -36, -35, -34, 100 });
                yield return new TestCaseData(new List<int> { 1 }, 12).Returns(new List<int> { 1, 12 });
                yield return new TestCaseData(new List<int> { 1, -10 }, -5).Returns(new List<int> { -10, -5, 1 });
            }
        }

        internal static IEnumerable AddRangeCases
        {
            get
            {
                yield return new TestCaseData(new List<int> { 1, 2, 3, 4, 5, 6 }, new List<int> { 7, 8, 9, 10 }).Returns(new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
                yield return new TestCaseData(new List<int> { 100, 1039, 439, 101, 32, 564, 345, 87, 5, 7865, 34, 1, 4 }, new List<int> { 99, 2 }).Returns(new List<int> { 1, 2, 4, 5, 32, 34, 87, 99, 100, 101, 345, 439, 564, 1039, 7865 });
                yield return new TestCaseData(new List<int> { -34, -35, -36, -37, -38, -39, -40, -41, -42, -43, -44 }, new List<int> { 0, 100, -100 }).Returns(new List<int> { -100, -44, -43, -42, -41, -40, -39, -38, -37, -36, -35, -34, 0, 100 });
                yield return new TestCaseData(new List<int> { 1 }, new List<int> { 100, -2, 0 }).Returns(new List<int> { -2, 0, 1, 100});
                yield return new TestCaseData(new List<int> { 1, -10 }, new List<int> { -5, 20 }).Returns(new List<int> { -10, -5, 1, 20 });
            }
        }

        internal static readonly object[] FindNotExistingDataCases =
        {
             new object[] {new List<int> { 1, 2, 3, 4, 5, 6 }, 10},
             new object[] {new List<int> { 2, 3, 5, 23, 7, 9, 54 }, -10},
             new object[] {new List<int> { 100, 1039, 439, 101, 32, 564, 345, 87, 5, 7865, 34, 1, 4 }, 30},
             new object[] {new List<int> { -34, -35, -36, -37, -38, -39, -40, -41, -42, -43, -44 }, -33},
             new object[] {new List<int> { 1 }, 2},
             new object[] {new List<int> { 1, -10 }, -5},
             new object[] {new List<int> { 6, 5, 4, 3, 2, 1 }, 0}
        };

        internal static IEnumerable RemoveExistingValueCases
        {
            get
            {
                yield return new TestCaseData(new List<int> { 1, 2, 3, 4, 5, 6 }, 1).Returns(new List<int> { 2, 3, 4, 5, 6 });
                yield return new TestCaseData(new List<int> { 100, 1039, 439, 101, 32, 564, 345, 87, 5, 7865, 34, 1, 4 }, 101).Returns(new List<int> { 1, 4, 5, 32, 34, 87, 100, 345, 439, 564, 1039, 7865 });
                yield return new TestCaseData(new List<int> { -34, -35, -36, -37, -38, -39, -40, -41, -42, -43, -44 }, -34).Returns(new List<int> { -44, -43, -42, -41, -40, -39, -38, -37, -36, -35 });
                yield return new TestCaseData(new List<int> { 1 }, 1).Returns(new List<int>());
                yield return new TestCaseData(new List<int> { 1, -10 }, 1).Returns(new List<int> { -10 });
                yield return new TestCaseData(new List<int> { 1, -10, 10 }, 1).Returns(new List<int> { -10, 10 });
                yield return new TestCaseData(new List<int> { 1, -10, 10, 5, 12 }, 10).Returns(new List<int> { -10, 1, 5, 12 });
                yield return new TestCaseData(new List<int> { 1, -10, 10, -5, -12 }, -10).Returns(new List<int> { -12, -5, 1, 10});
            }
        }

        internal static IEnumerable RemoveNotExistingValueCases
        {
            get
            {
                yield return new TestCaseData(new List<int> { 1, 2, 3, 4, 5, 6 }, 7).Returns(new List<int> { 1, 2, 3, 4, 5, 6 });
                yield return new TestCaseData(new List<int> { 100, 1039, 439, 101, 32, 564, 345, 87, 5, 7865, 34, 1, 4 }, 102).Returns(new List<int> { 1, 4, 5, 32, 34, 87, 100, 101, 345, 439, 564, 1039, 7865 });
                yield return new TestCaseData(new List<int> { -34, -35, -36, -37, -38, -39, -40, -41, -42, -43, -44 }, -33).Returns(new List<int> { -44, -43, -42, -41, -40, -39, -38, -37, -36, -35, -34});
                yield return new TestCaseData(new List<int> { 1 }, 0).Returns(new List<int> { 1 });
                yield return new TestCaseData(new List<int> { 1, -10 }, -3).Returns(new List<int> { -10, 1 });
            }
        }
    }
}
