﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BinarySearchTreeProcessor;
using NUnit.Framework;

namespace BinarySearchTreeTests.TestCaseSources
{
    internal static class BinaryTreeStringTestCaseSource
    {
        internal static IEnumerable<IEnumerable<string>> NodeValuesCases
        {
            get
            {
                yield return new List<string> { "1", "2", "3", "4", "5"};
                yield return new List<string> { "b", "f", "a", "n", "z", "d", "x" };
                yield return new List<string> { "sa", "sb", "sc", "ad", "ae", "af" };
                yield return new List<string> { "asd", "asdf", "asdfg", "bn", "bnm", "bnmo" };
                yield return new List<string> { "d" };
                yield return new List<string> { "derevo" };
                yield return new List<string> { "f", "afg" };
                yield return new List<string> { "6", "5", "4", "3", "2", "1" };
            }
        }

        internal static IEnumerable GetMinCases
        {
            get
            {
                yield return new TestCaseData(new List<string> { "1", "2", "3", "4", "5" }).Returns("1");
                yield return new TestCaseData(new List<string> { "b", "f", "a", "n", "z", "d", "x" }).Returns("a");
                yield return new TestCaseData(new List<string> { "sa", "sb", "sc", "ad", "ae", "af" }).Returns("ad");
                yield return new TestCaseData(new List<string> { "asd", "asdf", "asdfg", "bn", "bnm", "bnmo" }).Returns("asd");
                yield return new TestCaseData(new List<string> { "d" }).Returns("d");
                yield return new TestCaseData(new List<string> { "derevo" }).Returns("derevo");
                yield return new TestCaseData(new List<string> { "f", "afg" }).Returns("afg");
                yield return new TestCaseData(new List<string> { "6", "5", "4", "3", "2", "1" }).Returns("1");

            }
        }

        internal static IEnumerable GetMaxCases
        {
            get
            {
                yield return new TestCaseData(new List<string> { "1", "2", "3", "4", "5" }).Returns("5");
                yield return new TestCaseData(new List<string> { "b", "f", "a", "n", "z", "d", "x" }).Returns("z");
                yield return new TestCaseData(new List<string> { "sa", "sb", "sc", "ad", "ae", "af" }).Returns("sc");
                yield return new TestCaseData(new List<string> { "asd", "asdf", "asdfg", "bn", "bnm", "bnmo" }).Returns("bnmo");
                yield return new TestCaseData(new List<string> { "d" }).Returns("d");
                yield return new TestCaseData(new List<string> { "derevo" }).Returns("derevo");
                yield return new TestCaseData(new List<string> { "f", "afg" }).Returns("f");
                yield return new TestCaseData(new List<string> { "6", "5", "4", "3", "2", "1" }).Returns("6");
            }
        }

        internal static IEnumerable GetRootCases
        {
            get
            {
                yield return new TestCaseData(new List<string> { "1", "2", "3", "4", "5" }).Returns("1");
                yield return new TestCaseData(new List<string> { "b", "f", "a", "n", "z", "d", "x" }).Returns("b");
                yield return new TestCaseData(new List<string> { "sa", "sb", "sc", "ad", "ae", "af" }).Returns("sa");
                yield return new TestCaseData(new List<string> { "asd", "asdf", "asdfg", "bn", "bnm", "bnmo" }).Returns("asd");
                yield return new TestCaseData(new List<string> { "d" }).Returns("d");
                yield return new TestCaseData(new List<string> { "derevo" }).Returns("derevo");
                yield return new TestCaseData(new List<string> { "f", "afg" }).Returns("f");
                yield return new TestCaseData(new List<string> { "6", "5", "4", "3", "2", "1" }).Returns("6");
            }
        }

        internal static IEnumerable AddCases
        {
            get
            {
                yield return new TestCaseData(new List<string> { "1", "2", "3", "4", "5" }, "6").Returns(new List<string> { "1", "2", "3", "4", "5", "6" });
                yield return new TestCaseData(new List<string> { "b", "f", "a", "n", "z", "d", "x" }, "k").Returns(new List<string>
                    { "a", "b", "d", "f", "k", "n", "x", "z"});
                yield return new TestCaseData(new List<string> { "sa", "sb", "sc", "ad", "ae", "af" }, "aa").Returns(new List<string> { "aa", "ad", "ae", "af", "sa", "sb", "sc" });
                yield return new TestCaseData(new List<string> { "asd", "asdf", "asdfg", "bn", "bnm", "bnmo" }, "ba").Returns(new List<string>
                    { "asd", "asdf", "asdfg", "ba", "bn", "bnm", "bnmo" });
                yield return new TestCaseData(new List<string> { "d" }, "a").Returns(new List<string>{ "a", "d" });
                yield return new TestCaseData(new List<string> { "derevo" }, "big").Returns(new List<string> { "big", "derevo" });
                yield return new TestCaseData(new List<string> { "f", "afg" }, "h").Returns(new List<string> { "afg", "f", "h" });
                yield return new TestCaseData(new List<string> { "6", "5", "4", "3", "2", "1" }, "7").Returns(new List<string> { "1", "2", "3", "4", "5", "6", "7" });
            }
        }

        internal static IEnumerable AddRangeCases
        {
            get
            {
                yield return new TestCaseData(new List<string> { "1", "2", "3", "4", "5" }, new List<string> {"6", "7"}).Returns(new List<string> { "1", "2", "3", "4", "5", "6", "7" });
                yield return new TestCaseData(new List<string> { "b", "f", "a", "n", "z", "d", "x" }, new List<string> { "k", "c", "y" }).Returns(new List<string>
                    { "a", "b", "c", "d", "f", "k", "n", "x", "y", "z"});
                yield return new TestCaseData(new List<string> { "sa", "sb", "sc", "ad", "ae", "af" }, new List<string> { "k", "c", "y" }).Returns(new List<string> { "ad", "ae", "af", "c", "k", "sa", "sb", "sc", "y" });
                yield return new TestCaseData(new List<string> { "asd", "asdf", "asdfg", "bn", "bnm", "bnmo" }, new List<string> {"ba", "baab"}).Returns(new List<string>
                    { "asd", "asdf", "asdfg", "ba", "baab", "bn", "bnm", "bnmo" });
                yield return new TestCaseData(new List<string> { "d" }, new List<string>{ "b", "f", "a", "n", "z", "x" }).Returns(new List<string> { "a", "b", "d", "f", "n", "x", "z" });
                yield return new TestCaseData(new List<string> { "derevo" }, new List<string>{ "big", "n" }).Returns(new List<string> { "big", "derevo", "n" });
                yield return new TestCaseData(new List<string> { "6", "5", "4", "3", "2", "1" }, new List<string> { "7", "0" }).Returns(new List<string> { "0", "1", "2", "3", "4", "5", "6", "7" });
            }
        }

        internal static readonly object[] FindNotExistingDataCases =
        {
             new object[] { new List<string> { "1", "2", "3", "4", "5" }, "10"},
             new object[] { new List<string> { "b", "f", "a", "n", "z", "d", "x" }, "A"},
             new object[] { new List<string> { "sa", "sb", "sc", "ad", "ae", "af" }, "a"},
             new object[] { new List<string> { "asd", "asdf", "asdfg", "bn", "bnm", "bnmo" }, "ad"},
             new object[] { new List<string> { "d" }, "dr"},
             new object[] { new List<string> { "derevo" }, "d"},
             new object[] { new List<string> { "f", "afg" }, "a"},
             new object[] { new List<string> { "6", "5", "4", "3", "2", "1" }, "0"}
        };

        internal static IEnumerable RemoveExistingValueCases
        {
            get
            {
                yield return new TestCaseData(new List<string> { "1", "2", "3", "4", "5" }, "5").Returns(new List<string> { "1", "2", "3", "4"});
                yield return new TestCaseData(new List<string> { "b", "f", "a", "n", "z", "d", "x" }, "a").Returns(new List<string> { "b", "d", "f", "n", "x", "z" });
                yield return new TestCaseData(new List<string> { "sa", "sb", "sc", "ad", "ae", "af" }, "sc").Returns(new List<string> { "ad", "ae", "af", "sa", "sb" });
                yield return new TestCaseData(new List<string> { "asd", "asdf", "asdfg", "bn", "bnm", "bnmo" }, "asdf").Returns(new List<string> { "asd", "asdfg", "bn", "bnm", "bnmo" });
                yield return new TestCaseData(new List<string> { "d" }, "d").Returns(new List<string>());
                yield return new TestCaseData(new List<string> { "derevo" }, "derevo").Returns(new List<string>());
                yield return new TestCaseData(new List<string> { "f", "afg" }, "afg").Returns(new List<string> { "f" });
                yield return new TestCaseData(new List<string> { "6", "5", "4", "3", "2", "1" }, "1").Returns(new List<string> { "2", "3", "4", "5", "6" });
            }
        }

        internal static IEnumerable RemoveNotExistingValueCases
        {
            get
            {
                yield return new TestCaseData(new List<string> { "1", "2", "3", "4", "5" }, "10").Returns(new List<string> { "1", "2", "3", "4", "5" });
                yield return new TestCaseData(new List<string> { "b", "f", "a", "n", "z", "d", "x" }, "A").Returns(new List<string> { "a", "b", "d", "f", "n", "x", "z" });
                yield return new TestCaseData(new List<string> { "sa", "sb", "sc", "ad", "ae", "af" }, "a").Returns(new List<string> { "ad", "ae", "af", "sa", "sb", "sc" });
                yield return new TestCaseData(new List<string> { "asd", "asdf", "asdfg", "bn", "bnm", "bnmo" }, "ad").Returns(new List<string> { "asd", "asdf", "asdfg", "bn", "bnm", "bnmo" });
                yield return new TestCaseData(new List<string> { "d" }, "dr").Returns(new List<string> { "d" });
                yield return new TestCaseData(new List<string> { "derevo" }, "d").Returns(new List<string> { "derevo" });
                yield return new TestCaseData(new List<string> { "f", "afg" }, "a").Returns(new List<string> { "afg", "f" });
                yield return new TestCaseData(new List<string> { "6", "5", "4", "3", "2", "1" }, "0").Returns(new List<string> { "1", "2", "3", "4", "5", "6" });
            }
        }
    }
}
