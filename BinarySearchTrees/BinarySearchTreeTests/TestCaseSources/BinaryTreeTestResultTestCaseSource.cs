﻿using System;
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using Entities;

namespace BinarySearchTreeTests.TestCaseSources
{
    internal static class BinaryTreeTestResultTestCaseSource
    {
        internal static IEnumerable<IEnumerable<TestResult<int>>> NodeValuesCases
        {
            get
            {
                yield return new List<TestResult<int>>
                {
                    new("Name1", "Surname1","Test1", new DateTime(2020, 1, 1), 1),
                    new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                    new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                    new("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5)
                };

                yield return new List<TestResult<int>>
                {
                    new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                    new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2),
                    new("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5),
                    new("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1),
                    new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3)
                };

                yield return new List<TestResult<int>>
                {
                    new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4)
                };

                yield return new List<TestResult<int>>
                {
                    new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                    new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2)
                };

                yield return new List<TestResult<int>>
                {
                    new("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5),
                    new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                    new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                    new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2),
                    new("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1)
                };
            }
        }

        internal static IEnumerable GetMinCases
        {
            get
            {
                yield return new TestCaseData(new List<TestResult<int>>
                {
                    new("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1),
                    new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2),
                    new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                    new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                    new("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5)
                }).Returns(new TestResult<int>("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1));

                yield return new TestCaseData(new List<TestResult<int>>
                {
                    new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                    new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2),
                    new("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5),
                    new("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1),
                    new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3)
                }).Returns(new TestResult<int>("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1));

                yield return new TestCaseData(new List<TestResult<int>>
                {
                    new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4)
                }).Returns(new TestResult<int>("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4));

                yield return new TestCaseData(new List<TestResult<int>>
                {
                    new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                    new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2)
                }).Returns(new TestResult<int>("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2));

                yield return new TestCaseData(new List<TestResult<int>>
                {
                    new("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5),
                    new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                    new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                    new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2),
                    new("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1)
                }).Returns(new TestResult<int>("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1));
            }
        }

        internal static IEnumerable GetMaxCases
        {
            get
            {
                yield return new TestCaseData(new List<TestResult<int>>
                {
                    new("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1),
                    new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2),
                    new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                    new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                    new("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5)
                }).Returns(new TestResult<int>("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5));

                yield return new TestCaseData(new List<TestResult<int>>
                {
                    new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                    new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2),
                    new("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5),
                    new("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1),
                    new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3)
                }).Returns(new TestResult<int>("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5));

                yield return new TestCaseData(new List<TestResult<int>>
                {
                    new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4)
                }).Returns(new TestResult<int>("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4));

                yield return new TestCaseData(new List<TestResult<int>>
                {
                    new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                    new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2)
                }).Returns(new TestResult<int>("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4));

                yield return new TestCaseData(new List<TestResult<int>>
                {
                    new("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5),
                    new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                    new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                    new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2),
                    new("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1)
                }).Returns(new TestResult<int>("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5));
            }
        }

        internal static IEnumerable GetRootCases
        {
            get
            {
                yield return new TestCaseData(new List<TestResult<int>>
                {
                    new("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1),
                    new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2),
                    new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                    new("Name2", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                    new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 2),
                    new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                    new("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5),
                    new("Name6", "Surname5", "Test5", new DateTime(2020, 5, 5), 5),
                    new("Name5", "Surname5", "Test6", new DateTime(2020, 5, 5), 5),
                    new("Name5", "Surname5", "Test2", new DateTime(2020, 5, 5), 5)

                }).Returns(new TestResult<int>("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1));

                yield return new TestCaseData(new List<TestResult<int>>
                {
                    new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                    new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2),
                    new("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5),
                    new("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1),
                    new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3)
                }).Returns(new TestResult<int>("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4));

                yield return new TestCaseData(new List<TestResult<int>>
                {
                    new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4)
                }).Returns(new TestResult<int>("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4));

                yield return new TestCaseData(new List<TestResult<int>>
                {
                    new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                    new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2)
                }).Returns(new TestResult<int>("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4));

                yield return new TestCaseData(new List<TestResult<int>>
                {
                    new("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5),
                    new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                    new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                    new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2),
                    new("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1)
                }).Returns(new TestResult<int>("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5));
            }
        }

        internal static IEnumerable AddCases
        {
            get
            {
                yield return new TestCaseData(
                        new List<TestResult<int>>
                        {
                            new("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1),
                            new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2),
                            new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                            new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                            new("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5)
                        },
                        new TestResult<int>("Name6", "Surname6", "Test6", new DateTime(2020, 6, 6), 6))
                    .Returns(new List<TestResult<int>>
                    {
                        new("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1),
                        new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2),
                        new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                        new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                        new("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5),
                        new("Name6", "Surname6", "Test6", new DateTime(2020, 6, 6), 6)
                    });

                yield return new TestCaseData(
                        new List<TestResult<int>>
                        {
                            new("Name6", "Surname6", "Test6", new DateTime(2020, 6, 6), 6),
                            new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2),
                            new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                            new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                            new("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5)
                        },
                        new TestResult<int>("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1))
                    .Returns(new List<TestResult<int>>
                    {
                        new("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1),
                        new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2),
                        new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                        new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                        new("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5),
                        new("Name6", "Surname6", "Test6", new DateTime(2020, 6, 6), 6)
                    });

                yield return new TestCaseData(
                        new List<TestResult<int>>
                        {
                            new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                            new("Name6", "Surname6", "Test6", new DateTime(2020, 6, 6), 6),
                            new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2),
                            new("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1),
                            new("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5)
                        },
                        new TestResult<int>("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3))
                    .Returns(new List<TestResult<int>>
                    {
                        new("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1),
                        new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2),
                        new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                        new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                        new("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5),
                        new("Name6", "Surname6", "Test6", new DateTime(2020, 6, 6), 6)
                    });

                yield return new TestCaseData(
                        new List<TestResult<int>>
                        {
                            new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                        },
                        new TestResult<int>("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3))
                    .Returns(new List<TestResult<int>>
                    {
                        new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                        new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                    });

                yield return new TestCaseData(
                        new List<TestResult<int>>
                        {
                            new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                        },
                        new TestResult<int>("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4))
                    .Returns(new List<TestResult<int>>
                    {
                        new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                        new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                    });
            }
        }

        internal static IEnumerable AddRangeCases
        {
            get
            {
                yield return new TestCaseData(
                        new List<TestResult<int>>
                        {
                            new("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1),
                            new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2),
                            new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                            new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                            new("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5)
                        },
                        new List<TestResult<int>>
                        {
                            new("Name6", "Surname6", "Test6", new DateTime(2020, 6, 6), 6),
                            new("Name7", "Surname7", "Test7", new DateTime(2020, 7, 7), 7),
                            new("Name8", "Surname8", "Test8", new DateTime(2020, 8, 8), 8)
                        })
                    .Returns(new List<TestResult<int>>
                    {
                        new("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1),
                        new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2),
                        new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                        new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                        new("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5),
                        new("Name6", "Surname6", "Test6", new DateTime(2020, 6, 6), 6),
                        new("Name7", "Surname7", "Test7", new DateTime(2020, 7, 7), 7),
                        new("Name8", "Surname8", "Test8", new DateTime(2020, 8, 8), 8)
                    });

                yield return new TestCaseData(
                        new List<TestResult<int>>
                        {
                            new("Name6", "Surname6", "Test6", new DateTime(2020, 6, 6), 6),
                            new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2),
                            new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                            new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                            new("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5)
                        },
                        new List<TestResult<int>>
                        {
                            new("Name7", "Surname7", "Test7", new DateTime(2020, 7, 7), 7),
                            new("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1),
                            new("Name8", "Surname8", "Test8", new DateTime(2020, 8, 8), 8)
                        })
                    .Returns(new List<TestResult<int>>
                    {
                        new("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1),
                        new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2),
                        new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                        new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                        new("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5),
                        new("Name6", "Surname6", "Test6", new DateTime(2020, 6, 6), 6),
                        new("Name7", "Surname7", "Test7", new DateTime(2020, 7, 7), 7),
                        new("Name8", "Surname8", "Test8", new DateTime(2020, 8, 8), 8)
                    });

                yield return new TestCaseData(
                        new List<TestResult<int>>
                        {
                            new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                            new("Name6", "Surname6", "Test6", new DateTime(2020, 6, 6), 6),
                            new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2),
                            new("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1),
                            new("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5)
                        },
                        new List<TestResult<int>>
                        {
                            new("Name7", "Surname7", "Test7", new DateTime(2020, 7, 7), 7),
                            new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                        })
                    .Returns(new List<TestResult<int>>
                    {
                        new("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1),
                        new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2),
                        new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                        new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                        new("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5),
                        new("Name6", "Surname6", "Test6", new DateTime(2020, 6, 6), 6),
                        new("Name7", "Surname7", "Test7", new DateTime(2020, 7, 7), 7)
                    });

                yield return new TestCaseData(
                        new List<TestResult<int>>
                        {
                            new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                        },
                        new List<TestResult<int>>
                        {
                            new("Name7", "Surname7", "Test7", new DateTime(2020, 7, 7), 7),
                            new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                        })
                    .Returns(new List<TestResult<int>>
                    {
                        new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                        new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                        new("Name7", "Surname7", "Test7", new DateTime(2020, 7, 7), 7)
                    });
            }
        }

        internal static readonly object[] FindNotExistingDataCases =
        {
             new object[] {
                 new List<TestResult<int>>
                 {
                     new("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1),
                     new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2),
                     new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                     new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                     new("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5)
                 },
                 new TestResult<int>("Name6", "Surname6", "Test6", new DateTime(2020, 6, 6), 6)
             },

             new object[] {
                 new List<TestResult<int>>
                 {
                     new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2),
                     new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                     new("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1),
                     new("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5),
                     new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3)
                 },
                 new TestResult<int>("Name6", "Surname6", "Test6", new DateTime(2020, 6, 6), 6)
             },

             new object[] {
                 new List<TestResult<int>>
                 {
                     new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2)
                 },
                 new TestResult<int>("Name6", "Surname6", "Test6", new DateTime(2020, 6, 6), 6)
             },
        };

        internal static IEnumerable RemoveExistingValueCases
        {
            get
            {
                yield return new TestCaseData(
                        new List<TestResult<int>>
                        {
                            new("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1),
                            new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2),
                            new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                            new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                            new("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5)
                        },
                        new TestResult<int>("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1))
                    .Returns(new List<TestResult<int>>
                    {
                        new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2),
                        new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                        new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                        new("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5)
                    });

                yield return new TestCaseData(
                        new List<TestResult<int>>
                        {
                            new("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1),
                            new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2),
                            new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                            new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                            new("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5)
                        },
                        new TestResult<int>("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5))
                    .Returns(new List<TestResult<int>>
                    {
                        new("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1),
                        new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2),
                        new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                        new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                    });

                yield return new TestCaseData(
                        new List<TestResult<int>>
                        {
                            new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                            new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                            new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2),
                            new("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5),
                            new("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1)
                        },
                        new TestResult<int>("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2))
                    .Returns(new List<TestResult<int>>
                    {
                        new("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1),
                        new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                        new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                        new("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5)
                    });

                yield return new TestCaseData(
                        new List<TestResult<int>>
                        {
                            new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                        },
                        new TestResult<int>("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3))
                    .Returns(new List<TestResult<int>> { });
            }
        }

        internal static IEnumerable RemoveNotExistingValueCases
        {
            get
            {
                yield return new TestCaseData(
                        new List<TestResult<int>>
                        {
                            new("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1),
                            new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2),
                            new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                            new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                            new("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5)
                        },
                        new TestResult<int>("Name7", "Surname7", "Test7", new DateTime(2020, 7, 7), 7))
                    .Returns(new List<TestResult<int>>
                    {
                        new("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1),
                        new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2),
                        new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                        new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                        new("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5)
                    });

                yield return new TestCaseData(
                        new List<TestResult<int>>
                        {
                            new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                            new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                            new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2),
                            new("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5),
                            new("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1)
                        },
                        new TestResult<int>("Name7", "Surname7", "Test7", new DateTime(2020, 7, 7), 7))
                    .Returns(new List<TestResult<int>>
                    {
                        new("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1),
                        new("Name2", "Surname2", "Test2", new DateTime(2020, 2, 2), 2),
                        new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                        new("Name4", "Surname4", "Test4", new DateTime(2020, 4, 4), 4),
                        new("Name5", "Surname5", "Test5", new DateTime(2020, 5, 5), 5)
                    });

                yield return new TestCaseData(
                        new List<TestResult<int>>
                        {
                            new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                        },
                        new TestResult<int>("Name1", "Surname1", "Test1", new DateTime(2020, 1, 1), 1))
                    .Returns(new List<TestResult<int>>
                    {
                        new("Name3", "Surname3", "Test3", new DateTime(2020, 3, 3), 3),
                    });
            }
        }
    }
}
