﻿using System;
using System.Xml.Serialization;

namespace Entities
{
    [Serializable]
    public class Student : IComparable<Student>
    {
        [XmlAttribute(AttributeName = "Name")]
        public string Name { get; set; }

        [XmlAttribute(AttributeName = "Surname")]
        public string Surname { get; set; }

        public Student(string name, string surname)
        {
            Name = name;
            Surname = surname;
        }

        protected Student()
        {

        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Name, Surname);
        }

        public override bool Equals(object? obj)
        {
            return Equals(obj as Student);
        }

        private bool Equals(Student? other)
        {
            return !ReferenceEquals(null, other) && GetHashCode() == other.GetHashCode();
        }

        public override string ToString()
        {
            return $"{Name} {Surname}";
        }

        public int CompareTo(Student? other)
        {
            var surnameComparison = string.Compare(Surname, other.Surname, StringComparison.Ordinal);
            switch (surnameComparison)
            {
                case < 0:
                    return -1;
                case > 0:
                    return 1;
            }

            var nameComparison =  string.Compare(Name, other.Name, StringComparison.Ordinal);
            return nameComparison switch
            {
                < 0 => -1,
                > 0 => 1,
                _ => 0
            };
        }
    }
}