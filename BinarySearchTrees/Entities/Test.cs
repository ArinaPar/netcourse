﻿using System;
using System.Xml.Serialization;

namespace Entities
{
    [Serializable]
    public class Test : IComparable<Test>
    {
        [XmlAttribute(AttributeName = "Name")]
        public string Name { get; set; }

        [XmlAttribute(AttributeName = "Date")]
        public DateTime Date { get; set; }

        public Test(string name, DateTime date)
        {
            Name = name;
            Date = date;
        }

        protected Test()
        {
            
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Name, Date);
        }

        public override bool Equals(object? obj)
        {
            return Equals(obj as Test);
        }

        public override string ToString()
        {
            return $"{Name} : {Date}";
        }

        private bool Equals(Test? other)
        {
            return !ReferenceEquals(null, other) && GetHashCode() == other.GetHashCode();
        }

        public int CompareTo(Test? other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            var nameComparison = string.Compare(Name, other.Name, StringComparison.Ordinal);
            if (nameComparison < 0) return -1;
            if (nameComparison > 0) return 1;
            return Date.CompareTo(other.Date);
        }
    }
}