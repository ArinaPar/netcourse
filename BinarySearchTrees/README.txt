The applicalion has two variants of the "Binary search tree" structure: iterative and recursive.
It can store data of any comparable type.
The user can add, find and remove tree nodes. Tree structure can also get a sequence of nodes in ascending order.
The solution contains tests for iterative and recursive trees with different data types:
1) integer;
2) string;
3) test result (custom class: includes information about the student, the test and the mark).
