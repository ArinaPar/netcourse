﻿using System;

namespace PointProcessor
{
    public class Formatter
    {
        private static readonly string outputFormat = "X: {0,4}{1,-5:.0###} Y: {2,4}{3,-5:.0###}";

        public static string Format(Point point)
        {
            if (point == null)
                return null;

            decimal wholePartOfX = TakeWholePart(point.X);
            decimal factionalPartOfX = TakeFactionalPart(point.X, wholePartOfX);
            decimal wholePartOfY = TakeWholePart(point.Y);
            decimal factionalPartOfY = TakeFactionalPart(point.Y, wholePartOfY);
            return FormatString(wholePartOfX, factionalPartOfX, wholePartOfY, factionalPartOfY);
        }

        private static decimal TakeWholePart(decimal coordinate)
        {
            return Math.Truncate(coordinate);
        }

        private static decimal TakeFactionalPart(decimal coordinate, decimal wholePartOfY)
        {
            return coordinate - wholePartOfY;
        }

        private static string FormatString(decimal wholePartOfX, decimal factionalPartOfX, decimal wholePartOfY, decimal factionalPartOfY)
        {
            return string.Format(outputFormat, wholePartOfX, factionalPartOfX, wholePartOfY, factionalPartOfY);
        }
    }
}
