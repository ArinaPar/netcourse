﻿using System;
using System.Globalization;

namespace PointProcessor
{
    public static class Parser
    {
        private const int xIndex = 0;
        private const int yIndex = 1;
        private const int countOfCoordinates = 2;
        private const string coordinatesSeparator = ",";
        private const NumberStyles style = NumberStyles.Number;
        private static readonly CultureInfo culture = CultureInfo.CreateSpecificCulture("en-GB");

        public static bool TryParsePoint(string line, out Point point)
        {
            if (line != null)
            {
                string[] coordinates = line.Split(coordinatesSeparator);
                if (coordinates.Length == countOfCoordinates)
                {
                    if (TryParseCoordinate(coordinates[xIndex], out decimal x)
                      && TryParseCoordinate(coordinates[yIndex], out decimal y))
                    {
                        point = new Point(x, y);
                        return true;
                    }
                }
            }

            point = null;
            return false;
        }

        private static bool TryParseCoordinate(string coordinate, out decimal x)
        {
            return decimal.TryParse(coordinate, style, culture, out x);
        }
    }
}
