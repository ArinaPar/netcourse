﻿using System;
using System.Collections.Generic;
using System.IO;

namespace PointProcessor
{
    public class Processor
    {
        public static void ProcessFiles(string[] filenames)
        {
            foreach (var filename in filenames)
            {
                if (File.Exists(filename))
                {
                    WriteFormatStringsFromFile(filename);
                }
                else
                {
                    Console.WriteLine($"File not found {filename}");
                }
            }
        }

        private static void WriteFormatStringsFromFile(string filename)
        {
            string[] lines = File.ReadAllLines(filename);
            foreach (var line in lines)
            {
                Console.WriteLine(ProcessLine(line));
            }
        }

        public static void ProcessConsole()
        {
            var lines = ReadLinesFromConsole();
            WriteFormatStrings(lines);
        }

        private static void WriteFormatStrings(List<string> lines)
        {
            foreach (var line in lines)
            {
                Console.WriteLine(line);
            }
        }

        private static List<string> ReadLinesFromConsole()
        {
            WriteInstructions();
            return ReadLines();
        }

        private static List<string> ReadLines()
        {
            var lines = new List<string>();
            string line;

            while ((line = Console.ReadLine()) != string.Empty)
            {
                var result = ProcessLine(line);
                if (result != null)
                {
                    lines.Add(result);
                }
                else
                {
                    Console.WriteLine("Invalid format");
                }
            }

            return lines;
        }

        private static void WriteInstructions()
        {
            Console.WriteLine("Enter coordinates (X,Y) of the points");
            Console.WriteLine("Format: \"123.234,43.6533\"");
            Console.WriteLine("Double press ENTER after you write coordinates of the last point");
        }

        public static string ProcessLine(string line)
        {
            if (Parser.TryParsePoint(line, out Point point))
            {
                return Formatter.Format(point);
            }
            
            return null;
        }
    }
}
