﻿using System;

namespace Calculator
{
    public static class RootCalculator
    {
        public static double CalculateByNewtonMethod(double number, double degree, double accuracy)
        {
            double root = number / degree;
            double delta = number;

            while (!Equals(root, delta, accuracy))
            {
                delta = number;
                for (int i = 1; i < degree; i++)
                {
                    delta /= root;
                }

                root = 0.5 * (delta + root);
            }

            return root;
        }

        public static double CalculateByStandardMethod(double number, double degree)
        {
            return Math.Pow(number, 1 / degree);
        }

        public static bool Equals(double first, double second, double accuracy)
        {
            return Math.Abs(first - second) < accuracy;
        }
    }
}
