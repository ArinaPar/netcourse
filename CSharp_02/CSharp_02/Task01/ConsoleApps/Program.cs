﻿using System;
using Calculator;

namespace ConsoleApps
{
    internal class Program
    {
        private const double minBorder = 0;
        private const double maxBorderOfAccuracy = 0.11;

        private static void Main()
        {
            WriteInstructionsOnConsole();

            double number = ReadPositiveNumberFromConsole("number", minBorder);
            double degree = ReadPositiveNumberFromConsole("degree", minBorder);
            double accuracy = ReadPositiveNumberFromConsole("accuracy", minBorder, maxBorderOfAccuracy);

            double newtonResult = Root.CalculateByNewtonMethod(number, degree, accuracy);
            double standardResult = Root.CalculateByStandardMethod(number, degree);

            ShowResults(accuracy, newtonResult, standardResult);
        }

        private static void WriteInstructionsOnConsole()
        {
            Console.WriteLine("This program can help you to get n-degree root of the number.");
        }

        private static void ShowResults(double accuracy, double newtonResult, double standardResult)
        {
            Console.WriteLine($"Result of calculation by Newton method: {newtonResult}");
            Console.WriteLine($"Result of calculation by standard method: {standardResult}");

            Console.WriteLine(Root.Equals(newtonResult, standardResult, accuracy) 
                ? $"Results are equal with accuracy {accuracy}"
                : $"Results are not equal with accuracy {accuracy}");
        }

        private static double ReadPositiveNumberFromConsole(string nameOfVariable, double minBorder, double? maxBorder = null)
        {
            bool isCorrect = false;
            double input = default;

            Console.Write($"Enter the {nameOfVariable}: ");
            while (!isCorrect)
            {
                if (double.TryParse(Console.ReadLine(), out input)
                    && (input >= minBorder)
                    && (!maxBorder.HasValue || input < maxBorder))
                {
                    isCorrect = true;
                }
                else
                {
                    Console.WriteLine($"Error. Enter the integer bigger than {minBorder} and smaller than {maxBorder ?? double.MaxValue}");
                }
            }

            return input;
        }
    }
}
