﻿using System;
using Convertation;

namespace ConvertToString
{
    internal class Program
    {

        private static void Main()
        {
            WriteInstructionsOnConsole();
            uint n = GetPositiveIntegerFromConsole();

            try
            {
                foreach (BaseSystem baseSystem in Enum.GetValues(typeof(BaseSystem)))
                {
                    var practiceResult = Convertor.Convert(n, baseSystem);
                    var theoryResult = Convertor.ConvertToStandard(n, baseSystem);
                    WrireResultsOnConsole(baseSystem, practiceResult, theoryResult);
                }
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private static void WriteInstructionsOnConsole()
        {
            Console.WriteLine("This program can help you to convert a positive number to different numeral systems\n");
        }

        private static uint GetPositiveIntegerFromConsole()
        {
            bool isCorrect = false;
            uint input = default;
            Console.WriteLine("Enter the positive integer");

            while (!isCorrect)
            {
                if (uint.TryParse(Console.ReadLine(), out input))
                {
                    isCorrect = true;
                }
                else
                {
                    Console.WriteLine("Error! Enter the integer bigger than 0");
                }
            }

            return input;
        }

        private static void WrireResultsOnConsole(BaseSystem baseSystem, string practiceResult, string? theoryResult)
        {
            Console.Write($"\n{baseSystem}\nCalculated result: {practiceResult}\n");
            if (!string.IsNullOrEmpty(theoryResult))
            {
                Console.Write($"Standard: {theoryResult}\n{CheckResultsForEquivalence(practiceResult, theoryResult)}\n");
            }
        }

        private static string CheckResultsForEquivalence(string firstResult, string secondResult)
        {
            return (firstResult == secondResult) ? "Results are equal" : "Results are not equal";
        }
    }
}