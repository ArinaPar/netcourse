﻿using System;
using System.Text;

namespace Convertation
{
    public static class Convertor
    {
        private static readonly string alphabetBin = "01";
        private static readonly string alphabetOct = "01234567";
        private static readonly string alphabetHex = "0123456789abcdef";
        private static readonly string alphabetBase32 = "0123456789ABCDEFGHJKMNPQRSTVWXYZ";
        private static readonly string alphabetBase64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

        public static string Convert(uint number, BaseSystem baseSystem)
        {
            return baseSystem switch
            {
                BaseSystem.Bin => ToBin(number),
                BaseSystem.Oct => ToOct(number),
                BaseSystem.Hex => ToHex(number),
                BaseSystem.Base32 => ToBase32(number),
                BaseSystem.Base64 => ToBase64(number),
                _ => throw new ArgumentOutOfRangeException(baseSystem.ToString(), "Invalid base system name"),
            };
        }

        public static string ConvertToStandard(uint number, BaseSystem baseSystem)
        {
            return baseSystem switch
            {
                BaseSystem.Bin => ToBinStandard(number),
                BaseSystem.Oct => ToOctStandard(number),
                BaseSystem.Hex => ToHexStandard(number),
                BaseSystem.Base32 => null,
                BaseSystem.Base64 => ToBase64Standard(number),
                _ => throw new ArgumentOutOfRangeException(baseSystem.ToString(), "Invalid base system name"),
            };
        }

        private static string ToBase(uint number, string alphabet)
        {
            uint basis = (uint)alphabet.Length;
            StringBuilder result = new StringBuilder();
            string modulo;

            while (number > 0)
            {
                int index = (int)(number % basis);
                modulo = alphabet[index].ToString();
                result = result.Insert(0, modulo);
                number /= basis;
            }

            return result.ToString();
        }

        public static string ToBin(uint number)
        {
            return ToBase(number, alphabetBin);
        }

        public static string ToBinStandard(uint number)
        {
            return System.Convert.ToString(number, alphabetBin.Length);
        }

        public static string ToOct(uint number)
        {
            return ToBase(number, alphabetOct);
        }

        public static string ToOctStandard(uint number)
        {
            return System.Convert.ToString(number, alphabetOct.Length);
        }

        public static string ToHex(uint number)
        {
            return ToBase(number, alphabetHex);
        }

        public static string ToHexStandard(uint number)
        {
            return System.Convert.ToString(number, alphabetHex.Length);
        }

        public static string ToBase32(uint number)
        {
            return ToBase(number, alphabetBase32);
        }

        public static string ToBase64(uint number)
        {
            return ToBase(number, alphabetBase64);
        }

        public static string ToBase64Standard(uint number)
        {
            return System.Convert.ToBase64String(BitConverter.GetBytes(number));
        }
    }
}
