﻿using System;

namespace TriangleProcessor
{
    public class Triangle
    {
        public double A { get; }
        public double B { get; }
        public double C { get; }

        private Triangle(double a, double b, double c) : base()
        {
            A = a;
            B = b;
            C = c;
        }

        public static Triangle CreateTriangle(double a, double b, double c)
        {
            if (IsExist(a, b, c))
            {
                return new Triangle(a, b, c);
            }

            throw new InvalidOperationException("A triangle with such sides does not exist");
        }

        public double CalculatePerimeter()
        {
            return A + B + C;
        }

        public double CalculateArea()
        {
            double halfOfPerimeter = CalculatePerimeter() / 2;
            return Math.Sqrt(halfOfPerimeter * (halfOfPerimeter - A) * (halfOfPerimeter - B) * (halfOfPerimeter - C));
        }

        private static bool IsExist(double a, double b, double c)
        {
            return (a > 0) && (b > 0) && (c > 0)
                && a + b > c
                && b + c > a
                && a + c > b;
        }

        public override string ToString()
        {
            return $"A = {A}, B = {B}, C = {C}";
        }
    }
}
