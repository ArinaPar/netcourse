﻿using NUnit.Framework;
using TriangleProcessor;
using System;

namespace TriangleProcessorTests
{
    [TestFixture]
    public class TriangleTests
    {
        [Test]
        public void TestTriangle_SimpleValues_Success(
            [Values(2, 2, 3)] int a,
            [Values(2, 3, 3)] int b,
            [Values(2, 3, 3)] int c)
        {
            Triangle triangle = Triangle.CreateTriangle(a, b, c);

            Assert.IsNotNull(triangle);
            Assert.AreEqual(a, triangle.A);
            Assert.AreEqual(b, triangle.B);
            Assert.AreEqual(c, triangle.C);
        }

        [Test]
        public void TestCreateTriangle_InvalidValues_ExceptionThrown(
            [Values(0, -2, 100)] int a,
            [Values(0, -5)] int b,
            [Values(0, -3, 1000)] int c)
        {

            Assert.Throws<InvalidOperationException>(() => Triangle.CreateTriangle(a, b, c));
        }

        [Test]
        public void TestToString_SimpleValues_CorrectFormat(
            [Values(2, 3, 2)] int a,
            [Values(2, 3, 2)] int b,
            [Values(2, 3, 3)] int c)
        {
            string expected = $"A = {a}, B = {b}, C = {c}";

            Triangle triangle = Triangle.CreateTriangle(a, b, c);
            string actual = triangle.ToString();

            Assert.AreEqual(expected, actual);
        }

        [TestCase(3, 4, 5, 12)]
        public void TestPerimeter_SimpleValues_CorrectValue(double a, double b, double c, double expected)
        {
            Triangle triangle = Triangle.CreateTriangle(a, b, c);
            double actual = triangle.CalculatePerimeter();

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void TestArea_SimpleValues_CorrectValue()
        {
            double expectedArea = 6.000;

            Triangle triangle = Triangle.CreateTriangle(3, 4, 5);
            double actualArea = triangle.CalculateArea();

            Assert.AreEqual(expectedArea, actualArea);
        }
    }
}
