﻿using System;
using TriangleProcessor;

namespace TriangleProcessorUI
{
    partial class Program
    {
        private static void Main()
        {
            double a = ReadSide("Enter the length of side A: ");
            double b = ReadSide("Enter the length of side B: ");
            double c = ReadSide("Enter the length of side C: ");

            try
            {
                var triangle = Triangle.CreateTriangle(a, b, c);
                double perimeter = triangle.CalculatePerimeter();
                double area = triangle.CalculateArea();
                ShowResults(triangle, perimeter, area);
            }
            catch (InvalidOperationException ex)
            {
                WhiteErrorOnConsole(ex);
            }
        }

        private static double ReadSide(string inputMesssage)
        {
            string errorMessage = "Error! Enter the POSITIVE NUMBER";
            return ReadDouble(inputMesssage, errorMessage);
        }

        private static double ReadDouble(string inputMessage, string errorMessage)
        {
            bool isCorrect = false;
            double value = default;

            while (!isCorrect)
            {
                Console.Write(inputMessage);

                if (double.TryParse(Console.ReadLine(), out value) && (value > 0))
                {
                    isCorrect = true;
                }
                else
                {
                    Console.WriteLine(errorMessage);
                }
            }

            return value;
        }

        private static void WhiteErrorOnConsole(InvalidOperationException ex)
        {
            Console.WriteLine(ex.Message);
        }

        private static void ShowResults(Triangle triangle, double perimeter, double area)
        {
            Console.WriteLine();
            Console.WriteLine(triangle);
            Console.WriteLine($"Perimeter: {perimeter}");
            Console.WriteLine($"Area: {area:f3}");
        }
    }
}
