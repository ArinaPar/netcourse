﻿namespace GreatestCommonDivisor
{
    public class EuclideanGreatestCommonDivisor : GreatestCommonDivisorCalculator
    {
        private static EuclideanGreatestCommonDivisor _instance;

        private EuclideanGreatestCommonDivisor() { }

        public static EuclideanGreatestCommonDivisor Instance => _instance ??= new EuclideanGreatestCommonDivisor();
     

        public override uint Calculate(uint first, uint second)
        {
            if (first == 0)
                return second;

            if (second == 0)
                return first;

            while (first != second)
            {
                if (first > second)
                {
                    first -= second;
                }
                else
                {
                    second -= first;
                }
            }

            return first;
        }
    }
}
