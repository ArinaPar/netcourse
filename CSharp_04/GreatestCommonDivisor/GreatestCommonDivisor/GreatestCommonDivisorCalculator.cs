﻿using System;
using System.Diagnostics;
using Helpers;

namespace GreatestCommonDivisor
{
    public abstract class GreatestCommonDivisorCalculator
    {
        private const uint firstIndex = 0;
        private const uint secondIndex = 1;
        private const uint thirdIndex = 2;

        public abstract uint Calculate(uint first, uint second);

        public uint Calculate(uint first, uint second, uint third)
        {
            return Calculate(new uint[] { first, second, third });
        }

        public uint Calculate(uint first, uint second, uint third, uint fourth)
        {
            return Calculate(new uint[] { first, second, third, fourth });
        }

        public uint Calculate(uint first, uint second, uint third, uint fourth, uint fifth)
        {
            return Calculate(new uint[] { first, second, third, fourth, fifth });
        }

        public uint Calculate(params uint[] numbers)
        {
            Validator.ThrowIfNull(numbers, nameof(numbers));
            Validator.ThrowIfOutOfRange(numbers.Length, 1, nameof(numbers));

            uint result = Calculate(numbers[firstIndex], numbers[secondIndex]);

            for (uint i = thirdIndex; i < numbers.Length; i++)
            {
                result = Calculate(result, numbers[i]);
            }

            return result;
        }

        public uint Calculate(out TimeSpan elapsed, params uint[] numbers)
        {
            Stopwatch timer = new Stopwatch();

            timer.Start();
            uint result = Calculate(numbers);
            timer.Stop();

            elapsed = timer.Elapsed;
            return result;
        }
    }
}
