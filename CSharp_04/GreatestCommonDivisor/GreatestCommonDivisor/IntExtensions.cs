﻿namespace GreatestCommonDivisor
{
    public static class IntExtensions
    {
        public static bool IsOdd(this uint num) => (num & 1) == 1;
        public static bool IsEven(this uint num) => (num & 1) == 0;
    }
}
