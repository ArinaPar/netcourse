﻿namespace GreatestCommonDivisor
{
    public class StainGreatestCommonDivisor : GreatestCommonDivisorCalculator
    {
        private static StainGreatestCommonDivisor _instance;

        private StainGreatestCommonDivisor() { }

        public static StainGreatestCommonDivisor Instance => _instance ??= new StainGreatestCommonDivisor();

        public override uint Calculate(uint first, uint second)
        {
            if (first == 0)
                return second;

            if (second == 0)
                return first;

            return CalculateRecursively(first, second);
        }

        private uint CalculateRecursively(uint first, uint second)
        {
            if (first == second)
            {
                return second;
            }

            if (first.IsEven())
            {
                return second.IsOdd()
                    ? CalculateRecursively(first / 2, second)
                    : CalculateRecursively(first / 2, second / 2) * 2;
            }
            else if (second.IsEven())
            {
                return CalculateRecursively(first, second / 2);
            }
            else
            {
                return first > second
                    ? CalculateRecursively((first - second) / 2, second)
                    : CalculateRecursively((second - first) / 2, first);
            }
        }
    }
}
