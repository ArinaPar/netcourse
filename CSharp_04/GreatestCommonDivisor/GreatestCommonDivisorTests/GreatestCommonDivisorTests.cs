﻿using GreatestCommonDivisor;
using NUnit.Framework;
using System;

namespace GreatestCommonDivisorTests
{
    [TestFixtureSource(typeof(TestHelper), nameof(TestHelper.Instances))]
    public class GreatestCommonDivisorTests
    {
        private GreatestCommonDivisorCalculator greatestCommonDivisor;

        public GreatestCommonDivisorTests(GreatestCommonDivisorCalculator greatestCommonDivisor)
        {
            this.greatestCommonDivisor = greatestCommonDivisor;
        }

        [TestCase(12u, 12u, 12u)]
        [TestCase(2u, 3u, 1u)]
        [TestCase(5u, 4u, 1u)]
        [TestCase(16u, 12u, 4u)]
        [TestCase(15u, 5u, 5u)]
        [TestCase(15u, 0u, 15u)]
        [TestCase(155555555u, 500000000u, 5u)]
        [TestCase(0u, 5u, 5u)]
        [TestCase(0u, 0u, 0u)]
        public void TestCalculate_TwoSimpleValues_Success(uint a, uint b, uint expected)
        {
            uint actual = greatestCommonDivisor.Calculate(a, b);

            Assert.AreEqual(expected, actual);
        }

        [TestCase(12u, 12u, 12u, 12u)]
        [TestCase(2u, 3u, 1u, 1u)]
        [TestCase(5u, 4u, 3u, 1u)]
        [TestCase(16u, 12u, 4u, 4u)]
        [TestCase(155u, 5u, 10u, 5u)]
        [TestCase(150000005u, 555555555u, 1000000u, 5u)]
        [TestCase(0u, 5u, 10u, 5u)]
        [TestCase(155u, 0u, 0u, 155u)]
        [TestCase(0u, 0u, 0u, 0u)]
        public void TestCalculate_ThreeSimpleValues_Success(uint a, uint b, uint c, uint expected)
        {
            uint actual = greatestCommonDivisor.Calculate(a, b, c);

            Assert.AreEqual(expected, actual);
        }

        [TestCase(12u, 12u, 12u, 12u, 12u)]
        [TestCase(2u, 3u, 2u, 4u, 1u)]
        [TestCase(5u, 4u, 3u, 2u, 1u)]
        [TestCase(16u, 12u, 4u, 8u, 4u)]
        [TestCase(15u, 5u, 10u, 455u, 5u)]
        [TestCase(15u, 0u, 0u, 0u, 15u)]
        [TestCase(11111115u, 500000000u, 10555555u, 455555555u, 5u)]
        [TestCase(0u, 0u, 0u, 0u, 0u)]
        public void TestCalculate_FourSimpleValues_Success(uint a, uint b, uint c, uint d, uint expected)
        {
            uint actual = greatestCommonDivisor.Calculate(a, b, c, d);

            Assert.AreEqual(expected, actual);
        }

        [TestCase(12u, 12u, 12u, 12u, 12u, 12u)]
        [TestCase(2u, 3u, 2u, 4u, 3u, 1u)]
        [TestCase(5u, 4u, 3u, 2u, 4u, 1u)]
        [TestCase(16u, 12u, 4u, 8u, 4u, 4u)]
        [TestCase(15u, 5u, 10u, 455u, 5u, 5u)]
        [TestCase(15498345u, 546445u, 154345460u, 455555555u, 5464545u, 5u)]
        [TestCase(0u, 5u, 0u, 0u, 0u, 5u)]
        [TestCase(0u, 0u, 0u, 0u, 0u, 0u)]
        public void TestCalculate_FiveSimpleValues_Success(uint a, uint b, uint c, uint d, uint e, uint expected)
        {
            uint actual = greatestCommonDivisor.Calculate(a, b, c, d, e);

            Assert.AreEqual(expected, actual);
        }

        [TestCase(new uint[] { 12u, 12u, 12u, 12u, 12u, 12u, 12u, 12u, 12u, 12u, 12u, 12u, 12u, 12u, 12u }, 12u)]
        [TestCase(new uint[] { 100u, 90u, 80u, 120u, 210u, 70u, 60u, 100u, 50u, 40u, 340u, 30u, 1000u, 20u, 60u }, 10u)]
        [TestCase(new uint[] { 12u, 12u, 12u, 12u, 12u, 12u, 12u, 12u, 12u, 12u, 12u, 12u, 12u, 12u, 12u }, 12u)]
        [TestCase(new uint[] { 500000000u, 53546640u, 134565645u, 2345645u, 3453564300u, 16342455u, 455555555u, 1556655u, 556345u, 453567545u, 342453450u, 1565u, 55555u, 163700u, 123289980u }, 5u)]
        [TestCase(new uint[] { 0u, 0u, 0u, 0u, 0u, 0u, 10u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u }, 10u)]
        [TestCase(new uint[] { 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u }, 0u)]
        public void TestCalculate_Params_Success(uint[] digits, uint expected)
        {
            uint actual = greatestCommonDivisor.Calculate(digits);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void TestCalculate_Null_ThrowsException()
        {
            Assert.That(
                Assert.Throws<ArgumentNullException>(() =>
                    greatestCommonDivisor.Calculate(null))
                .ParamName,
            Is.EqualTo("numbers"));
        }

        [Test]
        public void TestCalculate_Zero_ThrowsException()
        {
            Assert.That(
                Assert.Throws<ArgumentOutOfRangeException>(() =>
                    greatestCommonDivisor.Calculate(new uint[] { }))
                .ParamName,
            Is.EqualTo("numbers"));
        }

        [Test]
        public void TestCalculate_OneValue_ThrowsException()
        {
            Assert.That(
                Assert.Throws<ArgumentOutOfRangeException>(() =>
                    greatestCommonDivisor.Calculate(new uint[] { 100 }))
                .ParamName,
            Is.EqualTo("numbers"));
        }


        [TestCase(new uint[] { 12u, 12u, 12u, 12u, 12u, 12u, 12u, 12u, 12u, 12u, 12u, 12u, 12u, 12u, 12u }, 2)]
        [TestCase(new uint[] { 100u, 90u, 80u, 120u, 210u, 70u, 60u, 100u, 50u, 40u, 340u, 30u, 1000u, 20u, 60u }, 2)]
        [TestCase(new uint[] { 12u, 12u, 12u, 12u, 12u, 12u, 12u, 12u, 12u, 12u, 12u, 12u, 12u, 12u, 12u }, 2)]
        [TestCase(new uint[] { 500000000u, 53546640u, 134565645u, 2345645u, 3453564300u, 16342455u, 455555555u, 1556655u, 556345u, 453567545u, 342453450u, 1565u, 55555u, 163700u, 123289980u }, 6)]
        [TestCase(new uint[] { 0u, 0u, 0u, 0u, 0u, 0u, 10u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u }, 2)]
        [TestCase(new uint[] { 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u }, 2)]
        public void TestCalculate_TimeSpan_Success(uint[] digits, int expected)
        {
            greatestCommonDivisor.Calculate(out TimeSpan time, digits);

            Assert.IsTrue(time.Seconds < expected);
        }
    }
}
