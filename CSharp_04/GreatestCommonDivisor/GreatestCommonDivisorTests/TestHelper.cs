﻿using GreatestCommonDivisor;
using NUnit.Framework;
using System.Collections;

namespace GreatestCommonDivisorTests
{
    internal class TestHelper
    {
        public static IEnumerable Instances
        {
            get
            {
                yield return new TestFixtureData(EuclideanGreatestCommonDivisor.Instance);
                yield return new TestFixtureData(StainGreatestCommonDivisor.Instance);
            }
        }
    }
}
