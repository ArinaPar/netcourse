﻿using GreatestCommonDivisor;
using System;

namespace GreatestCommonDivisorUI
{
    internal class Program
    {
        private static void Main()
        {
            uint[] numbers = ReadUintsFromConsole();
            
            try
            {
                uint resultEuclid = EuclideanGreatestCommonDivisor.Instance.Calculate(out TimeSpan timeEuclid, numbers);
                uint resultStain = StainGreatestCommonDivisor.Instance.Calculate(out TimeSpan timeStain, numbers);

                PrintResults(resultEuclid, timeEuclid, resultStain, timeStain);
            }
            catch (Exception ex)
            {
                PrintExceptionMessage(ex);
            }
        }

        private static uint[] ReadUintsFromConsole()
        {
            Console.WriteLine("Enter the number of parameters (more than 1): ");
            uint numberOfParameters = ReadUintFromConsole();
            var result = new uint[numberOfParameters];

            Console.WriteLine($"Enter {numberOfParameters} non-negative integers");
            for (int i = 0; i < result.Length; i++)
            {
                result[i] = ReadUintFromConsole();
            }

            return result;
        }

        private static uint ReadUintFromConsole()
        {
            uint number;
            while (!uint.TryParse(Console.ReadLine(), out number))
            {
                Console.WriteLine("Invalid input. Value should be a non-negative integer.");
            }

            return number;
        }

        private static void PrintExceptionMessage(Exception ex)
        {
            Console.WriteLine(ex.Message);
        }

        private static void PrintResults(uint resultEuclid, TimeSpan timeEuclid, uint resultStain, TimeSpan timeStain)
        {
            const string euclideanMethodName = "EUCLIDEAN METHOD";
            const string stainMethodName = "STAIN METHOD";

            PrintResult(euclideanMethodName, resultEuclid, timeEuclid);
            PrintResult(stainMethodName, resultStain, timeStain);
            CompareResults(resultEuclid, resultStain);
        }

        private static void PrintResult(string name, uint result, TimeSpan time)
        {
            const string formatString = "Calculated GCD: {0}, spended time: {1:s\\:ffffff}s";

            Console.WriteLine(name);
            Console.WriteLine(formatString, result, time);
        }

        private static void CompareResults(uint firstResult, uint secondResult)
        {
            if (firstResult == secondResult)
            {
                Console.WriteLine("Results are equal");
            }
            else
            {
                Console.WriteLine("Results are not equal");
            }
        }
    }
}