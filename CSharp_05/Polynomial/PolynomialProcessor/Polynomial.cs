﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Helpers;

namespace PolynomialProcessor
{
    public class Polynomial : IEnumerable<int>
    {
        private readonly int[] coefficients;

        public int Degree => coefficients.Length - 1;

        public int Length => coefficients.Length;

        public int this[int index] => coefficients[index];

        private Polynomial(params int[] coefficients)
        {
            this.coefficients = NormalizeCoefficients(coefficients);
        }

        private Polynomial(Polynomial polynomial) : this(polynomial.coefficients) { }

        public static Polynomial CreatePolynomial(Polynomial polynomial)
        {
            Validator.ThrowIfNull(polynomial, nameof(polynomial));
            return new Polynomial(polynomial);
        }

        public static Polynomial CreatePolynomial(params int[] coefficients)
        {
            Validator.ThrowIfNull(coefficients, nameof(coefficients));
            return new Polynomial(coefficients);
        }

        public static Polynomial operator +(Polynomial a, Polynomial b)
        {
            ThrowIfNull(a, b, nameof(a), nameof(b));

            var summed = new int[Math.Max(a.Length, b.Length)];
            for (int i = 0; i < summed.Length; i++)
            {
                if (i < a.Length)
                {
                    summed[i] = a[i];
                }

                if (i < b.Length)
                {
                    summed[i] += b[i];
                }
            }

            return new Polynomial(summed);
        }

        public static Polynomial operator -(Polynomial a, Polynomial b)
        {
            ThrowIfNull(a, b, nameof(a), nameof(b));

            var deducted = new int[Math.Max(a.Length, b.Length)];
            for (int i = 0; i < deducted.Length; i++)
            {
                if (i < a.Length)
                {
                    deducted[i] = a[i];
                }

                if (i < b.Length)
                {
                    deducted[i] -= b[i];
                }
            }

            return new Polynomial(deducted.ToArray());
        }

        public static Polynomial operator *(Polynomial polynomial, int digit)
        {
            Validator.ThrowIfNull(polynomial, nameof(polynomial));

            var multiplied = new int[polynomial.Length];
            for (int i = 0; i < polynomial.Length; i++)
            {
                multiplied[i] = polynomial[i] * digit;
            }

            return new Polynomial(multiplied);
        }

        public static Polynomial operator *(Polynomial a, Polynomial b)
        {
            ThrowIfNull(a, b, nameof(a), nameof(b));

            var multiplied = new int[a.Length + b.Length - 1];
            for (int i = 0; i < a.Length; i++)
            {
                for (int j = 0; j < b.Length; j++)
                {
                    multiplied[i + j] += a[i] * b[j];
                }
            }

            return new Polynomial(multiplied);
        }

        public static bool operator ==(Polynomial a, Polynomial b)
        {
            return Equals(a, b);
        }

        public static bool operator !=(Polynomial a, Polynomial b)
        {
            return !(a == b);
        }

        private static void ThrowIfNull(Polynomial first, Polynomial second, string firstName, string secondName)
        {
            Validator.ThrowIfNull(first, firstName);
            Validator.ThrowIfNull(second, secondName);
        }

        public override int GetHashCode()
        {
            const int seed = 31;
            int hash = 17;
            for (int i = 0; i < Length; i++)
            {
                hash = hash * seed + coefficients[i];
            }

            return hash;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Polynomial);
        }

        public bool Equals(Polynomial polynomial)
        {
            if (!ReferenceEquals(polynomial, null) && GetHashCode() == polynomial.GetHashCode())
            {
                return (ReferenceEquals(this, polynomial) || this.SequenceEqual(polynomial));
            }

            return false;
        }

        public string ToString(bool shouldNormalize)
        {
            return shouldNormalize
                ? ToNormalizedString()
                : ToString();
        }

        public override string ToString()
        {
            StringBuilder result = BuildFullString();
            RemoveFirstPlus(result);
            return result.ToString();
        }

        public IEnumerator<int> GetEnumerator()
        {
            return ((IEnumerable<int>)coefficients).GetEnumerator();
        }

        private int[] NormalizeCoefficients(int[] coefficients)
        {
            for (int index = coefficients.Length - 1; index >= 0; index--)
            {
                if (coefficients[index] != 0)
                {
                    return Truncate(coefficients, index + 1);
                }
            }

            return new int[] { 0 };
        }

        private int[] Truncate(int[] coefficients, int length)
        {
            var result = new int[length];
            Array.Copy(coefficients, result, length);
            return result;
        }

        private string ToNormalizedString()
        {
            if (Length == 1)
            {
                return this[0].ToString();
            }

            StringBuilder result = BuildNormalizedString();
            RemoveFirstPlus(result);
            return result.ToString();
        }

        private StringBuilder BuildNormalizedString()
        {
            var result = new StringBuilder();

            if (this[0] != 0)
            {
                AppendFirstMonomial(result);
            }

            for (int i = 1; i <= Degree; i++)
            {
                if (this[i] != 0)
                {
                    InsertMonomial(result, i);
                }
            }

            return result;
        }

        private void AppendFirstMonomial(StringBuilder result)
        {
            result.Append(this[0].ToString("+#;-#"));
        }

        private void InsertMonomial(StringBuilder result, int i)
        {
            InsertDegree(result, i);
            InsertMember(result, i);
        }

        private static void InsertDegree(StringBuilder result, int i)
        {
            if (i > 1)
            {
                result.Insert(0, $"^{i}");
            }
        }

        private void InsertMember(StringBuilder result, int i)
        {
            result.Insert(0,
                this[i] == -1 ? "-x" : (
                    this[i] == 1 ? "+x" : $"{this[i]:+#;-#}*x"
                ));
        }

        private void RemoveFirstPlus(StringBuilder sb)
        {
            if (sb[0] == '+')
            {
                sb.Remove(0, 1);
            }
        }

        private StringBuilder BuildFullString()
        {
            var result = new StringBuilder();
            for (int index = 0; index <= Degree; index++)
            {
                result.Append($"{this[index]:+#;-#;+0}*x^{index}");
            }

            return result;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
