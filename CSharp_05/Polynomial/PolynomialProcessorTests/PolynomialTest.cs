﻿using System;
using System.Linq;
using NUnit.Framework;
using PolynomialProcessor;

namespace PolynomialProcessorTests
{
    public class PolynomialTest
    {
        [TestCaseSource(typeof(PolynomialTestCaseSources), nameof(PolynomialTestCaseSources.CoefficientsCases))]
        public void TestGetLength_SimpleValues_CorrectValue(int[] coefficients, int degree)
        {
            Polynomial polynomial = Polynomial.CreatePolynomial(coefficients);

            Assert.AreEqual(polynomial.Length, degree + 1);
        }

        [TestCaseSource(typeof(PolynomialTestCaseSources), nameof(PolynomialTestCaseSources.CoefficientsCases))]
        public void TestGetDegree_SimpleValues_CorrectValue(int[] coefficients, int degree)
        {
            Polynomial polynomial = Polynomial.CreatePolynomial(coefficients);

            Assert.AreEqual(polynomial.Degree, degree);
        }

        [TestCaseSource(typeof(PolynomialTestCaseSources), nameof(PolynomialTestCaseSources.CoefficientsCases))]
        public void TestIndexer_SimpleValues_CorrectValues(int[] coefficients, int _)
        {
            Polynomial polynomial = Polynomial.CreatePolynomial(coefficients);

            for (int i = 0; i < polynomial.Length; i++)
            {
                Assert.AreEqual(polynomial[i], coefficients[i]);
            }
        }

        [TestCaseSource(typeof(PolynomialTestCaseSources), nameof(PolynomialTestCaseSources.CreateCoefficientsCases))]
        public void CreatePolynomial_WithCoefficients_CorrectValue(int[] coefficients, int[] expected)
        {
            Polynomial polynomial = Polynomial.CreatePolynomial(coefficients);

            Assert.IsTrue(polynomial.SequenceEqual(expected));
        }

        [Test]
        public void CreatePolynomial_Empty_CorrectValue()
        {
            Polynomial empty = Polynomial.CreatePolynomial();

            Assert.IsTrue(empty.SequenceEqual(new int[] { 0 }));
        }

        [TestCaseSource(typeof(PolynomialTestCaseSources), nameof(PolynomialTestCaseSources.PolynomialCases))]
        public void CreatePolynomial_WithPolynomial_CorrectValue(Polynomial param)
        {
            Polynomial polynomial = Polynomial.CreatePolynomial(param);

            Assert.IsTrue(param.SequenceEqual(polynomial));
        }

        [Test]
        public void TestGetHashCode_TwoPolynomials_Success(
            [Values(-9, 0, 1)] int a0,
            [Values(-1, 0, 2)] int a1,
            [Values(0, 1, 5)] int a2,
            [Values(0, 1, 4)] int a3,
            [Values(-9, 1, 2)] int b0,
            [Values(0, -1, 7)] int b1,
            [Values(0, 9, -1)] int b2,
            [Values(0, 9, 10)] int b3)
        {
            Polynomial a = Polynomial.CreatePolynomial(a0, a1, a2, a3);
            Polynomial b = Polynomial.CreatePolynomial(b0, b1, b2, b3);

            bool areEqual = a.GetHashCode() == b.GetHashCode();
            bool expected = a.SequenceEqual(b);

            Assert.AreEqual(expected, areEqual);
        }

        [Test]
        public void TestEquals_TwoPolynomials_Success(
            [Values(-9, 0, 1)] int a0,
            [Values(-1, 0, 2)] int a1,
            [Values(0, 1, 5)] int a2,
            [Values(0, 1, 4)] int a3,
            [Values(-9, 1, 2)] int b0,
            [Values(0, -1, 7)] int b1,
            [Values(0, 9, -1)] int b2,
            [Values(0, 9, 10)] int b3)
        {
            Polynomial a = Polynomial.CreatePolynomial(a0, a1, a2, a3);
            Polynomial b = Polynomial.CreatePolynomial(b0, b1, b2, b3);

            bool areEqualsFirst = a.Equals(b);
            bool areEqualsSecond = b.Equals(a);
            bool expected = a.SequenceEqual(b);

            Assert.AreEqual(expected, areEqualsFirst);
            Assert.AreEqual(expected, areEqualsSecond);
        }

        [Test]
        public void TestEquals_TwoObjects_Success(
            [Values(-9, 0, 1)] int a0,
            [Values(-1, 0, 2)] int a1,
            [Values(0, 1, 5)] int a2,
            [Values(0, 1, 4)] int a3,
            [Values(-9, 1, 2)] int b0,
            [Values(0, -1, 7)] int b1,
            [Values(0, 9, -1)] int b2,
            [Values(0, 9, 10)] int b3)
        {
            Polynomial a = Polynomial.CreatePolynomial(a0, a1, a2, a3);
            Polynomial b = Polynomial.CreatePolynomial(b0, b1, b2, b3);

            bool areEqualsFirst = a.Equals((object)b);
            bool areEqualsSecond = b.Equals((object)a);
            bool expected = a.SequenceEqual(b);

            Assert.AreEqual(expected, areEqualsFirst);
            Assert.AreEqual(expected, areEqualsSecond);
        }

        [Test]
        public void TestEquals_Null_ReturnsFalse()
        {
            Polynomial a = Polynomial.CreatePolynomial(1, 2, 3);
            Polynomial b = null;

            Assert.IsFalse(a.Equals(b));
        }

        [Test]
        public void TestEquals_NotPolynomial_ReturnsFalse()
        {
            Polynomial polynomial = Polynomial.CreatePolynomial(1, 2, 3);
            int digit = 0;

            Assert.IsFalse(polynomial.Equals(digit));
        }

        [Test]
        public void TestEquals_SameReferences_Success()
        {
            var a = Polynomial.CreatePolynomial(1, 2, 3);
            var b = a;

            Assert.IsTrue(a.Equals(b));
        }

        [Test]
        public void TestOperatorEquals_TwoPolynomials_Success(
            [Values(-9, 0, 1)] int a0,
            [Values(-1, 0, 2)] int a1,
            [Values(0, 1, 5)] int a2,
            [Values(0, 1, 4)] int a3,
            [Values(-9, 1, 2)] int b0,
            [Values(0, -1, 7)] int b1,
            [Values(0, 9, -1)] int b2,
            [Values(0, 9, 10)] int b3)
        {
            Polynomial a = Polynomial.CreatePolynomial(a0, a1, a2, a3);
            Polynomial b = Polynomial.CreatePolynomial(b0, b1, b2, b3);

            bool areEqualsFirst = a == b;
            bool areEqualsSecond = b == a;
            bool expected = a.SequenceEqual(b);

            Assert.AreEqual(expected, areEqualsFirst);
            Assert.AreEqual(expected, areEqualsSecond);
        }

        [Test]
        public void TestOperatorNotEquals_TwoPolynomials_Success(
            [Values(-9, 0, 1)] int a0,
            [Values(-1, 0, 2)] int a1,
            [Values(0, 1, 5)] int a2,
            [Values(0, 1, 4)] int a3,
            [Values(-9, 1, 2)] int b0,
            [Values(0, -1, 7)] int b1,
            [Values(0, 9, -1)] int b2,
            [Values(0, 9, 10)] int b3)
        {
            Polynomial a = Polynomial.CreatePolynomial(a0, a1, a2, a3);
            Polynomial b = Polynomial.CreatePolynomial(b0, b1, b2, b3);

            bool areNotEqualsFirst = a != b;
            bool areNotEqualsSecond = b != a;
            bool expected = !a.SequenceEqual(b);

            Assert.AreEqual(expected, areNotEqualsFirst);
            Assert.AreEqual(expected, areNotEqualsSecond);
        }

        [TestCaseSource(typeof(PolynomialTestCaseSources), nameof(PolynomialTestCaseSources.NullOperatorCases))]
        public void TestOperatorEquals_NullValues_ReturnsFalse(Polynomial a, Polynomial b, bool expected)
        {
            Assert.AreEqual(a == b, expected);
        }

        [TestCaseSource(typeof(PolynomialTestCaseSources), nameof(PolynomialTestCaseSources.NullOperatorCases))]
        public void TestOperatorNotEquals_NullValues_ReturnsTrue(Polynomial a, Polynomial b, bool notExpected)
        {
            Assert.AreNotEqual(a != b, notExpected);
        }


        [TestCaseSource(typeof(PolynomialTestCaseSources), nameof(PolynomialTestCaseSources.GetSumCases))]
        public int[] TestGetSum_SimpleValues_CorrectResult(Polynomial a, Polynomial b)
        {
            return (a + b).ToArray();
        }

        [TestCaseSource(typeof(PolynomialTestCaseSources), nameof(PolynomialTestCaseSources.NullCases))]
        public void TestGetSum_TwoNulls_ExceptionThrown(Polynomial a, Polynomial b)
        {
            Assert.Throws<ArgumentNullException>(() => _ = a + b);
        }

        [TestCaseSource(typeof(PolynomialTestCaseSources), nameof(PolynomialTestCaseSources.GetDiffCases))]
        public int[] TestGetDiff_SimpleValues_CorrectResult(Polynomial a, Polynomial b)
        {
            return (a - b).ToArray();
        }

        [TestCaseSource(typeof(PolynomialTestCaseSources), nameof(PolynomialTestCaseSources.NullCases))]
        public void TestGetDiff_TwoNulls_ExceptionThrown(Polynomial a, Polynomial b)
        {
            Assert.Throws<ArgumentNullException>(() => _ = a - b);
        }

        [TestCaseSource(typeof(PolynomialTestCaseSources), nameof(PolynomialTestCaseSources.GetPolynomialMulCases))]
        public int[] TestGetMul_SimpleValues_CorrectResult(Polynomial a, Polynomial b)
        {
            return (a * b).ToArray();
        }

        [TestCaseSource(typeof(PolynomialTestCaseSources), nameof(PolynomialTestCaseSources.NullCases))]
        public void TestGetMul_TwoNulls_ExceptionThrown(Polynomial a, Polynomial b)
        {
            Assert.Throws<ArgumentNullException>(() => _ = a * b);
        }

        [TestCaseSource(typeof(PolynomialTestCaseSources), nameof(PolynomialTestCaseSources.GetDigitalMulCases))]
        public int[] GetDigitalMul_SimpleValues_CorrectResult(Polynomial a, int digit)
        {
            return (a * digit).ToArray();
        }

        [Test]
        public void TestGetMul_Null_ExceptionThrown()
        {
            Polynomial polynomial = null;
            int multiplier = 0;

            Assert.Throws<ArgumentNullException>(() => _ = polynomial * multiplier);
        }

        [TestCaseSource(typeof(PolynomialTestCaseSources), nameof(PolynomialTestCaseSources.GetToStringCases))]
        public string TestToString_SimpleValues_CorrectFormatedString(Polynomial polynomial)
        {
            return polynomial.ToString(false);
        }

        [TestCaseSource(typeof(PolynomialTestCaseSources), nameof(PolynomialTestCaseSources.GetToNornamalizedStringCases))]
        public string TestToNormalizedString_SimpleValues_CorrectFormatedString(Polynomial polynomial)
        {
            return polynomial.ToString(true);
        }
    }
}
