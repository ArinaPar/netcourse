﻿using System.Collections;
using NUnit.Framework;
using PolynomialProcessor;

namespace PolynomialProcessorTests
{
    internal class PolynomialTestCaseSources
    {

        internal static readonly object[] CoefficientsCases =
        {
            new object[] { new int[] {6, 9, -3, 80, 21}, 4 },
            new object[] { new int[] {3 ,5, 0, 0, 0, 1}, 5 },
            new object[] { new int[] { 3, 5, 0, 0, 0, 0 }, 1},
            new object[] { new int[] { 3, 5, 0, -3, 0, 0 }, 3},
            new object[] { new int[] { 9, 3, 2}, 2 },
            new object[] { new int[] { 0 }, 0},
            new object[] { new int[] { 0, 0, 3 }, 2},
            new object[] { new int[] { 0, 0, 0, 0 }, 0},
            new object[] { new int[] { -10000, -3, 3, 10000 }, 3}
        };

        internal static readonly object[] CreateCoefficientsCases =
        {
            new object[] { new int[] { 6, 9, -3, 80, 21 }, new int[] { 6, 9, -3, 80, 21 } },
            new object[] { new int[] { 3 ,5, 0, 0, 0, 1 }, new int[] { 3 ,5, 0, 0, 0, 1 }},
            new object[] { new int[] { 3, 5, 0, 0, 0, 0 }, new int[] { 3, 5 }},
            new object[] { new int[] { 3, 5, 0, -3, 0, 0 }, new int[] { 3, 5, 0, -3 }},
            new object[] { new int[] { 9, 3, 2 }, new int[] { 9, 3, 2 }},
            new object[] { new int[] { 0 }, new int[] { 0 }},
            new object[] { new int[] { 0, 0, 3 }, new int[] { 0, 0, 3 }},
            new object[] { new int[] { 0, 0, 0, 0 }, new int[] { 0 }},
            new object[] { new int[] { -10000, -3, 3, 10000 }, new int[] { -10000, -3, 3, 10000 }}
        };

        internal static readonly object[] PolynomialCases =
        {
            Polynomial.CreatePolynomial(),
            Polynomial.CreatePolynomial(new int[3]),
            Polynomial.CreatePolynomial( 1, 2, 3, 4, 5, 6, 7, 8, 9, 10),
            Polynomial.CreatePolynomial( -100, -200, -300, 100, 200 ),
            Polynomial.CreatePolynomial( 0 , 0, 4, 400, 0, 1 ),
        };

        public static readonly object[] NullCases =
        {
            new object[] {Polynomial.CreatePolynomial(1, 2, 3), null},
            new object[] {null, Polynomial.CreatePolynomial(1, 2, 3)},
            new object[] {null, null}
        };


        public static readonly object[] NullOperatorCases =
        {
            new object[] {null, null, true},
            new object[] {Polynomial.CreatePolynomial(1, 2, 3), null, false},
            new object[] {null, Polynomial.CreatePolynomial(1, 2, 3), false}
        };

        internal static IEnumerable GetSumCases
        {
            get
            {
                yield return new TestCaseData(
                    Polynomial.CreatePolynomial(1, 2, 3),
                    Polynomial.CreatePolynomial(4, 5, 6))
                    .Returns(new int[] { 5, 7, 9 });
                yield return new TestCaseData(
                    Polynomial.CreatePolynomial(1, 2, 3),
                    Polynomial.CreatePolynomial(-4, -5, -6))
                    .Returns(new int[] { -3, -3, -3 });
                yield return new TestCaseData(
                    Polynomial.CreatePolynomial(1, 2, 3),
                    Polynomial.CreatePolynomial(4, 5, 0, 6, 7))
                    .Returns(new int[] { 5, 7, 3, 6, 7 });
                yield return new TestCaseData(
                    Polynomial.CreatePolynomial(1, 2, 3, 4, 5),
                    Polynomial.CreatePolynomial(6, 7, 8))
                    .Returns(new int[] { 7, 9, 11, 4, 5 });
                yield return new TestCaseData(
                    Polynomial.CreatePolynomial(1, 2, 3, 0, 0),
                    Polynomial.CreatePolynomial(0, 4, 5, 6, 0))
                    .Returns(new int[] { 1, 6, 8, 6 });
                yield return new TestCaseData(
                    Polynomial.CreatePolynomial(1, 2, 3),
                    Polynomial.CreatePolynomial(-1, -2, -3))
                    .Returns(new int[] { 0 });
            }
        }

        internal static IEnumerable GetDiffCases
        {
            get
            {
                yield return new TestCaseData(
                    Polynomial.CreatePolynomial(1, 2, 3),
                    Polynomial.CreatePolynomial(4, 5, 6))
                    .Returns(new int[] { -3, -3, -3 });
                yield return new TestCaseData(
                    Polynomial.CreatePolynomial(1, 2, 3),
                    Polynomial.CreatePolynomial(-4, -5, -6))
                    .Returns(new int[] { 5, 7, 9 });
                yield return new TestCaseData(
                    Polynomial.CreatePolynomial(1, 2, 3),
                    Polynomial.CreatePolynomial(4, 5, 0, 6, 7))
                    .Returns(new int[] { -3, -3, 3, -6, -7 });
                yield return new TestCaseData(
                    Polynomial.CreatePolynomial(1, 2, 3, 4, 5),
                    Polynomial.CreatePolynomial(6, 7, 8))
                    .Returns(new int[] { -5, -5, -5, 4, 5 });
                yield return new TestCaseData(
                    Polynomial.CreatePolynomial(1, 2, 3, 0, 0),
                    Polynomial.CreatePolynomial(0, 4, 5, 6, 0))
                    .Returns(new int[] { 1, -2, -2, -6 });
                yield return new TestCaseData(
                    Polynomial.CreatePolynomial(1, 2, 3),
                    Polynomial.CreatePolynomial(1, 2, 3))
                    .Returns(new int[] { 0 });
            }
        }

        internal static IEnumerable GetPolynomialMulCases
        {
            get
            {
                yield return new TestCaseData(
                    Polynomial.CreatePolynomial(1, 2, 3),
                    Polynomial.CreatePolynomial(4, 5, 6))
                    .Returns(new int[] { 4, 13, 28, 27, 18 });
                yield return new TestCaseData(
                    Polynomial.CreatePolynomial(1, 2, 3),
                    Polynomial.CreatePolynomial(-4, -5, -6))
                    .Returns(new int[] { -4, -13, -28, -27, -18 });
                yield return new TestCaseData(
                    Polynomial.CreatePolynomial(1, 2, 3),
                    Polynomial.CreatePolynomial(4, 5, 0, 6, 7))
                    .Returns(new int[] { 4, 13, 22, 21, 19, 32, 21 });
                yield return new TestCaseData(
                    Polynomial.CreatePolynomial(1, 2, 3, 4, 5),
                    Polynomial.CreatePolynomial(6, 7, 8))
                    .Returns(new int[] { 6, 19, 40, 61, 82, 67, 40 });
                yield return new TestCaseData(
                    Polynomial.CreatePolynomial(1, 2, 3, 0, 0),
                    Polynomial.CreatePolynomial(0, 4, 5, 6, 0))
                    .Returns(new int[] { 0, 4, 13, 28, 27, 18 });
                yield return new TestCaseData(
                    Polynomial.CreatePolynomial(1, 2, 3),
                    Polynomial.CreatePolynomial(1, 2, 3))
                    .Returns(new int[] { 1, 4, 10, 12, 9 });
            }
        }

        internal static IEnumerable GetDigitalMulCases
        {
            get
            {
                yield return new TestCaseData(Polynomial.CreatePolynomial(1, 2, 3), 2).Returns(new int[] { 2, 4, 6 });
                yield return new TestCaseData(Polynomial.CreatePolynomial(1, 2, 3), -2).Returns(new int[] { -2, -4, -6 });
                yield return new TestCaseData(Polynomial.CreatePolynomial(), 2).Returns(new int[] { 0 });
                yield return new TestCaseData(Polynomial.CreatePolynomial(0, 0, 0), -2).Returns(new int[] { 0 });
                yield return new TestCaseData(Polynomial.CreatePolynomial(1, 2, 3), 0).Returns(new int[] { 0 });
            }
        }

        internal static IEnumerable GetToStringCases
        {
            get
            {
                yield return new TestCaseData(Polynomial.CreatePolynomial(1, 2, 3)).Returns("1*x^0+2*x^1+3*x^2");
                yield return new TestCaseData(Polynomial.CreatePolynomial(-1, -2, 3)).Returns("-1*x^0-2*x^1+3*x^2");
                yield return new TestCaseData(Polynomial.CreatePolynomial(1, 2, 0)).Returns("1*x^0+2*x^1");
                yield return new TestCaseData(Polynomial.CreatePolynomial()).Returns("0*x^0");
                yield return new TestCaseData(Polynomial.CreatePolynomial(0, 0, 0)).Returns("0*x^0");
                yield return new TestCaseData(Polynomial.CreatePolynomial(0, -0, -0)).Returns("0*x^0");
                yield return new TestCaseData(Polynomial.CreatePolynomial(0, 6, 8)).Returns("0*x^0+6*x^1+8*x^2");
                yield return new TestCaseData(Polynomial.CreatePolynomial(33, 0, 6, 8)).Returns("33*x^0+0*x^1+6*x^2+8*x^3");
            }
        }

        internal static IEnumerable GetToNornamalizedStringCases
        {
            get
            {
                yield return new TestCaseData(Polynomial.CreatePolynomial(0)).Returns("0");
                yield return new TestCaseData(Polynomial.CreatePolynomial(-0)).Returns("0");
                yield return new TestCaseData(Polynomial.CreatePolynomial(-0, 0)).Returns("0");
                yield return new TestCaseData(Polynomial.CreatePolynomial(0, 1)).Returns("x");
                yield return new TestCaseData(Polynomial.CreatePolynomial(0, -1)).Returns("-x");
                yield return new TestCaseData(Polynomial.CreatePolynomial(-1, -1)).Returns("-x-1");
                yield return new TestCaseData(Polynomial.CreatePolynomial(0, 31, -21)).Returns("-21*x^2+31*x");
                yield return new TestCaseData(Polynomial.CreatePolynomial(-0, 1, -0)).Returns("x");
                yield return new TestCaseData(Polynomial.CreatePolynomial(0, -0, -0)).Returns("0");
                yield return new TestCaseData(Polynomial.CreatePolynomial(0, -0, -0, -10)).Returns("-10*x^3");
                yield return new TestCaseData(Polynomial.CreatePolynomial(0, -0, 1)).Returns("x^2");
                yield return new TestCaseData(Polynomial.CreatePolynomial(0, -0, -1)).Returns("-x^2");
                yield return new TestCaseData(Polynomial.CreatePolynomial(-15, 0, -0, -0, -0)).Returns("-15");
                yield return new TestCaseData(Polynomial.CreatePolynomial(-10, 0, -0, -13)).Returns("-13*x^3-10");
                yield return new TestCaseData(Polynomial.CreatePolynomial(10, 11, 12, 13, 14, 15)).Returns("15*x^5+14*x^4+13*x^3+12*x^2+11*x+10");
                yield return new TestCaseData(Polynomial.CreatePolynomial(-10, -11, -12, -13, -14, -15)).Returns("-15*x^5-14*x^4-13*x^3-12*x^2-11*x-10");
            }
        }
    }
}
