﻿using Helpers;

namespace VectorProcessor
{
    public class Vector
    {
        public float X { get; }
        public float Y { get; }
        public float Z { get; }

        public Vector(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public static Vector operator +(Vector a, Vector b)
        {
            ThrowIfNull(a, b, nameof(a), nameof(b));
            return new Vector(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
        }

        public static Vector operator -(Vector a, Vector b)
        {
            ThrowIfNull(a, b, nameof(a), nameof(b));
            return new Vector(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
        }

        public static float operator *(Vector a, Vector b)
        {
            ThrowIfNull(a, b, nameof(a), nameof(b));
            return a.X * b.X + a.Y * b.Y + a.Z * b.Z;
        }

        public static Vector CrossProduct(Vector a, Vector b)
        {
            ThrowIfNull(a, b, nameof(a), nameof(b));

            float x = a.Y * b.Z - b.Y * a.Z;
            float y = b.X * a.Z - a.X * b.Z;
            float z = a.X * b.Y - b.X * a.Y;
            return new Vector(x, y, z);
        }

        public static bool operator ==(Vector a, Vector b)
        {
            return Equals(a, b);
        }

        public static bool operator !=(Vector a, Vector b)
        {
            return !(a == b);
        }

        private static void ThrowIfNull(Vector a, Vector b, string aName, string bName)
        {
            Validator.ThrowIfNull(a, aName);
            Validator.ThrowIfNull(b, bName);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Vector);
        }

        public bool Equals(Vector vector)
        {
            if (!ReferenceEquals(vector, null) && GetHashCode() == vector.GetHashCode())
            {
                return (ReferenceEquals(this, vector)
                    || X == vector.X && Y == vector.Y && Z == vector.Z);
            }

            return false;
        }

        public override int GetHashCode()
        {
            int hash = 0;
            hash ^= CalculateFieldHashCode(X, hash);
            hash ^= CalculateFieldHashCode(Y, hash);
            hash ^= CalculateFieldHashCode(Z, hash);
            return hash;
        }

        public override string ToString()
        {
            return $"{X}, {Y}, {Z}";
        }

        private int CalculateFieldHashCode(float param, int hashCode)
        {
            return (hashCode << 5) + (hashCode >> 2) + param.GetHashCode();
        }
    }
}
