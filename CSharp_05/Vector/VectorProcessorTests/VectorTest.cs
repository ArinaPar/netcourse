﻿using System;
using NUnit.Framework;
using VectorProcessor;

namespace VectorProcessorTests
{
    [TestFixture]
    public class VectorTest
    {
        [TestCaseSource(typeof(VectorTestCaseSources), nameof(VectorTestCaseSources.ParametersCases))]
        public void TestGetXYZ_SimpleValues_CorrectValues(float x, float y, float z)
        {
            var vector = new Vector(x, y, z);

            Assert.AreEqual(x, vector.X);
            Assert.AreEqual(y, vector.Y);
            Assert.AreEqual(z, vector.Z);
        }

        [Test]
        public void TestGetHashCode_TwoVectors_Success(
            [Values(-9f, 0, 1.11f)] float x1,
            [Values(-1f, 0, 1.11f)] float y1,
            [Values(2f, 0, 1.13f)] float z1,
            [Values(-9f, 0, 1.001f)] float x2,
            [Values(-1f, 0, 1.33f)] float y2,
            [Values(2f, 0, 1.11f)] float z2)
        {
            var a = new Vector(x1, y1, z1);
            var b = new Vector(x2, y2, z2);

            bool expected = x1 == x2 && y1 == y2 && z1 == z2;
            bool actual = a.GetHashCode() == b.GetHashCode();

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void TestEquals_TwoVectors_Success(
            [Values(-9f, 0, 1.11f)] float x1,
            [Values(-1f, 0, 1.11f)] float y1,
            [Values(2f, 0, 1.13f)] float z1,
            [Values(-9f, 0, 1.001f)] float x2,
            [Values(-1f, 0, 1.33f)] float y2,
            [Values(2f, 0, 1.11f)] float z2)
        {
            var a = new Vector(x1, y1, z1);
            var b = new Vector(x2, y2, z2);

            bool actual = a.Equals(b);
            bool expected = x1 == x2 && y1 == y2 && z1 == z2;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void TestEquals_SameReferences_Success(
            [Values(-9f, 0, 1.11f)] float x1,
            [Values(-1f, 0, 1.11f)] float y1,
            [Values(2f, 0, 1.13f)] float z1)
        {
            var a = new Vector(x1, y1, z1);
            var b = a;

            Assert.IsTrue(a.Equals(b));
        }

        [Test]
        public void TestEquals_TwoObjects_Success(
            [Values(-9f, 0, 1.11f)] float x1,
            [Values(-1f, 0, 1.11f)] float y1,
            [Values(2f, 0, 1.13f)] float z1,
            [Values(-9f, 0, 1.001f)] float x2,
            [Values(-1f, 0, 1.33f)] float y2,
            [Values(2f, 0, 1.11f)] float z2)
        {
            var a = new Vector(x1, y1, z1);
            var b = new Vector(x2, y2, z2);

            bool actual = a.Equals((object)b);
            bool expected = x1 == x2 && y1 == y2 && z1 == z2;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void TestEquals_Null_ReturnsFalse()
        {
            Vector vector = new Vector(1, 2, 3);
            object o = null;

            bool areEqual = vector.Equals(o);

            Assert.IsFalse(areEqual);
        }

        [Test]
        public void TestEquals_NotVector_ReturnsFalse()
        {
            Vector vector = new Vector(1, 2, 3);
            int notVector = 0;

            bool areEqual = vector.Equals(notVector);

            Assert.IsFalse(areEqual);
        }

        [Test]
        public void TestEqualsOperator_TwoVectors_Success(
            [Values(-9f, 0, 1.11f)] float x1,
            [Values(-1f, 0, 1.11f)] float y1,
            [Values(2f, 0, 1.13f)] float z1,
            [Values(-9f, 0, 1.001f)] float x2,
            [Values(-1f, 0, 1.33f)] float y2,
            [Values(2f, 0, 1.11f)] float z2)
        {
            var a = new Vector(x1, y1, z1);
            var b = new Vector(x2, y2, z2);

            bool actual = a == b;
            bool expected = x1 == x2 && y1 == y2 && z1 == z2;

            Assert.AreEqual(expected, actual);
        }

        [TestCaseSource(typeof(VectorTestCaseSources), nameof(VectorTestCaseSources.NullOperatorCases))]
        public void TestEqualsOperator_NullValues_ReturnsFalse(Vector a, Vector b, bool expected)
        {
            Assert.AreEqual(a == b, expected);
        }

        [Test]
        public void TestNotEqualsOperator_TwoVectors_Success(
           [Values(-9f, 0, 1.11f)] float x1,
           [Values(-1f, 0, 1.11f)] float y1,
           [Values(2f, 0, 1.13f)] float z1,
           [Values(-9f, 0, 1.001f)] float x2,
           [Values(-1f, 0, 1.33f)] float y2,
           [Values(2f, 0, 1.11f)] float z2)
        {
            var a = new Vector(x1, y1, z1);
            var b = new Vector(x2, y2, z2);

            bool actual = a != b;
            bool expected = x1 != x2 || y1 != y2 || z1 != z2;

            Assert.AreEqual(expected, actual);
        }

        [TestCaseSource(typeof(VectorTestCaseSources), nameof(VectorTestCaseSources.NullOperatorCases))]
        public void TestNotEqualsOperator_NullValues_ReturnsFalse(Vector a, Vector b, bool notExpected)
        {
            Assert.AreNotEqual(a != b, notExpected);
        }

        [TestCaseSource(typeof(VectorTestCaseSources), nameof(VectorTestCaseSources.GetSumCases))]
        public Vector TestGetSum_SimpleValues_CorrectResult(Vector a, Vector b)
        {
            return a + b;
        }

        [TestCaseSource(typeof(VectorTestCaseSources), nameof(VectorTestCaseSources.NullCases))]
        public void TestGetSum_NullValues_ExceptionThrown(Vector a, Vector b)
        {
            Assert.Throws<ArgumentNullException>(() => _ = a + b);
        }

        [TestCaseSource(typeof(VectorTestCaseSources), nameof(VectorTestCaseSources.GetDiffCases))]
        public Vector TestGetDiff_SimpleValues_CorrectResult(Vector a, Vector b)
        {
            return a - b;
        }

        [TestCaseSource(typeof(VectorTestCaseSources), nameof(VectorTestCaseSources.NullCases))]
        public void TestGetDiff_NullValues_ExceptionThrown(Vector a, Vector b)
        {
            Assert.Throws<ArgumentNullException>(() => _ = a - b);
        }

        [TestCaseSource(typeof(VectorTestCaseSources), nameof(VectorTestCaseSources.GetMulCases))]
        public float TestGetMul_SimpleValues_CorrectResult(Vector a, Vector b)
        {
            return a * b;
        }

        [TestCaseSource(typeof(VectorTestCaseSources), nameof(VectorTestCaseSources.NullCases))]
        public void TestGetMul_NullValues_ExceptionThrown(Vector a, Vector b)
        {
            Assert.Throws<ArgumentNullException>(() => _ = a * b);
        }

        [TestCaseSource(typeof(VectorTestCaseSources), nameof(VectorTestCaseSources.GetCrossProductCases))]
        public Vector TestGetCrossProduct_SimpleValues_CorrectResult(Vector a, Vector b)
        {
            return Vector.CrossProduct(a, b);
        }

        [TestCaseSource(typeof(VectorTestCaseSources), nameof(VectorTestCaseSources.NullCases))]
        public void TestGetCrossProduct_NullValues_ExceptionThrown(Vector a, Vector b)
        {
            Assert.Throws<ArgumentNullException>(() => Vector.CrossProduct(a, b));
        }

        [TestCaseSource(typeof(VectorTestCaseSources), nameof(VectorTestCaseSources.ParametersCases))]
        public void TestToString_SimpleValues_CorrectFormatedString(float x, float y, float z)
        {
            string expected = $"{x}, {y}, {z}";
            var vector = new Vector(x, y, z);

            string actual = vector.ToString();

            Assert.AreEqual(actual, expected);
        }
    }
}
