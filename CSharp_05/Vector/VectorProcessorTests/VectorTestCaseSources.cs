﻿using System.Collections;
using NUnit.Framework;
using VectorProcessor;

namespace VectorProcessorTests
{
    class VectorTestCaseSources
    {
        public static readonly object[] ParametersCases =
        {
            new object[] {12.65f, 32f, 199.02f},
            new object[] {12.65f, 32f, 0f},
            new object[] {199.02f, -32f, 199.02f},
            new object[] {0f, 32f, -199.02f},
            new object[] {-12.65f, 0f, 199.02f},
            new object[] {0f, 32f, 1f}
        };

        public static readonly object[] NullCases =
        {
            new object[] {new Vector(1, 2, 3), null},
            new object[] {null, new Vector(1, 2, 3)},
            new object[] {null, null}
        };


        public static readonly object[] NullOperatorCases =
        {
            new object[] {null, null, true},
            new object[] {new Vector(1, 2, 3), null, false},
            new object[] {null, new Vector(1, 2, 3), false}
        };

        public static IEnumerable GetSumCases
        {
            get
            {
                yield return new TestCaseData(new Vector(1, 2, 3), new Vector(4, 5, 6)).Returns(new Vector(5, 7, 9));
                yield return new TestCaseData(new Vector(-1, -2, -3), new Vector(-4, -5, -6)).Returns(new Vector(-5, -7, -9));
                yield return new TestCaseData(new Vector(1, 2, 3), new Vector(0, 0, 0)).Returns(new Vector(1, 2, 3));
                yield return new TestCaseData(new Vector(-100, -2, 0), new Vector(0, 5, 6)).Returns(new Vector(-100, 3, 6));
            }
        }

        public static IEnumerable GetDiffCases
        {
            get
            {
                yield return new TestCaseData(new Vector(1, 2, 3), new Vector(4, 5, 6)).Returns(new Vector(-3, -3, -3));
                yield return new TestCaseData(new Vector(-1, -2, -3), new Vector(-4, -5, -6)).Returns(new Vector(3, 3, 3));
                yield return new TestCaseData(new Vector(1, 2, 3), new Vector(0, 0, 0)).Returns(new Vector(1, 2, 3));
                yield return new TestCaseData(new Vector(-100, -2, 0), new Vector(0, 5, 6)).Returns(new Vector(-100, -7, -6));
            }
        }

        public static IEnumerable GetMulCases
        {
            get
            {
                yield return new TestCaseData(new Vector(1, 2, 3), new Vector(4, 5, 6)).Returns(32f);
                yield return new TestCaseData(new Vector(-1, -2, -3), new Vector(-4, -5, -6)).Returns(32f);
                yield return new TestCaseData(new Vector(1, 2, 3), new Vector(0, 0, 0)).Returns(0f);
                yield return new TestCaseData(new Vector(-100, -2, 0), new Vector(0, 5, 6)).Returns(-10f);
            }
        }

        public static IEnumerable GetCrossProductCases
        {
            get
            {
                yield return new TestCaseData(new Vector(1, 2, 3), new Vector(4, 5, 6)).Returns(new Vector(-3, 6, -3));
                yield return new TestCaseData(new Vector(-1, -2, -3), new Vector(-4, -5, -6)).Returns(new Vector(-3, 6, -3));
                yield return new TestCaseData(new Vector(1, 2, 3), new Vector(0, 0, 0)).Returns(new Vector(0f, 0f, 0f));
                yield return new TestCaseData(new Vector(-100, -2, 0), new Vector(0, 5, 6)).Returns(new Vector(-12, 600, -500));
            }
        }
    }
}
