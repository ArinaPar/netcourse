﻿namespace AbstractFactory
{
    public interface IStudentFactory
    {
        public IBadMark CreateBadMark();
        public IGoodMark CreateGoodMark();
    }
}
