﻿using System;

namespace AbstractFactory.SchoolStudentFactory
{
    public class BadMarkSchool : IBadMark
    {
        public void Get()
        {
            Console.WriteLine("Oops... Mom will swear.");
        }
    }
}
