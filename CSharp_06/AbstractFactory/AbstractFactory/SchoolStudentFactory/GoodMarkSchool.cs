﻿using System;

namespace AbstractFactory.SchoolStudentFactory
{
    public class GoodMarkSchool : IGoodMark
    {
        public void Get()
        {
            Console.WriteLine("Hooray! Mom will praise you.");
        }
    }
}
