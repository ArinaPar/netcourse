﻿namespace AbstractFactory.SchoolStudentFactory
{
    public class SchoolStudentFactory : IStudentFactory
    {
        public IBadMark CreateBadMark()
        {
            return new BadMarkSchool();
        }

        public IGoodMark CreateGoodMark()
        {
            return new GoodMarkSchool();
        }
    }
}
