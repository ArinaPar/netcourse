﻿using System;

namespace AbstractFactory.UniversityStudentFactory
{
    public class BadMarkUniversity : IBadMark
    {
        public void Get()
        {
            Console.WriteLine("Oops... You are going to army.");
        }
    }
}
