﻿using System;

namespace AbstractFactory.UniversityStudentFactory
{
    public class GoodMarkUniversity : IGoodMark
    {
        public void Get()
        {
            Console.WriteLine("Hooray! You will get grants.");
        }
    }
}
