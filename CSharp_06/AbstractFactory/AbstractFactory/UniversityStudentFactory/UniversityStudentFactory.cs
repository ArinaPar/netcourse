﻿namespace AbstractFactory.UniversityStudentFactory
{
    public class UniversityStudentFactory : IStudentFactory
    {
        public IBadMark CreateBadMark()
        {
            return new BadMarkUniversity();
        }

        public IGoodMark CreateGoodMark()
        {
            return new GoodMarkUniversity();
        }
    }
}
