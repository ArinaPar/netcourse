﻿using AbstractFactory;
using AbstractFactory.SchoolStudentFactory;
using AbstractFactory.UniversityStudentFactory;

namespace AbstractFactoryUI
{
    public class Program
    {
        private static IStudentFactory[] factories =
        {
            new SchoolStudentFactory(),
            new UniversityStudentFactory()
        };

        public static void Main()
        {
            foreach (var f in factories)
            {
                GetAllMarks(new Student(f));
            }
        }

        private static void GetAllMarks(Student student)
        {
            student.GetBadMark();
            student.GetGoodMark();
        }
    }
}
