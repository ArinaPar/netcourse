﻿using AbstractFactory;

namespace AbstractFactoryUI
{
    public class Student
    {
        private readonly IBadMark badMark;
        private readonly IGoodMark goodMark;

        public Student(IStudentFactory studentFactory)
        {
            badMark = studentFactory.CreateBadMark();
            goodMark = studentFactory.CreateGoodMark();
        }

        public void GetBadMark()
        {
            badMark.Get();
        }

        public void GetGoodMark()
        {
            goodMark.Get();
        }
    }
}
