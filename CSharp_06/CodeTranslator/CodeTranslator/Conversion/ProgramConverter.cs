﻿using System;

namespace CodeTranslator.Conversion
{
    public class ProgramConverter : IConvertible
    {
        public string ConvertToCSharp(string input)
        {
            Console.WriteLine("Converting to C#...");
            return "Converted to C#";
        }

        public string ConvertToVB(string input)
        {
            Console.WriteLine("Converting to VB...");
            return "Converted to VB";
        }
    }
}
