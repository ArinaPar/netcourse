﻿using System;

namespace CodeTranslator.Conversion
{
    public class ProgramHelper : ProgramConverter, ICodeChecker
    {
        public bool CheckCodeSyntax(string input, string language)
        {
            Console.WriteLine($"{language} syntax checking...");
            return input.Contains(language);
        }
    }
}
