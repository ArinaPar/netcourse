﻿namespace CodeTranslator
{
    public interface ICodeChecker
    {
        bool CheckCodeSyntax(string input, string language);
    }
}
