﻿using System;
using CodeTranslator;
using CodeTranslator.Conversion;
using IConvertible = CodeTranslator.IConvertible;

namespace CodeTranslatorUI
{
    public class Program
    {
        private const string CSharp = "C#";
        private const string VB = "VB";

        private static readonly IConvertible[] Convertibles =
        {
            new ProgramConverter(),
            new ProgramConverter(),
            new ProgramHelper(),
            new ProgramConverter(),
            new ProgramHelper(),
            new ProgramHelper()
        };

        public static void Main()
        {
            string code = "This is C# code";

            foreach (var convertible in Convertibles)
            {
                if (convertible is ICodeChecker codeChecker)
                {
                    WriteConvertedWithCheck(code, codeChecker);
                }
                else
                {
                    WriteConvertedToCSharp(code, convertible);
                    WriteConvertedToVB(code, convertible);
                }

                WriteStringSeparator();
            }
        }

        private static void WriteConvertedWithCheck(string input, ICodeChecker codeChecker)
        {
            if (codeChecker.CheckCodeSyntax(input, CSharp))
            {
                WriteConvertedToVB(input, (IConvertible)codeChecker);
            }
            else if (codeChecker.CheckCodeSyntax(input, VB))
            {
                WriteConvertedToCSharp(input, (IConvertible)codeChecker);
            }
            else
            {
                WriteUnableToConvert();
            }
        }

        private static void WriteConvertedToCSharp(string input, IConvertible convertible)
        {
            Console.WriteLine(convertible.ConvertToCSharp(input));
        }

        private static void WriteConvertedToVB(string input, IConvertible convertible)
        {
            Console.WriteLine(convertible.ConvertToVB(input));
        }

        private static void WriteUnableToConvert()
        {
            Console.WriteLine("Unable to convert input");
        }

        private static void WriteStringSeparator()
        {
            Console.WriteLine();
        }
    }
}
