This program converts user's code in C# code or VB code. 
It can use syntax checking of user's code before converting or not.
If syntax checking returns false, the code is not converting.