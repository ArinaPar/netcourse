﻿namespace MatrixProcessor
{
    public static class ArrayExtention
    {
        public static int GetRowsCount(this int[,] array) => array.GetLength(0);
        public static int GetColumnsCount(this int[,] array) => array.GetLength(1);

        public static bool EqualTo(this int[,] a, int[,] b)
        {
            if (b is null || a.GetRowsCount() != b.GetRowsCount() || a.GetColumnsCount() != b.GetColumnsCount())
            {
                return false;
            }

            for (int i = 0; i < a.GetRowsCount(); i++)
            {
                for (int j = 0; j < a.GetColumnsCount(); j++)
                {
                    if (a[i, j] != b[i, j])
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}
