﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace MatrixProcessor
{
    [Serializable]
    public class InvalidMatrixOperationException : Exception
    {
        public int RowsA { get; private set; }
        public int ColumnsA { get; private set; }
        public int RowsB { get; private set; }
        public int ColumnsB { get; private set; }

        public InvalidMatrixOperationException() { }

        public InvalidMatrixOperationException(string message) : base(message) { }

        public InvalidMatrixOperationException(string message, int rowsA, int columnsA, int rowsB, int columnsB) : base(message)
        {
            Init(rowsA, columnsA, rowsB, columnsB);
        }

        public InvalidMatrixOperationException(string message, Exception innerException) : base(message, innerException) { }

        public InvalidMatrixOperationException(string message, Exception innerException, int rowsA, int columnsA, int rowsB, int columnsB) : base(message, innerException)
        {
            Init(rowsA, columnsA, rowsB, columnsB);
        }

        public InvalidMatrixOperationException(int rowsA, int columnsA, int rowsB, int columnsB) : base()
        {
            Init(rowsA, columnsA, rowsB, columnsB);
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected InvalidMatrixOperationException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            RowsA = (int)info.GetValue("RowsA", typeof(int));
            ColumnsA = (int)info.GetValue("ColumnsA", typeof(int));
            RowsB = (int)info.GetValue("RowsB", typeof(int));
            ColumnsB = (int)info.GetValue("ColumnsB", typeof(int));
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("RowsA", RowsA, typeof(int));
            info.AddValue("ColumnsA", ColumnsA, typeof(int));
            info.AddValue("RowsB", RowsB, typeof(int));
            info.AddValue("ColumnsB", ColumnsB, typeof(int));
            base.GetObjectData(info, context);
        }

        public override string Message
        {
            get
            {
                string s = base.Message;
                if (RowsA > 0 && ColumnsA > 0 && RowsB > 0 && ColumnsB > 0)
                {
                    return s + $"\nUnable to perform the operation. Sizes of operands are not matching: they were {RowsA}x{ColumnsA} and {RowsB}x{ColumnsB}";
                }

                return s;
            }
        }

        private void Init(int rowsA, int columnsA, int rowsB, int columnsB)
        {
            RowsA = rowsA;
            ColumnsA = columnsA;
            RowsB = rowsB;
            ColumnsB = columnsB;
        }
    }
}
