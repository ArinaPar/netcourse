﻿using System;
using System.Text;
using Helpers;

namespace MatrixProcessor
{
    public class Matrix
    {
        private readonly int[,] elements;

        public int RowsCount => elements.GetRowsCount();

        public int ColumnsCount => elements.GetColumnsCount();

        public int this[int i, int j]
        {
            get => elements[i, j];
            set => elements[i, j] = value;
        }

        private Matrix(int rows, int columns)
        {
            elements = new int[rows, columns];
        }

        private Matrix(int[,] elements)
        {
            this.elements = new int[elements.GetRowsCount(), elements.GetColumnsCount()];
            Array.Copy(elements, this.elements, elements.Length);
        }

        public static Matrix Create(int rows, int columns)
        {
            ThrowIfNotPositive(rows, nameof(rows));
            ThrowIfNotPositive(columns, nameof(columns));

            return new Matrix(rows, columns);
        }

        public static Matrix Create(int[,] elements)
        {
            Validator.ThrowIfNull(elements, nameof(elements));

            return new Matrix(elements);
        }

        public static Matrix operator +(Matrix a, Matrix b)
        {
            ThrowIfNull(a, b, nameof(a), nameof(b));
            ThrowIfDifferentSizes(a, b);

            var result = new int[a.RowsCount, a.ColumnsCount];
            for (int i = 0; i < a.RowsCount; i++)
            {
                for (int j = 0; j < a.ColumnsCount; j++)
                {
                    result[i, j] = a[i, j] + b[i, j];
                }
            }

            return new Matrix(result);
        }

        public static Matrix operator -(Matrix a, Matrix b)
        {
            ThrowIfNull(a, b, nameof(a), nameof(b));
            ThrowIfDifferentSizes(a, b);

            var result = new int[a.RowsCount, a.ColumnsCount];
            for (int i = 0; i < a.RowsCount; i++)
            {
                for (int j = 0; j < a.ColumnsCount; j++)
                {
                    result[i, j] = a[i, j] - b[i, j];
                }
            }

            return new Matrix(result);
        }

        public static Matrix operator *(Matrix a, Matrix b)
        {
            ThrowIfNull(a, b, nameof(a), nameof(b));
            ThrowIfNotMultipliable(a, b);

            var result = new int[a.RowsCount, b.ColumnsCount];
            for (int i = 0; i < a.RowsCount; i++)
            {
                for (int j = 0; j < b.ColumnsCount; j++)
                {
                    for (int k = 0; k < a.ColumnsCount; k++)
                    {
                        result[i, j] += a[i, k] * b[k, j];
                    }
                }
            }

            return new Matrix(result);
        }

        public static bool operator ==(Matrix a, Matrix b)
        {
            return Equals(a, b);
        }

        public static bool operator !=(Matrix a, Matrix b)
        {
            return !(a == b);
        }

        public override string ToString()
        {
            const int alignment = -7;

            var result = new StringBuilder();
            for (int i = 0; i < RowsCount; i++)
            {
                for (int j = 0; j < ColumnsCount; j++)
                {
                    result.Append($"{elements[i, j],alignment}");
                }

                result.Append(Environment.NewLine);
            }

            result.Remove(result.Length - 1, 1);
            return result.ToString();
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Matrix);
        }

        public bool Equals(Matrix matrix)
        {
            if (!(matrix is null) && GetHashCode() == matrix.GetHashCode())
            {
                return ReferenceEquals(this, matrix)
                    || elements.EqualTo(matrix.elements);
            }

            return false;
        }

        public override int GetHashCode()
        {
            int hash = 0;
            hash ^= (hash << 5) + (hash >> 2) + RowsCount.GetHashCode();
            hash ^= (hash << 5) + (hash >> 2) + ColumnsCount.GetHashCode();
            foreach (var element in elements)
            {
                hash ^= (hash << 5) + (hash >> 2) + element.GetHashCode();
            }

            return hash;
        }

        private static void ThrowIfNotPositive(int param, string paramName)
        {
            Validator.ThrowIfOutOfRange(param, 0, paramName);
        }

        private static void ThrowIfNull(Matrix paramA, Matrix paramB, string paramAName, string paramBName)
        {
            Validator.ThrowIfNull(paramA, paramAName);
            Validator.ThrowIfNull(paramB, paramBName);
        }

        private static void ThrowIfDifferentSizes(Matrix a, Matrix b)
        {
            if (a.ColumnsCount != b.ColumnsCount || a.RowsCount != b.RowsCount)
            {
                throw new InvalidMatrixOperationException(a.RowsCount, a.ColumnsCount, b.RowsCount, b.ColumnsCount);
            }
        }

        private static void ThrowIfNotMultipliable(Matrix a, Matrix b)
        {
            if (a.ColumnsCount != b.RowsCount)
            {
                throw new InvalidMatrixOperationException(a.RowsCount, a.ColumnsCount, b.RowsCount, b.ColumnsCount);
            }
        }
    }
}
