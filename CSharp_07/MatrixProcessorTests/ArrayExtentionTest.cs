﻿using NUnit.Framework;
using MatrixProcessor;

namespace MatrixProcessorTests
{
    public class ArrayExtentionTest
    {
        [TestCaseSource(typeof(ArrayExtentionTestSource), nameof(ArrayExtentionTestSource.GetRowsCountCases))]
        public static int TestGetRowsCount_SimpleArray_CorrectResult(int[,] a)
        {
            return a.GetRowsCount();
        }

        [TestCaseSource(typeof(ArrayExtentionTestSource), nameof(ArrayExtentionTestSource.GetColumnsCountCases))]
        public static int TestGetColumnsCount_SimpleArray_CorrectResult(int[,] a)
        {
            return a.GetColumnsCount();
        }

        [TestCaseSource(typeof(ArrayExtentionTestSource), nameof(ArrayExtentionTestSource.EqualsCases))]
        public static bool TestEquals_SimpleArrays_CorrectResult(int[,] a, int[,] b)
        {
            return a.EqualTo(b);
        }
    }
}
