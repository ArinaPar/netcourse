﻿using System.Collections;
using NUnit.Framework;

namespace MatrixProcessorTests
{
    internal class ArrayExtentionTestSource
    {
        internal static IEnumerable GetRowsCountCases
        {
            get
            {
                yield return new TestCaseData(new int[,] { { 1, 2, 3, 4 }, { 5, 6, 7, 8 } }).Returns(2);
                yield return new TestCaseData(new int[,] { { 1, 2 }, { 3, 4 }, { 5, 6 }, { 7, 8 } }).Returns(4);
                yield return new TestCaseData(new int[0, 0]).Returns(0);
                yield return new TestCaseData(new int[3, 5]).Returns(3);
            }
        }

        internal static IEnumerable GetColumnsCountCases
        {
            get
            {
                yield return new TestCaseData(new int[,] { { 1, 2, 3, 4 }, { 5, 6, 7, 8 } }).Returns(4);
                yield return new TestCaseData(new int[,] { { 1, 2 }, { 3, 4 }, { 5, 6 }, { 7, 8 } }).Returns(2);
                yield return new TestCaseData(new int[0, 0]).Returns(0);
                yield return new TestCaseData(new int[3, 5]).Returns(5);
            }
        }

        internal static IEnumerable EqualsCases
        {
            get
            {
                yield return new TestCaseData(new int[,] { { 1, 2, 3, 4 }, { 5, 6, 7, 8 } }, new int[,] { { 1, 2, 3, 4 }, { 5, 6, 7, 8 } }).Returns(true);
                yield return new TestCaseData(new int[,] { { 1, 2, 3, 4 }, { 5, 7, 7, 8 } }, new int[,] { { 1, 2, 3, 4 }, { 5, 6, 7, 8 } }).Returns(false);
                yield return new TestCaseData(new int[,] { { 1, 2 }, { 3, 4 }, { 5, 6 }, { 7, 8 } }, new int[,] { { 1, 2, 3, 4 }, { 5, 6, 7, 8 } }).Returns(false);
                yield return new TestCaseData(new int[,] { { 1, 2, 3, 4 }, { 5, 6, 7, 8 } }, new int[,] { { 1, 2 }, { 3, 4 }, { 5, 6 }, { 7, 8 } }).Returns(false);
                yield return new TestCaseData(new int[0, 0], new int[0, 0]).Returns(true);
                yield return new TestCaseData(new int[3, 5], new int[3, 5]).Returns(true);
                yield return new TestCaseData(new int[3, 5], null).Returns(false);
            }
        }
    }
}
