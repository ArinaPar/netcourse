﻿using System;
using NUnit.Framework;
using MatrixProcessor;

namespace MatrixProcessorTests
{
    public class MatrixTest
    {
        [Test]
        public static void TestConstructor_ValidRowsAndColumnsCount_ReturnsEmptyMatrix(
            [Values(1, 20, 2000, 100)] int rows,
            [Values(123, 20, 2, 43210)] int columns)
        {
            var expected = new int[rows, columns];

            Matrix result = Matrix.Create(rows, columns);

            Assert.IsTrue(result.RowsCount == expected.GetRowsCount() && result.ColumnsCount == expected.GetColumnsCount());
        }

        [Test]
        public static void TestConstructor_NotValidRowsCount_ThrowsException(
            [Values(0, -1, -10, -100)] int rows,
            [Values(1, 100)] int columns)
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => Matrix.Create(rows, columns));
        }

        [Test]
        public static void TestConstructor_NotValidColumnsCount_ThrowsException(
            [Values(0, -1, -10, -100)] int columns,
            [Values(1, 100)] int rows)
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => Matrix.Create(rows, columns));
        }

        [TestCaseSource(typeof(MatrixTestCaseSource), nameof(MatrixTestCaseSource.ElementsCases))]
        public static void TestConstructor_WithElements_CorrectValues(int[,] elements)
        {
            Matrix result = Matrix.Create(elements);

            Assert.IsTrue(ElementsEquals(result, elements));
        }

        [Test]
        public static void TestConstructor_NullValue_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => Matrix.Create(null));
        }

        [TestCaseSource(typeof(MatrixTestCaseSource), nameof(MatrixTestCaseSource.ElementsCases))]
        public static void TestIndexerGet_SimpleValues_CorrectValues(int[,] elements)
        {
            Matrix matrix = Matrix.Create(elements);

            Assert.IsTrue(ElementsEquals(matrix, elements));
        }

        [TestCaseSource(typeof(MatrixTestCaseSource), nameof(MatrixTestCaseSource.ElementsCases))]
        public static void TestIndexerSet_SimpleValues_CorrectValues(int[,] elements)
        {
            Matrix matrix = Matrix.Create(elements.GetRowsCount(), elements.GetColumnsCount());

            for (int i = 0; i < elements.GetRowsCount(); i++)
            {
                for (int j = 0; j < elements.GetColumnsCount(); j++)
                {
                    matrix[i, j] = elements[i, j];
                }
            }

            Assert.IsTrue(ElementsEquals(matrix, elements));
        }

        [TestCaseSource(typeof(MatrixTestCaseSource), nameof(MatrixTestCaseSource.SumCases))]
        public static void TestOperatorPlus_ValidOperands_CorrectValue(Matrix a, Matrix b, Matrix expected)
        {
            Matrix result = a + b;

            Assert.IsTrue(result.Equals(expected));
        }

        [TestCaseSource(typeof(MatrixTestCaseSource), nameof(MatrixTestCaseSource.InvalidOperandsCases))]
        public static void TestOperatorPlus_DifferentLengths_ThrowsException(Matrix a, Matrix b)
        {
            Assert.Throws<InvalidMatrixOperationException>(() => _ = a + b);
        }

        [TestCaseSource(typeof(MatrixTestCaseSource), nameof(MatrixTestCaseSource.NullCases))]
        public static void TestOperatorPlus_NullValues_ThrowsException(Matrix a, Matrix b)
        {
            Assert.Throws<ArgumentNullException>(() => _ = a + b);
        }

        [TestCaseSource(typeof(MatrixTestCaseSource), nameof(MatrixTestCaseSource.DiffCases))]
        public static void TestOperatorMinus_ValidOperands_CorrectValue(Matrix a, Matrix b, Matrix expected)
        {
            Matrix result = a - b;

            Assert.IsTrue(result.Equals(expected));
        }

        [TestCaseSource(typeof(MatrixTestCaseSource), nameof(MatrixTestCaseSource.InvalidOperandsCases))]
        public static void TestOperatorMinus_DifferentLengths_ThrowsException(Matrix a, Matrix b)
        {
            Assert.Throws<InvalidMatrixOperationException>(() => _ = a - b);
        }

        [TestCaseSource(typeof(MatrixTestCaseSource), nameof(MatrixTestCaseSource.NullCases))]
        public static void TestOperatorMinus_NullValues_ThrowsException(Matrix a, Matrix b)
        {
            Assert.Throws<ArgumentNullException>(() => _ = a - b);
        }

        [TestCaseSource(typeof(MatrixTestCaseSource), nameof(MatrixTestCaseSource.MultiplyCases))]
        public static void TestOperatorMultiple_ValidOperands_CorrectValue(Matrix a, Matrix b, Matrix expected)
        {
            Matrix result = a * b;

            Assert.IsTrue(result.Equals(expected));
        }

        [TestCaseSource(typeof(MatrixTestCaseSource), nameof(MatrixTestCaseSource.InvalidOperandsCases))]
        public static void TestOperatorMultiple_InvalidOperandLengths_ThrowsException(Matrix a, Matrix b)
        {
            Assert.Throws<InvalidMatrixOperationException>(() => _ = a * b);
        }

        [TestCaseSource(typeof(MatrixTestCaseSource), nameof(MatrixTestCaseSource.NullCases))]
        public static void TestOperatorMultiple_NullValues_ThrowsException(Matrix a, Matrix b)
        {
            Assert.Throws<ArgumentNullException>(() => _ = a * b);
        }

        [TestCaseSource(typeof(MatrixTestCaseSource), nameof(MatrixTestCaseSource.EqualenceCases))]
        public static void TestOperatorEquals_ValidOperands_CorrectValue(Matrix a, Matrix b, bool expected)
        {
            Assert.AreEqual(a == b, expected);
        }

        [TestCaseSource(typeof(MatrixTestCaseSource), nameof(MatrixTestCaseSource.ElementsCases))]
        public static void TestOperatorEquals_SameReference_CorrectValue(int[,] elements)
        {
            Matrix a = Matrix.Create(elements);
            Matrix b = a;

            Assert.IsTrue(a == b);
        }

        [TestCaseSource(typeof(MatrixTestCaseSource), nameof(MatrixTestCaseSource.NullCases))]
        public static void TestOperatorEquals_NullValues_CorrectValue(Matrix a, Matrix b)
        {
            bool expected = a is null && b is null;

            Assert.AreEqual(a == b, expected);
        }

        [TestCaseSource(typeof(MatrixTestCaseSource), nameof(MatrixTestCaseSource.EqualenceCases))]
        public static void TestOperatorNotEquals_ValidOperands_CorrectValue(Matrix a, Matrix b, bool notExpected)
        {
            Assert.AreNotEqual(a != b, notExpected);
        }

        [TestCaseSource(typeof(MatrixTestCaseSource), nameof(MatrixTestCaseSource.NullCases))]
        public static void TestOperatorNotEquals_NullValues_CorrectValue(Matrix a, Matrix b)
        {
            bool expected = !(a is null && b is null);

            Assert.AreEqual(a != b, expected);
        }

        [TestCaseSource(typeof(MatrixTestCaseSource), nameof(MatrixTestCaseSource.EqualenceCases))]
        public static void TestEquals_TwoMatrixes_CorrectValue(Matrix a, Matrix b, bool expected)
        {
            Assert.AreEqual(a.Equals(b), expected);
        }

        [TestCaseSource(typeof(MatrixTestCaseSource), nameof(MatrixTestCaseSource.EqualenceCases))]
        public static void TestEquals_TypeObject_CorrectValue(Matrix a, Matrix b, bool expected)
        {
            Assert.AreEqual(a.Equals((object)b), expected);
        }

        [Test]
        public static void TestEquals_NotMatrix_ReturnsFalse()
        {
            int number = 0;
            Matrix matrix = Matrix.Create(1, 1);

            Assert.IsFalse(matrix.Equals(number));
        }

        [TestCaseSource(typeof(MatrixTestCaseSource), nameof(MatrixTestCaseSource.ElementsCases))]
        public static void TestEquals_SameReference_CorrectValue(int[,] elements)
        {
            Matrix a = Matrix.Create(elements);
            Matrix b = a;

            Assert.IsTrue(a.Equals(b));
        }

        [TestCaseSource(typeof(MatrixTestCaseSource), nameof(MatrixTestCaseSource.ElementsCases))]
        public static void TestEquals_NullValue_ReturnsFalse(int[,] elements)
        {
            Matrix a = Matrix.Create(elements);
            Matrix b = null;

            Assert.IsFalse(a.Equals(b));
        }

        [TestCaseSource(typeof(MatrixTestCaseSource), nameof(MatrixTestCaseSource.EqualenceCases))]
        public static void TestGetHashCode_TwoMatrixes_CorrectValue(Matrix a, Matrix b, bool expected)
        {
            Assert.AreEqual(a.GetHashCode() == b.GetHashCode(), expected);
        }

        [TestCaseSource(typeof(MatrixTestCaseSource), nameof(MatrixTestCaseSource.ToStringCases))]
        public static string TestToString_SimpleValues_CorrectResult(Matrix matrix)
        {
            return matrix.ToString();
        }

        private static bool ElementsEquals(Matrix matrix, int[,] elements)
        {
            if (matrix.RowsCount != elements.GetRowsCount() || matrix.ColumnsCount != elements.GetColumnsCount())
            {
                return false;
            }

            for (int i = 0; i < elements.GetRowsCount(); i++)
            {
                for (int j = 0; j < elements.GetColumnsCount(); j++)
                {
                    if (matrix[i, j] != elements[i, j])
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}
