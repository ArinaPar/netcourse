﻿using System.Collections;
using NUnit.Framework;
using MatrixProcessor;

namespace MatrixProcessorTests
{
    internal class MatrixTestCaseSource
    {
        internal static readonly object[] ElementsCases =
        {
             new int[,] { {6, 9, -3, 80, 21}, { -10000, 4354, 34, 4, 0}, { 0, 0, 0, 0, 0}, { 12, 324, 345, 89, 67} },
             new int[,] { {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10} },
             new int[,] { { 12345, 23456, 34567 } },
             new int[,] { {-12345, -23456, -34567, -45678} }
        };

        internal static readonly object[] SumCases =
        {
            new object[]
            {
                Matrix.Create(new int[,] { {1, 2, 3, 4, 5}, {1, 2, 3, 4, 5} }),
                Matrix.Create(new int[,] { {2, 4, 6, 8, 10}, {2, 4, 6, 8, 10} }),
                Matrix.Create(new int[,] { { 3, 6, 9, 12, 15 }, { 3, 6, 9, 12, 15 } })
            },
            new object[]
            {
                Matrix.Create(new int[,] { {2, 4, 6, 8, 10}, {2, 4, 6, 8, 10} }),
                Matrix.Create(new int[2,5]),
                Matrix.Create(new int[,] { {2, 4, 6, 8, 10}, {2, 4, 6, 8, 10} })
            },
            new object[]
            {
                Matrix.Create(new int[2,5]),
                Matrix.Create(new int[,] { {2, 4, 6, 8, 10}, {2, 4, 6, 8, 10} }),
                Matrix.Create(new int[,] { { 2, 4, 6, 8, 10 }, { 2, 4, 6, 8, 10 } })
            },
            new object[]
            {
                Matrix.Create(new int[100,2]),
                Matrix.Create(new int[100,2]),
                Matrix.Create(new int[100,2])
            },
            new object[]
            {
                Matrix.Create(new int[,] { {1, 2, 3, 4, 5}, {-1, -2, -3, -4, -5} }),
                Matrix.Create(new int[,] { {-2, -4, -6, -8, -10}, {2, 4, 6, 8, 10} }),
                Matrix.Create(new int[,] { { -1, -2, -3, -4, -5 }, { 1, 2, 3, 4, 5 } })
            },
            new object[]
            {
                Matrix.Create(new int[,] { {100000, 200000, 300000, 4, 5}, {1, 2, 3, 400000, 500000} }),
                Matrix.Create(new int[,] { {2, 4, 6, 8, 10}, {2, 4, 6, 8, 10} }),
                Matrix.Create(new int[,] { { 100002, 200004, 300006, 12, 15 }, { 3, 6, 9, 400008, 500010 } })
            },
            new object[]
            {
                Matrix.Create(new int[,] { {2, 4, 6, 8, 10}, {2, 4, 6, 8, 10} }),
                Matrix.Create(new int[,] { {100000, 200000, 300000, 4, 5}, {1, 2, 3, 400000, 500000} }),
                Matrix.Create(new int[,] { { 100002, 200004, 300006, 12, 15 }, { 3, 6, 9, 400008, 500010 } })
            }
        };

        internal static readonly object[] DiffCases =
        {
            new object[]
            {
                Matrix.Create(new int[,] { {1, 2, 3, 4, 5}, {1, 2, 3, 4, 5} }),
                Matrix.Create(new int[,] { {2, 4, 6, 8, 10}, {2, 4, 6, 8, 10} }),
                Matrix.Create(new int[,] { { -1, -2, -3, -4, -5 }, { -1, -2, -3, -4, -5 } })
            },
            new object[]
            {
                Matrix.Create(new int[,] { {2, 4, 6, 8, 10}, {2, 4, 6, 8, 10} }),
                Matrix.Create(new int[2,5]),
                Matrix.Create(new int[,] { { 2, 4, 6, 8, 10 }, { 2, 4, 6, 8, 10 } })
            },
            new object[]
            {
                Matrix.Create(new int[2,5]),
                Matrix.Create(new int[,] { {2, 4, 6, 8, 10}, {2, 4, 6, 8, 10} }),
                Matrix.Create(new int[,] { { -2, -4, -6, -8, -10 }, { -2, -4, -6, -8, -10 } })
            },
            new object[]
            {
                Matrix.Create(new int[100,2]),
                Matrix.Create(new int[100,2]),
                Matrix.Create(new int[100, 2])
            },
            new object[]
            {
                Matrix.Create(new int[,] { {1, 2, 3, 4, 5}, {-1, -2, -3, -4, -5} }),
                Matrix.Create(new int[,] { {-2, -4, -6, -8, -10}, {2, 4, 6, 8, 10} }),
                Matrix.Create(new int[,] { { 3, 6, 9, 12, 15 }, { -3, -6, -9, -12, -15 } })
            },
            new object[]
            {
                Matrix.Create(new int[,] { {100000, 200000, 300000, 4, 5}, {1, 2, 3, 400000, 500000} }),
                Matrix.Create(new int[,] { {2, 4, 6, 8, 10}, {2, 4, 6, 8, 10} }),
                Matrix.Create(new int[,] { { 99998, 199996, 299994, -4, -5 }, { -1, -2, -3, 399992, 499990 } })
            },
            new object[]
            {
                Matrix.Create(new int[,] { {2, 4, 6, 8, 10}, {2, 4, 6, 8, 10} }),
                Matrix.Create(new int[,] { {100000, 200000, 300000, 4, 5}, {1, 2, 3, 400000, 500000} }),
                Matrix.Create(new int[,] { { -99998, -199996, -299994, 4, 5 }, { 1, 2, 3, -399992, -499990 } })
            }
        };

        internal static readonly object[] MultiplyCases =
        {
            new object[]
            {
                Matrix.Create(new int[,] { {1, 2, 3, 4, 5}, {1, 2, 3, 4, 5} }),
                Matrix.Create(new int[,] { { 2, 4 }, { 6, 8 }, { 10, 2 }, { 4, 6 }, { 8, 10 } }),
                Matrix.Create(new int[,] { { 100, 100 }, { 100, 100 } })
            },
            new object[]
            {
                Matrix.Create(new int[3,2]),
                Matrix.Create(new int[,] { {2, 4, 6}, {2, 4, 6} }),
                Matrix.Create(new int[3, 3])
            },
             new object[]
            {
                Matrix.Create(new int[,] { {2, 4, 6}, {2, 4, 6} }),
                Matrix.Create(new int[3,2]),
                Matrix.Create(new int[2, 2])
            },
            new object[]
            {
                Matrix.Create(new int[100,2]),
                Matrix.Create(new int[2, 100]),
                Matrix.Create(new int[100, 100])
            },
            new object[]
            {
                Matrix.Create(new int[,] { { 1, 2 }, { 3, 4 }, { 5, -1 }, { -2, -3 }, {-4, -5} }),
                Matrix.Create(new int[,] { {-2, -4, -6, -8, -10}, {2, 4, 6, 8, 10} }),
                Matrix.Create(new int[,] { { 2, 4, 6, 8, 10 }, { 2, 4, 6, 8, 10 }, { -12, -24, -36, -48, -60 }, { -2, -4, -6, -8, -10 }, { -2, -4, -6, -8, -10 } })
            },
            new object[]
            {
                Matrix.Create(new int[,] { {100000, 200000, 300000, 4, 5}, {1, 2, 3, 400000, 500000} }),
                Matrix.Create(new int[,] { { 2, 4 }, { 6, 8 }, { 10, 2 }, { 4, 6 }, {8, 10} }),
                Matrix.Create(new int[,] { { 4400056, 2600074 }, { 5600044, 7400026 } })
            },
            new object[]
            {
                Matrix.Create(new int[,] { {2, 4, 6, 8, 10}, {2, 4, 6, 8, 10} }),
                Matrix.Create(new int[,] { { 100000, 200000 }, { 300000, 4 }, { 5, 1 }, { 2, 3 }, {400000, 500000} }),
                Matrix.Create(new int[,] { { 5400046, 5400046 }, { 5400046, 5400046 } })
            }
        };

        internal static readonly object[] InvalidOperandsCases =
        {
            new object[]
            {
                Matrix.Create(new int[,] { {2, 4, 6, 8, 10}, {2, 4, 6, 8, 10} }),
                Matrix.Create(new int[1,5])
            },
            new object[]
            {
                Matrix.Create(new int[1,5]),
                Matrix.Create(new int[,] { {2, 4, 6, 8, 10}, {2, 4, 6, 8, 10} })
            },
            new object[]
            {
                Matrix.Create(new int[100,1]),
                Matrix.Create(new int[100,2])
            }
        };

        internal static readonly object[] NullCases =
        {
            new object[] { Matrix.Create(new int[,] { {2, 4, 6, 8, 10}, {2, 4, 6, 8, 10} }), null },
            new object[] { null, Matrix.Create(new int[,] { {2, 4, 6, 8, 10}, {2, 4, 6, 8, 10} }) },
            new object[] { null, null }
        };

        internal static readonly object[] EqualenceCases =
        {
            new object[]
            {
                Matrix.Create(new int[,] { {1, 2, 3, 4, 5}, {1, 2, 3, 4, 5} }),
                Matrix.Create(new int[,] { {1, 2, 3, 4, 5}, {1, 2, 3, 4, 5} }),
                true
            },
            new object[]
            {
                Matrix.Create(new int[,] { {1, 2, 3, 4, 5}, {1, 2, 3, 4, 5} }),
                Matrix.Create(new int[,] { {1, 2, 3, 4, 5}, {1, 2, 3, 4, 6} }),
                false
            },
            new object[]
            {
                Matrix.Create(new int[3,2]),
                Matrix.Create(new int[3,2]),
                true
            },
             new object[]
            {
                Matrix.Create(new int[,] { {2, 4, 6}, {2, 4, 6} }),
                Matrix.Create(new int[2,3]),
                false
            },
            new object[]
            {
                Matrix.Create(new int[100,2]),
                Matrix.Create(new int[2, 100]),
                false
            }
        };

        internal static IEnumerable ToStringCases
        {
            get
            {
                yield return new TestCaseData(Matrix.Create(new int[,] { { 2, 4, 6 }, { 2, 4, 6 } })).Returns("2      4      6      \r\n2      4      6      \r");
                yield return new TestCaseData(Matrix.Create(new int[,] { { 1, 22345 }, { 144323, 2 } })).Returns("1      22345  \r\n144323 2      \r");
                yield return new TestCaseData(Matrix.Create(new int[1, 5])).Returns("0      0      0      0      0      \r");
                yield return new TestCaseData(Matrix.Create(new int[,] { { 2, 4 }, { 6, 8 }, { 10, 2 }, { 4, 6 }, { 8, 10 } })).Returns("2      4      \r\n6      8      \r\n10     2      \r\n4      6      \r\n8      10     \r");
            }
        }
    }
}
