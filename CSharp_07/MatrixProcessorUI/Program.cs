﻿using System;
using MatrixProcessor;

namespace MatrixProcessorUI
{
    public class Program
    {
        public static void Main()
        {
            Matrix a = ReadMatrixFromConsole(nameof(a));
            Matrix b = ReadMatrixFromConsole(nameof(b));

            HandleLinearOperations(a, b);
            HandleProduct(a, b);
        }

        private static Matrix ReadMatrixFromConsole(string name)
        {
            WriteMessageOnConsole($"MATRIX {name.ToUpperInvariant()}\n");
            int rows = ReadCountFromConsole("rows");
            int columns = ReadCountFromConsole("columns");
            int[,] elements = ReadArrayFromConsole(rows, columns);

            return Matrix.Create(elements);
        }

        private static void HandleLinearOperations(Matrix a, Matrix b)
        {
            try
            {
                WriteMatrixOnConsole(a + b);
                WriteMatrixOnConsole(a - b);
            }
            catch (InvalidMatrixOperationException ex)
            {
                WriteMessageOnConsole(ex.Message + Environment.NewLine);
            }
        }

        private static void HandleProduct(Matrix a, Matrix b)
        {
            try
            {
                WriteMatrixOnConsole(a * b);
            }
            catch (InvalidMatrixOperationException ex)
            {
               WriteMessageOnConsole(ex.Message + Environment.NewLine);
            }
        }

        private static void WriteMessageOnConsole(string message)
        {
            Console.Write(message);
        }

        private static int ReadCountFromConsole(string countable)
        {
            WriteMessageOnConsole($"Enter the count of {countable}: ");
            return ReadIntFromConsole(0);
        }

        private static int ReadIntFromConsole(int? rangeBound = null)
        {
            bool isCorrect = false;
            int result = 0;
            while (!isCorrect)
            {
                if (int.TryParse(Console.ReadLine(), out result) 
                    && (!(rangeBound.HasValue) || result > rangeBound))
                {
                    isCorrect = true;
                }
                else
                {
                    Console.WriteLine(rangeBound.HasValue
                        ? $"Invalid input: it's not an integer bigger than {rangeBound}."
                        : "Invalid input: it's not an integer.");
                }
            }

            return result;
        }

        private static int[,] ReadArrayFromConsole(int rows, int columns)
        {
            int[,] result = new int[rows, columns];
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    WriteMessageOnConsole($"[{i + 1}, {j + 1}]: ");
                    result[i, j] = ReadIntFromConsole();
                }
            }

            return result;
        }

        private static void WriteMatrixOnConsole(Matrix matrix)
        {
            Console.WriteLine($"\n{matrix}\n");
        }

    }
}
