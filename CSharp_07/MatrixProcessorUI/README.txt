The application makes operations with two user-defined matrixes.
It writes sum, difference and product of matrixes on console.
First it tries to calculate their sum and difference. If sizes of matrixes are different, it's impossible to perform theese operations.
Then it tries to calculate their product. If columns count of first matrix is not equal to rows coumt of second matrix, it's impossible to perform this operation.
If the operation performance is impossible, the application writes an appropriate message.