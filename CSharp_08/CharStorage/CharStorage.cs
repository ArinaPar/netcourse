﻿using System;
using System.IO;
using System.Text;
using Helpers;

namespace FileCharStorage
{
    public class CharStorage : IDisposable
    {
        private readonly string filePath;

        private FileStream fileStream;
        private StreamReader streamReader;
        private StreamWriter streamWriter;

        public Encoding Encoding { get; private set; }

        public long Length => fileStream.Length;

        public char this[int index]
        {
            get
            {
                fileStream.Position = index;
                var buffer = new char[1];
                if (streamReader.Read(buffer, 0, 1) == 0)
                    throw new EndOfStreamException(nameof(index));

                return buffer[0];
            }

            set
            {
                fileStream.Position = index;
                streamWriter.Write(value);
                streamWriter.Flush();
            }
        }

        private CharStorage(string filePath, FileMode fileMode, int? length = null)
        {
            this.filePath = filePath;
            OpenStream(fileMode);
            if (length.HasValue)
                WriteGapsOnFile(length.Value);
        }

        public static CharStorage Read(string filePath)
        {
            Validator.ThrowIfNull(filePath, nameof(filePath));
            if (!File.Exists(filePath))
                throw new FileNotFoundException(filePath);

            return new CharStorage(filePath, FileMode.Open);
        }

        public static CharStorage Create(string filePath, int length)
        {
            Validator.ThrowIfNull(filePath, nameof(filePath));
            Validator.ThrowIfOutOfRange(length, -1, nameof(length));

            return new CharStorage(filePath, FileMode.Create, length);
        }

        public void Dispose()
        {
            streamWriter?.Close();
            streamReader?.Close();
            fileStream?.Close();
        }

        private void OpenStream(FileMode fileMode)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            Encoding = Encoding.GetEncoding(1252);

            fileStream = new FileStream(filePath, fileMode, FileAccess.ReadWrite);
            streamReader = new StreamReader(fileStream, Encoding);
            streamWriter = new StreamWriter(fileStream, Encoding);
        }

        private void WriteGapsOnFile(int count)
        {
            fileStream.Position = 0;
            var gaps = new string(' ', count);
            streamWriter.Write(gaps);
            streamWriter.Flush();
        }
    }
}