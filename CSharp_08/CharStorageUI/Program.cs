﻿using System;
using System.IO;
using FileCharStorage;

namespace CharStorageUI
{
    public class Program
    {
        private static readonly string baseFolder = AppDomain.CurrentDomain.BaseDirectory;
        private static readonly string filePath = Path.Combine(baseFolder, "storage");

        public static void Main()
        {
            const string testString = "[01] Hello World!";
            WriteLineToFile(testString);
            ShowFileContent();

            WriteSeparator();

            const int index = 2;
            const char newCharacter = '2';
            ReplaceSymbol(index, newCharacter);
            ShowFileContent();
        }

        private static void WriteLineToFile(string line)
        {
            CharStorage cs = null;
            try
            {
                cs = CharStorage.Create(filePath, line.Length);
                for (int i = 0; i < cs.Length; i++)
                {
                    cs[i] = line[i];
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                cs?.Dispose();
            }
        }

        private static void ShowFileContent()
        {
            try
            {
                using CharStorage cs = CharStorage.Read(filePath);
                for (int i = 0; i < cs.Length; i++)
                {
                    Console.Write(cs[i]);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static void WriteSeparator()
        {
            Console.WriteLine();
        }

        private static void ReplaceSymbol(int index, char newSymbol)
        {
            try
            {
                using CharStorage cs = CharStorage.Read(filePath);
                cs[index] = newSymbol;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
