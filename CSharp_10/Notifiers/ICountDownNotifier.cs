﻿namespace Notifiers
{
    public delegate void Handle(string s, int i);

    public interface ICountDownNotifier
    {
        public void Init();

        public void Run();

        public void Unsubscribe();
    }
}
