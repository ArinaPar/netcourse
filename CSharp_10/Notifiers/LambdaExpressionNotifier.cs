﻿using System;
using TimerProcessor;

namespace Notifiers
{
    public class LambdaExpressionNotifier : ICountDownNotifier
    {
        private readonly Timer timer;

        private readonly Logger logger;

        private readonly Handle handleStart;

        private readonly Action<string, int> handleStop;

        private StartHandler onTimerStart;

        private EventHandler<TimerEventArgs> onTimerTick;

        private EventHandler<TimerEventArgs> onTimerStop;

        public LambdaExpressionNotifier(Timer timer, Logger logger, Handle handleStart, Action<string, int> handleStop)
        {
            this.timer = timer;
            this.logger = logger;
            this.handleStart = handleStart;
            this.handleStop = handleStop;
        }

        public void Init()
        {
            CreateHandlers();
            SubscribeHandlers();
        }

        public void Run()
        {
            timer.Run();
        }

        public void Unsubscribe()
        {
            timer.Start -= onTimerStart;
            timer.Tick -= onTimerTick;
            timer.Stop -= onTimerStop;
        }

        private void CreateHandlers()
        {
            onTimerStart = (sender, e)
                => handleStart(e.Name, e.Ticks);
            onTimerTick = (sender, e)
                => logger.Log($"Timer: Task {e.Name} {e.Ticks} seconds remaining.");
            onTimerStop = (sender, e)
                => handleStop(e.Name, e.Ticks);
        }

        private void SubscribeHandlers()
        {
            timer.Start += onTimerStart;
            timer.Tick += onTimerTick;
            timer.Stop += onTimerStop;
        }
    }
}
