﻿namespace Notifiers
{
    public abstract class Logger
    {
        public abstract void Log(string message);
    }
}
