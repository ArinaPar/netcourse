﻿using System;
using TimerProcessor;

namespace Notifiers
{
    public class MethodNotifier : ICountDownNotifier
    {
        private readonly Timer timer;

        private readonly Logger logger;

        private readonly Handle handleStart;

        private readonly Action<string, int> handleStop;

        public MethodNotifier(Timer timer, Logger logger, Handle handleStart, Action<string, int> handleStop)
        {
            this.timer = timer;
            this.logger = logger;
            this.handleStart = handleStart;
            this.handleStop = handleStop;
        }

        public void Init()
        {
            timer.Start += OnTimerStart;
            timer.Tick += OnTimerTick;
            timer.Stop += OnTimerStop;
        }

        public void Run()
        {
            timer.Run();
        }

        public void Unsubscribe()
        {
            timer.Start -= OnTimerStart;
            timer.Tick -= OnTimerTick;
            timer.Stop -= OnTimerStop;
        }

        private void OnTimerStart(object sender, TimerEventArgs e)
        {
            handleStart(e.Name, e.Ticks);
        }

        private void OnTimerTick(object sender, TimerEventArgs e)
        {
            logger.Log($"Timer: Task {e.Name} {e.Ticks} seconds remaining.");
        }

        private void OnTimerStop(object sender, TimerEventArgs e)
        {
            handleStop(e.Name, e.Ticks);
        }
    }
}
