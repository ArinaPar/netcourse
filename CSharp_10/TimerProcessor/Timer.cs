﻿using System;
using System.Threading;

namespace TimerProcessor
{
    public delegate void StartHandler(object? sender, TimerEventArgs e);

    public class Timer
    {
        public string Name { get; set; }

        public int Seconds { get; private set; }

        public Timer(string name, int seconds)
        {
            Name = name;
            Seconds = seconds;
        }

        public event StartHandler Start;

        public event EventHandler<TimerEventArgs> Tick;

        public event EventHandler<TimerEventArgs> Stop;

        public void Run()
        {
            OnStart();
            DoTicks();
            OnStop();
        }

        private void OnStart()
        {
            Start?.Invoke(this, new TimerEventArgs(Name, Seconds));
        }

        private void DoTicks()
        {
            OnTick();
            int ticks = Seconds;
            for (int i = 0; i < ticks; i++)
            {
                Thread.Sleep(1000);
                Seconds--;
                OnTick();
            }
        }

        private void OnTick()
        {
            Tick?.Invoke(this, new TimerEventArgs(Name, Seconds));
        }

        private void OnStop()
        {
            Stop?.Invoke(this, new TimerEventArgs(Name, Seconds));
        }
    }
}
