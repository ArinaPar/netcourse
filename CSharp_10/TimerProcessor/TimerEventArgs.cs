﻿using System;

namespace TimerProcessor
{
    public class TimerEventArgs : EventArgs
    {
        public string Name { get; }

        public int Ticks { get; }

        public TimerEventArgs(string name, int ticks)
        {
            Name = name;
            Ticks = ticks;
        }
    }
}
