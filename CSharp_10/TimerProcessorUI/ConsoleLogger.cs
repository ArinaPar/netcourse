﻿using System;
using Notifiers;

namespace TimerProcessorUI
{
    public class ConsoleLogger : Logger
    {
        public override void Log(string message)
        {
            Console.WriteLine($"[{DateTime.Now.Second}]: {message}");
        }
    }
}
