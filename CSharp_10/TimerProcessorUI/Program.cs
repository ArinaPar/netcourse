﻿using Notifiers;
using TimerProcessor;

namespace TimerProcessorUI
{
    public class Program
    {
        private static ConsoleLogger consoleLogger = new ConsoleLogger();

        public static void Main()
        {
            ICountDownNotifier[] countDownNotifiers = InitCountDownNotifiers();
            foreach (var notifier in countDownNotifiers)
            {
                HandleNotifier(notifier);
            }
        }

        private static ICountDownNotifier[] InitCountDownNotifiers()
        {
            var timerForReading = new Timer("Reading", 1);
            var methodNotifier = new MethodNotifier(timerForReading, consoleLogger, LogStart, LogStop);

            var timerForExecution = new Timer("Execution", 2);
            var anonymousMethodNotifier =
                new AnonymousMethodNotifier(timerForExecution, consoleLogger, LogStart, LogStop);

            var timerForChecking = new Timer("Checking before send", 5);
            var lambdaExpressionNotifier =
                new LambdaExpressionNotifier(timerForChecking, consoleLogger, LogStart, LogStop);

            return new ICountDownNotifier[]
            {
                methodNotifier,
                anonymousMethodNotifier,
                lambdaExpressionNotifier
            };
        }

        private static void HandleNotifier(ICountDownNotifier notifier)
        {
            notifier.Init();
            notifier.Run();
            notifier.Unsubscribe();
        }

        private static void LogStart(string taskName, int seconds)
        {
            consoleLogger.Log($"Task {taskName} has started for {seconds} seconds.");
        }

        private static void LogStop(string taskName, int seconds)
        {
            consoleLogger.Log($"Task {taskName} has finished.");
        }
    }
}
