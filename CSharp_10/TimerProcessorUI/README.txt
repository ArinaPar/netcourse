The application demonstraits the result of timer working.
It is down counting and it allows to get time to the different actions.
For example, there are 3 steps of task process: 
- reading task (1 sec);
- execution task (2 sec);
- checking task before send (5 sec).
Handlers white information about every step, namely:
- step was started;
- the count of remaining second (every second);
- step was stopped.
It can emulate a real process.