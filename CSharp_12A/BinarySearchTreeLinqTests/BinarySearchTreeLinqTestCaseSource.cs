﻿using System;
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using Entities;

namespace BinarySearchTreeLinqTests
{
    internal static class BinarySearchTreeLinqTestCaseSource
    {
        internal static IEnumerable<IEnumerable<TestResult<int>>> NodeValuesCases
        {
            get
            {
                yield return NodeValuesSource;
            }
        }
        
        internal static IEnumerable GetTestResultsInMonthSource
        {
            get
            {
                yield return new TestCaseData(NodeValuesSource, 2, 2022).Returns(new[] { 4, 4, 5 });
                yield return new TestCaseData(NodeValuesSource, 12, 2021).Returns(new[] { 4, 4, 4 });
                yield return new TestCaseData(NodeValuesSource, 3, 2020).Returns(new[] { 2, 5, 5 });
                yield return new TestCaseData(NodeValuesSource, 3, 2021).Returns(new[] { 4 });
                yield return new TestCaseData(NodeValuesSource, 4, 2022).Returns(new int[] {});
            }
        }

        internal static IEnumerable GetThreeMaxMarksSource
        {
            get
            {
                yield return new TestCaseData(NodeValuesSource, "HTML-300").Returns(new[] { 5, 5, 4 });
                yield return new TestCaseData(NodeValuesSource, "CSS-100").Returns(new[] { 5, 4, 4 });
                yield return new TestCaseData(NodeValuesSource, "SQL-300+").Returns(new[] { 4 });
                yield return new TestCaseData(NodeValuesSource, "SQL-200").Returns(new[] { 5, 5, 3 });
                yield return new TestCaseData(NodeValuesSource, "HTML-100").Returns(new[] { 4 });
                yield return new TestCaseData(NodeValuesSource, "CSS-300").Returns(new[] { 3, 3 });
                yield return new TestCaseData(NodeValuesSource, "SQL-100").Returns(new[] { 4, 4, 3 });
            }
        }

        private static readonly IEnumerable<TestResult<int>> NodeValuesSource =
            new List<TestResult<int>>
            {
                new(new Student("Michael", "Bold"), new Test("HTML-300", new DateTime(2022, 2, 2)), 4),
                new(new Student("Peter", "Bold"), new Test("HTML-300", new DateTime(2022, 2, 2)), 4),
                new(new Student("Olya", "Lukina"), new Test("HTML-100", new DateTime(2021, 12, 21)), 4),
                new(new Student("Alabay", "Abalaev"), new Test("SQL-200", new DateTime(2020, 3, 1)), 2),
                new(new Student("Bob", "Morley"), new Test("CSS-100", new DateTime(2021, 12, 2)), 4),
                new(new Student("Michael", "Bold"), new Test("CSS-100", new DateTime(2021, 12, 2)), 4),
                new(new Student("Alabay", "Abalaev"), new Test("SQL-200", new DateTime(2021, 4, 4)), 3),
                new(new Student("Olya", "Lukina"), new Test("CSS-200", new DateTime(2021, 5, 21)), 4),
                new(new Student("Alabay", "Abalaev"), new Test("HTML-200", new DateTime(2022, 1, 21)), 4),
                new(new Student("Bob", "Morley"), new Test("SQL-200", new DateTime(2020, 3, 1)), 5),
                new(new Student("Alabay", "Abalaev"), new Test("HTML-300", new DateTime(2021, 6, 21)), 2),
                new(new Student("Alabay", "Abalaev"), new Test("HTML-300", new DateTime(2022, 1, 21)), 4),
                new(new Student("Bob", "Morley"), new Test("SQL-400", new DateTime(2021, 8, 5)), 5),
                new(new Student("Bob", "Morley"), new Test("SQL-100", new DateTime(2020, 1, 1)), 4),
                new(new Student("Olya", "Lukina"), new Test("CSS-300", new DateTime(2022, 1, 21)), 3),
                new(new Student("Bob", "Morley"), new Test("HTML-300", new DateTime(2022, 2, 2)), 5),
                new(new Student("Bob", "Morley"), new Test("SQL-300+", new DateTime(2021, 10, 25)), 4),
                new(new Student("Peter", "Bold"), new Test("HTML-200", new DateTime(2021, 2, 2)), 3),
                new(new Student("Olya", "Lukina"), new Test("CSS-100", new DateTime(2021, 1, 21)), 5),
                new(new Student("Alabay", "Abalaev"), new Test("CSS-300", new DateTime(2022, 1, 10)), 3),
                new(new Student("Bob", "Morley"), new Test("SQL-300", new DateTime(2021, 2, 21)), 4),
                new(new Student("Michael", "Bold"), new Test("SQL-100", new DateTime(2020, 1, 1)), 4),
                new(new Student("Alabay", "Abalaev"), new Test("SQL-100", new DateTime(2020, 1, 1)), 3),
                new(new Student("Michael", "Bold"), new Test("SQL-300", new DateTime(2021, 2, 21)), 4),
                new(new Student("Olya", "Lukina"), new Test("HTML-300", new DateTime(2020, 1, 21)), 3),
                new(new Student("Michael", "Bold"), new Test("SQL-200", new DateTime(2020, 3, 1)), 5),
                new(new Student("Olya", "Lukina"), new Test("HTML-300", new DateTime(2022, 1, 21)), 5),
                new(new Student("Alabay", "Abalaev"), new Test("CSS-200", new DateTime(2021, 3, 14)), 4)
            };
    }
}
