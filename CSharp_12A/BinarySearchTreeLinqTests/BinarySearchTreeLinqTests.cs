﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using NUnit.Framework;
using Entities;

namespace BinarySearchTreeLinqTests
{
    [TestFixture]
    public class BinarySearchTreeLinqTests
    {
        [TestCaseSource(typeof(BinarySearchTreeLinqTestCaseSource), nameof(BinarySearchTreeLinqTestCaseSource.NodeValuesCases))]
        public void TestGetFirstTestDate_TestResultCollection_CorrectResult(IEnumerable<TestResult<int>> collection)
        {
            var expected = new DateTime(2020, 1, 1);

            var actual = collection.Select(n => n.Test.Date).Min();

            Assert.AreEqual(expected, actual);
        }

        [TestCaseSource(typeof(BinarySearchTreeLinqTestCaseSource), nameof(BinarySearchTreeLinqTestCaseSource.NodeValuesCases))]
        public void TestGetCurrentYearTestCount_TestResultCollection_CorrectResult(IEnumerable<TestResult<int>> collection)
        {
            var expected = 8;

            var actual = collection.Count(n => n.Test.Date.Year == DateTime.Now.Year);

            Assert.AreEqual(expected, actual);
        }

        [TestCaseSource(typeof(BinarySearchTreeLinqTestCaseSource), nameof(BinarySearchTreeLinqTestCaseSource.GetThreeMaxMarksSource))]
        public int[] TestGetThreeMaxMarks_TestResultCollection_CorrectResult(IEnumerable<TestResult<int>> collection, string testName)
        {
            return collection
                .Where(n => n.Test.Name == testName)
                .Select(n => n.Mark)
                .OrderByDescending(n => n)
                .Take(3)
                .ToArray();
        }

        [TestCaseSource(typeof(BinarySearchTreeLinqTestCaseSource), nameof(BinarySearchTreeLinqTestCaseSource.NodeValuesCases))]
        public void TestGetStudentNames_TestResultCollection_CorrectResult(IEnumerable<TestResult<int>> collection)
        {
            var expected = new[]
            {
                new {Name = "Alabay", Surname = "Abalaev" },
                new {Name = "Michael", Surname = "Bold"},
                new {Name = "Peter", Surname = "Bold"},
                new {Name = "Olya", Surname = "Lukina"},
                new {Name = "Bob", Surname = "Morley"}
            };

            var actual = collection
                .Select(n => new {n.Student.Name, n.Student.Surname})
                .Distinct()
                .OrderBy(n => n.Surname)
                .ThenBy(n => n.Name)
                .ToArray();

            Assert.AreEqual(expected, actual);
        }

        [TestCaseSource(typeof(BinarySearchTreeLinqTestCaseSource), nameof(BinarySearchTreeLinqTestCaseSource.NodeValuesCases))]
        public void TestGetHonorStudents_TestResultCollection_CorrectResult(IEnumerable<TestResult<int>> collection)
        {
            var expected = new[]
            {
                new Student("Michael", "Bold"),
                new Student("Bob", "Morley")
            };

            var actual = collection
                .GroupBy(n => n.Student)
                .Where(n => n.All(m => m.Mark > 3))
                .OrderBy(n => n.Key)
                .Select(n => n.Key);

            Assert.AreEqual(expected, actual);
        }

        [TestCaseSource(typeof(BinarySearchTreeLinqTestCaseSource), nameof(BinarySearchTreeLinqTestCaseSource.NodeValuesCases))]
        public void TestGetBadPassedTests_TestResultCollection_CorrectResult(IEnumerable<TestResult<int>> collection)
        {
            var expected = new[]
            {
                new Test("HTML-300", new DateTime(2021, 6, 21)),
                new Test("SQL-200", new DateTime(2020, 3, 1))
            };

            var actual = collection.Where(n => n.Mark == 2).Select(n => n.Test).Distinct().OrderBy(n => n).ToArray();

            Assert.AreEqual(expected, actual);
        }

        [TestCaseSource(typeof(BinarySearchTreeLinqTestCaseSource), nameof(BinarySearchTreeLinqTestCaseSource.NodeValuesCases))]
        public void TestGetAverageMarks_TestResultCollection_CorrectResult(IEnumerable<TestResult<int>> collection)
        {
            var expected = new[]
            {
                new {Name = "Alabay", Surname = "Abalaev", Average = 3.125},
                new {Name = "Michael", Surname = "Bold", Average = 4.2},
                new {Name = "Peter", Surname = "Bold", Average = 3.5},
                new {Name = "Olya", Surname = "Lukina", Average = 4.0},
                new {Name = "Bob", Surname = "Morley", Average = (double)31/7}
            };

            var actual =
                from c in collection.GroupBy(n => n.Student).OrderBy(n => n.Key)
                select c
                into student
                let average = student.Average(m => m.Mark)
                select new { student.Key.Name, student.Key.Surname, Average = average };

            Assert.AreEqual(expected, actual);
        }

        [TestCaseSource(typeof(BinarySearchTreeLinqTestCaseSource), nameof(BinarySearchTreeLinqTestCaseSource.GetTestResultsInMonthSource))]
        public int[] TestGetTestResultsInMonth_TestResultCollection_CorrectResult(IEnumerable<TestResult<int>> collection, int month, int year)
        {
            return collection.Where(n => n.Test.Date.Year == year && n.Test.Date.Month == month).Select(n => n.Mark).ToArray();
        }

        [TestCaseSource(typeof(BinarySearchTreeLinqTestCaseSource), nameof(BinarySearchTreeLinqTestCaseSource.NodeValuesCases))]
        public void TestGetNotPassedTestsOnThisYear_TestResultCollection_CorrectResult(IEnumerable<TestResult<int>> collection)
        {
            var expected = new[] { "CSS-100", "CSS-200", "HTML-100", "SQL-100", "SQL-200", "SQL-300", "SQL-300+", "SQL-400" };

            var actual = collection
                .GroupBy(c => c.Test.Name)
                .Where(c => c.All(n => n.Test.Date.Year != DateTime.Now.Year))
                .OrderBy(c => c.Key)
                .Select(c => c.Key);

            Assert.AreEqual(expected, actual);
        }

        [TestCaseSource(typeof(BinarySearchTreeLinqTestCaseSource), nameof(BinarySearchTreeLinqTestCaseSource.NodeValuesCases))]
        public void TestGetRepassedTests_TestResultCollection_CorrectResult(IEnumerable<TestResult<int>> collection)
        {
            var expected = new[]
            {
                new {TestName = "HTML-300", Name = "Alabay", Surname = "Abalaev"},
                new {TestName = "SQL-200", Name = "Alabay", Surname = "Abalaev"},
                new {TestName = "HTML-300", Name = "Olya", Surname = "Lukina"},
            };

            var actual = collection
                .GroupBy(c => new { TestName = c.Test.Name, c.Student.Name, c.Student.Surname})
                .Where(c => c.Count() > 1)
                .OrderBy(c => c.Key.Surname)
                .ThenBy(c => c.Key.Name)
                .ThenBy(c => c.Key.TestName)
                .Select(c => new { c.Key.TestName, c.Key.Name, c.Key.Surname });

            Assert.AreEqual(expected, actual);
        }

        [TestCaseSource(typeof(BinarySearchTreeLinqTestCaseSource), nameof(BinarySearchTreeLinqTestCaseSource.NodeValuesCases))]
        public void TestGetWrongTestNames_TestResultCollection_CorrectResult(IEnumerable<TestResult<int>> collection)
        {
            var expected = new[] { "SQL-300+", "SQL-400" };
            var pattern = @"^[A-Z]+[-][123]00$";

            var actual = collection.Where(n => Regex.IsMatch(n.Test.Name, pattern) == false).Select(n => n.Test.Name).OrderBy(n => n);

            Assert.AreEqual(expected, actual);
        }

        [TestCaseSource(typeof(BinarySearchTreeLinqTestCaseSource), nameof(BinarySearchTreeLinqTestCaseSource.NodeValuesCases))]
        public void TestGetStudentPassedTests_TestResultCollection_CorrectResult(IEnumerable<TestResult<int>> collection)
        {
            var expected = new[]
            {
                new TestList { Name = "Alabay Abalaev", Tests = new [] { "CSS-200", "CSS-300", "HTML-200", "HTML-300", "SQL-100", "SQL-200" }},
                new TestList { Name = "Michael Bold", Tests = new [] { "CSS-100", "HTML-300", "SQL-100", "SQL-200", "SQL-300" }},
                new TestList { Name = "Peter Bold", Tests = new [] { "HTML-200", "HTML-300" }},
                new TestList { Name = "Olya Lukina", Tests = new [] { "CSS-100", "CSS-200", "CSS-300", "HTML-100", "HTML-300" }},
                new TestList { Name = "Bob Morley", Tests = new [] { "CSS-100", "HTML-300", "SQL-100", "SQL-200", "SQL-300", "SQL-300+", "SQL-400" }}
            };

            var students = collection.Select(n => n.Student).Distinct().OrderBy(n => n);

            var tests = collection.Select(n => new { n.Student, n.Test.Name }).Distinct().OrderBy(n => n.Name);

            var actual = students.Select(n => new TestList
            {
                Name = string.Concat(n.Name, " ", n.Surname),
                Tests = tests
                    .Where(t => t.Student.Equals(n))
                    .Select(t => t.Name)
                    .ToArray()
            }).ToArray();

            Assert.AreEqual(expected, actual);
        }
    }
}
