﻿using System;
using System.Linq;

namespace BinarySearchTreeLinqTests
{
    internal class TestList
    {
        public string Name { get; set; }

        public string[] Tests { get; set; }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((TestList)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Name, Tests);
        }

        protected bool Equals(TestList other)
        {
            return Name.Equals(other.Name) && Tests.SequenceEqual(other.Tests);
        }
    }
}