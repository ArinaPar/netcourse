﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using Helpers;

namespace LinqTasksReinvent
{
    public static class LinqExtensions
    {
        public static IEnumerable<TSource> Repeat<TSource>(this IEnumerable<TSource> source, int count)
        {
            Validator.ThrowIfNull(source, nameof(source));

            using var sourceEnumerator = source.GetEnumerator();
            sourceEnumerator.MoveNext();
            var element = sourceEnumerator.Current;
            if (sourceEnumerator.MoveNext())
            {
                throw new ArgumentException();
            }

            for (int i = 0; i < count; i++)
            {
                yield return element;
            }
        }

        public static IEnumerable<TSource> Concat<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second)
        {
            Validator.ThrowIfNull(first, nameof(first));
            Validator.ThrowIfNull(second, nameof(second));

            foreach (var f in first)
            {
                yield return f;
            }

            foreach (var s in second)
            {
                yield return s;
            }
        }

        public static IEnumerable<TSource> Take<TSource>(this IEnumerable<TSource> source, int count)
        {
            Validator.ThrowIfNull(source, nameof(source));

            var counter = 0;
            using var sourceEnumerator = source.GetEnumerator();
            while (sourceEnumerator.MoveNext() && counter < count)
            {
                yield return sourceEnumerator.Current;
                counter++;
            }
        }

        public static TSource FirstOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
        {
            Validator.ThrowIfNull(source, nameof(source));
            Validator.ThrowIfNull(predicate, nameof(predicate));

            foreach (var s in source)
            {
                if (predicate(s))
                {
                    return s;
                }
            }

            return default;
        }

        public static List<TSource> ToList<TSource>(this IEnumerable<TSource> source)
        {
            Validator.ThrowIfNull(source, nameof(source));

            var result = new List<TSource>();
            foreach (var s in source)
            {
                result.Add(s);
            }

            return result;
        }

        public static IEnumerable<TSource> Where<TSource>(this IEnumerable<TSource> source, Func<TSource, int, bool> predicate)
        {
            Validator.ThrowIfNull(source, nameof(source));
            Validator.ThrowIfNull(predicate, nameof(predicate));

            int index = 0;
            foreach (var s in source)
            {
                if (predicate(s, index++))
                {
                    yield return s;
                }
            }
        }

        public static bool Any<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
        {
            Validator.ThrowIfNull(source, nameof(source));
            Validator.ThrowIfNull(predicate, nameof(predicate));

            foreach (var s in source)
            {
                if (predicate(s))
                {
                    return true;
                }
            }

            return false;
        }

        public static bool Contains<TSource>(this IEnumerable<TSource> source, TSource value, IEqualityComparer<TSource> comparer)
        {
            Validator.ThrowIfNull(source, nameof(source));
            Validator.ThrowIfNull(comparer, nameof(comparer));

            foreach (var s in source)
            {
                if (comparer.Equals(s, value))
                {
                    return true;
                }
            }

            return false;
        }

        public static IEnumerable<TResult> Select<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, int, TResult> selector)
        {
            Validator.ThrowIfNull(source, nameof(source));
            Validator.ThrowIfNull(selector, nameof(selector));

            var index = 0;
            foreach (var s in source)
            {
                yield return selector(s, index++);
            }
        }

        public static IEnumerable<TResult> SelectMany<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, IEnumerable<TResult>> selector)
        {
            Validator.ThrowIfNull(source, nameof(source));
            Validator.ThrowIfNull(selector, nameof(selector));

            foreach (var s in source)
            {
                foreach (var sel in selector(s))
                {
                    yield return sel;
                }
            }
        }

        public static TSource Aggregate<TSource>(this IEnumerable<TSource> source, Func<TSource, TSource, TSource> func)
        {
            Validator.ThrowIfNull(source, nameof(source));
            Validator.ThrowIfNull(func, nameof(func));

            TSource result = default;
            foreach (var s in source)
            {
                result = func(result, s);
            }

            return result;
        }

        public static IEnumerable<TSource> Distinct<TSource>(this IEnumerable<TSource> source)
        {
            Validator.ThrowIfNull(source, nameof(source));

            var set = new HashSet<TSource>();
            foreach (var s in source)
            {
                if (set.Add(s))
                {
                    yield return s;
                }
            }
        }

        public static IEnumerable<TSource> Intersect<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second)
        {
            Validator.ThrowIfNull(first, nameof(first));
            Validator.ThrowIfNull(second, nameof(second));

            var set1 = new HashSet<TSource>(first);
            var set2 = new HashSet<TSource>(second);
            foreach (var s in set2)
            {
                if (set1.Contains(s))
                {
                    yield return s;
                }
            }
        }

        public static IEnumerable<TResult> GroupBy<TSource, TKey, TElement, TResult>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, Func<TKey, IEnumerable<TElement>, TResult> resultSelector)
        {
            Validator.ThrowIfNull(source, nameof(source));
            Validator.ThrowIfNull(keySelector, nameof(keySelector));
            Validator.ThrowIfNull(elementSelector, nameof(elementSelector));
            Validator.ThrowIfNull(resultSelector, nameof(resultSelector));

            var set = new HashSet<TKey>();
            foreach (var s in source)
            {
                var key = keySelector(s);
                if (!set.Add(key))
                {
                    continue;
                }

                var elements = new List<TElement>();
                foreach (var s1 in source)
                {
                    if (keySelector(s1).Equals(key))
                    {
                        elements.Add(elementSelector(s1));
                    }
                }

                yield return resultSelector(key, elements);
            }
        }

        public static IEnumerable<TResult> Join<TOuter, TInner, TKey, TResult>(this IEnumerable<TOuter> outer, IEnumerable<TInner> inner, Func<TOuter, TKey> outerKeySelector, Func<TInner, TKey> innerKeySelector, Func<TOuter, TInner, TResult> resultSelector)
        {
            Validator.ThrowIfNull(outer, nameof(outer));
            Validator.ThrowIfNull(inner, nameof(inner));
            Validator.ThrowIfNull(outerKeySelector, nameof(outerKeySelector));
            Validator.ThrowIfNull(innerKeySelector, nameof(innerKeySelector));
            Validator.ThrowIfNull(resultSelector, nameof(resultSelector));

            foreach (var o in outer)
            {
                foreach (var i in inner)
                {
                    if (innerKeySelector(i).Equals(outerKeySelector(o)))
                    {
                        yield return resultSelector(o, i);
                    }
                }
            }
        }
    }
}