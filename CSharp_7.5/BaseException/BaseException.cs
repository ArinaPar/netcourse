﻿using System;
using System.Reflection;
using System.Runtime.Serialization;

namespace ApplicationExceptionInherit
{
    public class BaseException : ApplicationException
    {
        public Guid Guid { get; private set; }
        public MethodBase ConstructorName { get; private set; }
        public Version NetVersion => Environment.Version;

        public BaseException()
        {
            InitGuidAndConstructorName(MethodBase.GetCurrentMethod());
        }

        public BaseException(string message) : base(message)
        {
            InitGuidAndConstructorName(MethodBase.GetCurrentMethod());
        }

        public BaseException(string message, Exception innerException) : base(message, innerException)
        {
            InitGuidAndConstructorName(MethodBase.GetCurrentMethod());
        }

        protected BaseException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            InitGuidAndConstructorName(MethodBase.GetCurrentMethod());
        }

        private void InitGuidAndConstructorName(MethodBase mb)
        {
            Guid = Guid.NewGuid();
            ConstructorName = mb;
        }
    }
}
