﻿using System;
using System.Collections;
using ApplicationExceptionInherit;

namespace ExceptionProcessorUI
{
    public class Program
    {
        public static void Main()
        {
            UseDeafTryCatch();
            UseNewBaseException();
            UseNewBaseExceptionWithInner();
            UseRethrownException();
            UseRethrownOriginalException();
        }

        private static void DoBoom()
        {
            int zero = 0;
            _ = 5 / zero;
        }

        private static void UseDeafTryCatch()
        {
            try
            {
                DoBoom();
            }
            catch 
            {
                //я, Паринова Арина Дмитриевна, сделала это в учебных целях. больше никогда так делать не буду! честно!! 1.11.21 *подпись*
            }
        }

        private static void UseNewBaseException()
        {
            try
            {
                GenerateNewBaseException();
            }
            catch (BaseException bex)
            {
                WriteBaseExceptionInfo(bex);
            }
        }

        private static void UseNewBaseExceptionWithInner()
        {
            try
            {
                GenerateNewBaseExceptionWithInner();
            }
            catch (BaseException bex)
            {
                WriteBaseExceptionInfo(bex);
            }
        }

        private static void UseRethrownException()
        {
            try
            {
                RethrowException();
            }
            catch (DivideByZeroException ex)
            {
                WriteExceptionInfo(ex);
            }
        }

        private static void UseRethrownOriginalException()
        {
            try
            {
                RethrowOriginalException();
            }
            catch (DivideByZeroException ex)
            {
                WriteExceptionInfo(ex);
            }
        }

        private static void GenerateNewBaseException()
        {
            try
            {
                DoBoom();
            }
            catch
            {
                throw new BaseException();
            }
        }

        private static void GenerateNewBaseExceptionWithInner()
        {
            try
            {
                DoBoom();
            }
            catch (DivideByZeroException ex)
            {
                throw new BaseException(ex.Message, ex);
            }
        }

        private static void RethrowException()
        {
            try
            {
                DoBoom();
            }
            catch (DivideByZeroException ex)
            {
                ex.Data.Add("Rethrowed", DateTime.Now);
                throw;
            }
        }

        private static void RethrowOriginalException()
        {
            try
            {
                DoBoom();
            }
            catch (DivideByZeroException ex)
            {
                ex.Data.Add("Rethrowed", DateTime.Now);
                throw ex;
            }
        }

        private static void WriteBaseExceptionInfo(BaseException bex)
        {
            WriteExceptionInfo(bex);
            WriteSpecialBaseExceptionInfo(bex);
        }

        private static void WriteSpecialBaseExceptionInfo(BaseException bex)
        {
            Console.WriteLine($"GUID: {bex.Guid}");
            Console.WriteLine($"Constructor name: {bex.ConstructorName}");
            Console.WriteLine($"Net version: {bex.NetVersion}");
        }

        private static void WriteExceptionInfo(Exception ex)
        {
            Console.WriteLine($"\nException type: {ex.GetType()}");
            Console.WriteLine($"Message: {ex.Message}");
            Console.WriteLine($"Source: {ex.Source}");
            Console.WriteLine($"Stack trace: {ex.StackTrace}");
            Console.WriteLine($"Targer site: {ex.TargetSite}");
            if (ex.Data.Count > 0)
            {
                Console.WriteLine("Data:");
                foreach (DictionaryEntry de in ex.Data)
                {
                    Console.WriteLine($"    Key: {de.Key,-10}      Value: {de.Value}");
                }
            }

            if (!(ex.InnerException is null))
            {
                Console.WriteLine($"Inner exception (see below):");
                WriteExceptionInfo(ex.InnerException);
            }
        }
    }
}
