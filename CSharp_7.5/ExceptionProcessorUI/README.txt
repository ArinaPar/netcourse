Application demostraits different ways of exception handling:

1. Using deaf try-catch construction.
2. Generation of new BaseException.
3. Generation of new BaseException passing inner exception into it.
4. Using statement throw.
5. Using statement throw for the base exception regeneration.

It writes exception info: 
- type;
- message;
- source;
- stack trace;
- target site;
- data (containing info about then the exception was rethrown);
- inner exception's info.

If the BaseException is handling, it also writes GUID, constructor name and NET version.

The result of .pdb file deleting is stack trace simplification.
It keeps only method(-s) name(-s) without specifying the line where the exception occured.