﻿namespace Entities
{
    [Serializable]
    public class Condition
    {
        public string Property { get; }

        public string Operation { get; }

        public string ValueA { get; }

        public string? ValueB { get; }

        public Condition(string property, string operation, string valueA, string? valueB = default)
        {
            Property = property;
            Operation = operation;
            ValueA = valueA;
            ValueB = valueB;
        }
    }
}
