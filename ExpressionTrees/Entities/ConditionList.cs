﻿namespace Entities
{
    [Serializable]
    public class ConditionList
    {
        public string Name { get; }

        public Condition[] Conditions { get; }

        public ConditionList(string name, Condition[] filters)
        {
            Name = name;
            Conditions = new Condition[filters.Length];
            filters.CopyTo(Conditions, 0);
        }
    }
}
