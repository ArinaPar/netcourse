﻿using System.Xml.Serialization;

namespace Entities
{
    [Serializable]
    public class TestResult<TMark> : IComparable<TestResult<TMark>> where TMark : IComparable<TMark>
    {
        [XmlAttribute(AttributeName = "StudentName")]
        public string StudentName { get; set; }

        [XmlAttribute(AttributeName = "StudentSurname")]
        public string StudentSurname { get; set; }

        [XmlAttribute(AttributeName = "TestName")]
        public string TestName { get; set; }

        [XmlAttribute(AttributeName = "TestDate")]
        public DateTime TestDate { get; set; }

        [XmlAttribute(AttributeName = "Mark")]
        public TMark Mark { get; set; }

        public TestResult(string studentName, string studentSurname, string testName, DateTime testDate, TMark mark)
        {
            StudentName = studentName;
            StudentSurname = studentSurname;
            TestName = testName;
            TestDate = testDate;
            Mark = mark;
        }

        protected TestResult()
        {

        }

        public override bool Equals(object? obj)
        {
            return Equals(obj as TestResult<TMark>);
        }

        protected bool Equals(TestResult<TMark>? other)
        {
            return !ReferenceEquals(null, other) && GetHashCode() == other.GetHashCode();
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(StudentName, StudentSurname, TestName, TestDate, Mark);
        }

        public override string ToString()
        {
            return ($"{StudentName} {StudentSurname} got {Mark} for test {TestName} on {TestDate}");
        }

        public int CompareTo(TestResult<TMark>? other)
        {
            if (ReferenceEquals(this, other))
            {
                return 0;
            }

            if (ReferenceEquals(null, other))
            {
                return 1;
            }

            if (!CheckCompareResultIsZero(StudentSurname, other.StudentSurname, out int compareResult)
                || !CheckCompareResultIsZero(StudentName, other.StudentName, out compareResult)
                || !CheckCompareResultIsZero(TestName, other.TestName, out compareResult)
                || !CheckCompareResultIsZero(TestDate, other.TestDate, out compareResult))
            {
                return compareResult;
            }

            return Mark.CompareTo(other.Mark);
        }

        private bool CheckCompareResultIsZero<T>(T valueA, T valueB, out int compareResult) where T : IComparable
        {
            return (compareResult = valueA.CompareTo(valueB)) == 0;
        }
    }
}