﻿using System.Linq.Expressions;
using System.Reflection;

namespace GenericFilter
{
    public class Filter
    {
        private Expression? expression;
        private readonly ParameterExpression parameterExpression;

        public Filter(Type t)
        {
            parameterExpression = Expression.Parameter(t);
        }

        public IEnumerable<T> Apply<T>(IEnumerable<T> collection)
        {
            if (expression is not null)
            {
                Expression<Func<T, bool>> lambda = Expression.Lambda<Func<T, bool>>(expression, parameterExpression);
                var action = lambda.Compile();
                var result = new List<T>();
                foreach (var c in collection)
                {
                    if (action(c))
                    {
                        result.Add(c);
                    }
                }

                return result;
            }

            return new List<T>(collection);
        }

        public void AddConditionIfEquals<T>(string propertyName, T parameter) where T : IComparable<T>
        {
            BinaryExpression condition = CompareWithMethod(propertyName, parameter, Expression.Equal);
            Add(condition);
        }

        public void AddConditionIfBigger<T>(string propertyName, T parameter) where T : IComparable<T>
        {
            BinaryExpression condition = CompareWithMethod(propertyName, parameter, Expression.GreaterThan);
            Add(condition);
        }

        public void AddConditionIfLess<T>(string propertyName, T parameter) where T : IComparable<T>
        {
            BinaryExpression condition = CompareWithMethod(propertyName, parameter, Expression.LessThan);
            Add(condition);
        }

        public void AddConditionIfInRange<T>(string propertyName, T lowerBorder, T upperBorder) where T : IComparable<T>
        {
            BinaryExpression greaterExpression = CompareWithMethod(propertyName, lowerBorder, Expression.GreaterThanOrEqual);
            BinaryExpression lessExpression = CompareWithMethod(propertyName, upperBorder, Expression.LessThanOrEqual);
            BinaryExpression condition = Expression.And(greaterExpression, lessExpression);
            Add(condition);
        }

        public void AddConditionIfContains(string propertyName, string parameter)
        {
            MethodCallExpression containsExpression = CallMethod("Contains", propertyName, parameter);
            Add(containsExpression);
        }

        private BinaryExpression CompareWithMethod<T>(string propertyName, T parameter, Func<Expression, Expression, BinaryExpression> compareMethod) where T : IComparable<T>
        {
            MethodCallExpression compareExpression = CallMethod("CompareTo", propertyName, parameter);
            ConstantExpression zero = Expression.Constant(0, typeof(int));
            return compareMethod(compareExpression, zero);
        }

        private MethodCallExpression CallMethod<T>(string methodName, string propertyName, T parameter) where T : IComparable<T>
        {
            var memberExpression = Expression.Property(parameterExpression, propertyName);
            var constantExpression = Expression.Constant(parameter, typeof(T));
            MethodInfo? methodInfo = typeof(T).GetMethod(methodName, new[] { typeof(T) });
            return Expression.Call(memberExpression, methodInfo, constantExpression);
        }

        private void Add(Expression condition)
        {
            expression = expression is not null ? Expression.And(expression, condition) : condition;
        }
    }
}