﻿using System.Collections;
using NUnit.Framework;
using Entities;

namespace GenericFilterTests
{
    internal static class GenericFilterTestCaseSource
    {
        internal static IEnumerable<IEnumerable<TestResult<int>>> NodeValuesCases
        {
            get
            {
                yield return NodeValuesSource;
            }
        }

        internal static IEnumerable GetTestResultsInMonthSource
        {
            get
            {
                yield return new TestCaseData(NodeValuesSource, 2, 2022).Returns(new[] { 4, 4, 5 });
                yield return new TestCaseData(NodeValuesSource, 12, 2021).Returns(new[] { 4, 4, 4 });
                yield return new TestCaseData(NodeValuesSource, 3, 2020).Returns(new[] { 2, 5, 5 });
                yield return new TestCaseData(NodeValuesSource, 3, 2021).Returns(new[] { 4 });
                yield return new TestCaseData(NodeValuesSource, 4, 2022).Returns(new int[] { });
            }
        }

        internal static IEnumerable GetThreeMaxMarksSource
        {
            get
            {
                yield return new TestCaseData(NodeValuesSource, "HTML-300").Returns(new[] { 5, 5, 4 });
                yield return new TestCaseData(NodeValuesSource, "CSS-100").Returns(new[] { 5, 4, 4 });
                yield return new TestCaseData(NodeValuesSource, "SQL-300+").Returns(new[] { 4 });
                yield return new TestCaseData(NodeValuesSource, "SQL-200").Returns(new[] { 5, 5, 3 });
                yield return new TestCaseData(NodeValuesSource, "HTML-100").Returns(new[] { 4 });
                yield return new TestCaseData(NodeValuesSource, "CSS-300").Returns(new[] { 3, 3 });
                yield return new TestCaseData(NodeValuesSource, "SQL-100").Returns(new[] { 4, 4, 3 });
            }
        }

        private static readonly IEnumerable<TestResult<int>> NodeValuesSource =
            new List<TestResult<int>>
            {
                new("Michael", "Bold", "HTML-300", new DateTime(2022, 2, 2), 4),
                new("Peter", "Bold", "HTML-300", new DateTime(2022, 2, 2), 4),
                new("Olya", "Lukina", "HTML-100", new DateTime(2021, 12, 21), 4),
                new("Alabay", "Abalaev", "SQL-200", new DateTime(2020, 3, 1), 2),
                new("Bob", "Morley", "CSS-100", new DateTime(2021, 12, 2), 4),
                new("Michael", "Bold", "CSS-100", new DateTime(2021, 12, 2), 4),
                new("Alabay", "Abalaev", "SQL-200", new DateTime(2021, 4, 4), 3),
                new("Olya", "Lukina", "CSS-200", new DateTime(2021, 5, 21), 4),
                new("Alabay", "Abalaev", "HTML-200", new DateTime(2022, 1, 21), 4),
                new("Bob", "Morley", "SQL-200", new DateTime(2020, 3, 1), 5),
                new("Alabay", "Abalaev", "HTML-300", new DateTime(2021, 6, 21), 2),
                new("Alabay", "Abalaev", "HTML-300", new DateTime(2022, 1, 21), 4),
                new("Bob", "Morley", "SQL-400", new DateTime(2021, 8, 5), 5),
                new("Bob", "Morley", "SQL-100", new DateTime(2020, 1, 1), 4),
                new("Olya", "Lukina", "CSS-300", new DateTime(2022, 1, 21), 3),
                new("Bob", "Morley", "HTML-300", new DateTime(2022, 2, 2), 5),
                new("Bob", "Morley", "SQL-300+", new DateTime(2021, 10, 25), 4),
                new("Peter", "Bold", "HTML-200", new DateTime(2021, 2, 2), 3),
                new("Olya", "Lukina", "CSS-100", new DateTime(2021, 1, 21), 5),
                new("Alabay", "Abalaev", "CSS-300", new DateTime(2022, 1, 10), 3),
                new("Bob", "Morley", "SQL-300", new DateTime(2021, 2, 21), 4),
                new("Michael", "Bold", "SQL-100", new DateTime(2020, 1, 1), 4),
                new("Alabay", "Abalaev", "SQL-100", new DateTime(2020, 1, 1), 3),
                new("Michael", "Bold", "SQL-300", new DateTime(2021, 2, 21), 4),
                new("Olya", "Lukina", "HTML-300", new DateTime(2020, 1, 21), 3),
                new("Michael", "Bold", "SQL-200", new DateTime(2020, 3, 1), 5),
                new("Olya", "Lukina", "HTML-300", new DateTime(2022, 1, 21), 5),
                new("Alabay", "Abalaev", "CSS-200", new DateTime(2021, 3, 14), 4)
            };
    }
}
