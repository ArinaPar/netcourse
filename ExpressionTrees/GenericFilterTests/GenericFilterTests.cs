﻿using NUnit.Framework;
using Entities;
using GenericFilter;

namespace GenericFilterTests
{
    public class GenericFilterTests
    {
        [TestCaseSource(typeof(GenericFilterTestCaseSource), nameof(GenericFilterTestCaseSource.NodeValuesCases))]
        public void TestAddConditionIfEquals_TestResultCollection_CorrectResult(IEnumerable<TestResult<int>> collection)
        {
            var expected = collection.Where(c => c.TestName.Equals("HTML-300"));

            var filter = new Filter(typeof(TestResult<int>));
            filter.AddConditionIfEquals("TestName", "HTML-300");
            var actual = filter.Apply(collection);

            Assert.AreEqual(expected, actual);
        }

        [TestCaseSource(typeof(GenericFilterTestCaseSource), nameof(GenericFilterTestCaseSource.NodeValuesCases))]
        public void TestAddConditionIfBigger_TestResultCollection_CorrectResult(IEnumerable<TestResult<int>> collection)
        {
            var expected = collection.Where(c => c.Mark > 3);

            var filter = new Filter(typeof(TestResult<int>));
            filter.AddConditionIfBigger("Mark", 3);
            var actual = filter.Apply(collection);

            Assert.AreEqual(expected, actual);
        }

        [TestCaseSource(typeof(GenericFilterTestCaseSource), nameof(GenericFilterTestCaseSource.NodeValuesCases))]
        public void TestAddConditionIfBiggerString_TestResultCollection_CorrectResult(IEnumerable<TestResult<int>> collection)
        {
            var expected = collection.Where(c => c.StudentSurname.CompareTo("4") > 0);

            var filter = new Filter(typeof(TestResult<int>));
            filter.AddConditionIfBigger("StudentSurname", "4");
            var actual = filter.Apply(collection);

            Assert.AreEqual(expected, actual);
        }

        [TestCaseSource(typeof(GenericFilterTestCaseSource), nameof(GenericFilterTestCaseSource.NodeValuesCases))]
        public void TestAddConditionIfInRange_TestResultCollection_CorrectResult(IEnumerable<TestResult<int>> collection)
        {
            var expected = collection
                .Where(c => c.TestDate > new DateTime(2020, 12, 31))
                .Where(c => c.TestDate < new DateTime(2022, 01, 01));

            var filter = new Filter(typeof(TestResult<int>));
            filter.AddConditionIfInRange("TestDate", new DateTime(2020, 12, 31), new DateTime(2022, 01, 01));
            var actual = filter.Apply(collection);

            Assert.AreEqual(expected, actual);
        }

        [TestCaseSource(typeof(GenericFilterTestCaseSource), nameof(GenericFilterTestCaseSource.NodeValuesCases))]
        public void TestAddConditionIfLess_TestResultCollection_CorrectResult(IEnumerable<TestResult<int>> collection)
        {
            var expected = collection.Where(c => c.Mark < 4);

            var filter = new Filter(typeof(TestResult<int>));
            filter.AddConditionIfLess("Mark", 4);
            var actual = filter.Apply(collection);

            Assert.AreEqual(expected, actual);
        }

        [TestCaseSource(typeof(GenericFilterTestCaseSource), nameof(GenericFilterTestCaseSource.NodeValuesCases))]
        public void TestAddConditionIfContains_TestResultCollection_CorrectResult(IEnumerable<TestResult<int>> collection)
        {
            var expected = collection.Where(c => c.StudentSurname.Contains("e"));

            var filter = new Filter(typeof(TestResult<int>));
            filter.AddConditionIfContains("StudentSurname", "e");
            var actual = filter.Apply(collection);

            Assert.AreEqual(expected, actual);
        }

        [TestCaseSource(typeof(GenericFilterTestCaseSource), nameof(GenericFilterTestCaseSource.NodeValuesCases))]
        public void TestSeveralConditions_TestResultCollection_CorrectResult(IEnumerable<TestResult<int>> collection)
        {
            var expected = collection
                .Where(c => c.Mark > 3)
                .Where(c => c.StudentSurname.Contains("e"))
                .Where(c => c.TestDate > new DateTime(2020, 12, 31))
                .Where(c => c.TestDate < new DateTime(2022, 01, 01));

            var filter = new Filter(typeof(TestResult<int>));
            filter.AddConditionIfBigger("Mark", 3);
            filter.AddConditionIfContains("StudentSurname", "e");
            filter.AddConditionIfInRange("TestDate", new DateTime(2020, 12, 31), new DateTime(2022, 01, 01));
            var actual = filter.Apply(collection);

            Assert.AreEqual(expected, actual);
        }
    }
}