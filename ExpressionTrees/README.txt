This class library contains a "Filter" class for working with generic collections.
The implementation is based on expression trees and allows to make dynamic queries.
The solution contains tests using collections of TestResult class.
TestResult is a type that includes information about the student, the test and the mark.