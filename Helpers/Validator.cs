﻿using System;

namespace Helpers
{
    public static class Validator
    {
        public static void ThrowIfNull(object param, string paramName)
        {
            if (Equals(param, null))
            {
                throw new ArgumentNullException(paramName, "Error! Value was NULL");
            }
        }

        public static void ThrowIfOutOfRange(float param, float rangeBound, string paramName)
        {
            if (param <= rangeBound)
            {
                throw new ArgumentOutOfRangeException(paramName, $"Error! Value should be bigger than {rangeBound}");
            }
        }
    }
}
