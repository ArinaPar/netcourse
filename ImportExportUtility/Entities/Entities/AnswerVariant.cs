﻿namespace ImportExportUtility.Data.Entities
{
    public class AnswerVariant
    {
        public int Id { get; set; }

        public int QuestionId { get; set; }

        public virtual Question Question { get; set; } = new Question();

        public string Text { get; set; } = string.Empty;

        public bool IsCorrect { get; set; }
    }
}
