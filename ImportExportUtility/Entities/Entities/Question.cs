﻿namespace ImportExportUtility.Data.Entities
{
    public class Question
    {
        public int Id { get; set; }

        public int TestId { get; set; }

        public virtual Test Test { get; set; } = new Test();

        public string Text { get; set; } = string.Empty;

        public virtual List<AnswerVariant> AnswerVariants { get; set; } = new List<AnswerVariant>();
    }
}
