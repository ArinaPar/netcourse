﻿namespace ImportExportUtility.Data.Entities
{
    public class Test
    {
        public int Id { get; set; }

        public string TestName { get; set; }

        public Guid Guid { get; set; } = Guid.NewGuid();

        public virtual Theory Theory { get; set; }

        public bool IsTheoryNeeded { get; set; }

        public Uri? Url { get; set; }

        public TimeSpan ExecutionTime { get; set; }

        public virtual List<Question> Questions { get; set; } = new List<Question>();

        public int QuestionCount { get; set; }

        public int AnswerCount { get; set; }

        public int? MaxMark { get; set; }
    }
}