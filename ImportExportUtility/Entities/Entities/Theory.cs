﻿namespace ImportExportUtility.Data.Entities
{
    public class Theory
    {
        public int Id { get; set; }

        public string Text { get; set; } = string.Empty;

        public int TestId { get; set; }

        public virtual Test Test { get; set; }
    }
}
