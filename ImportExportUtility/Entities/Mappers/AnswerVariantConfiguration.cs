﻿using ImportExportUtility.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ImportExportUtility.Data.Mappers
{
    internal class AnswerVariantConfiguration : IEntityTypeConfiguration<AnswerVariant>
    {
        public void Configure(EntityTypeBuilder<AnswerVariant> builder)
        {
            builder.HasOne(b => b.Question)
                .WithMany(q => q.AnswerVariants)
                .HasForeignKey(b => b.QuestionId);
        }
    }
}
