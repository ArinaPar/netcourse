﻿using ImportExportUtility.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ImportExportUtility.Data.Mappers
{
    internal class QuestionConfiguration : IEntityTypeConfiguration<Question>
    {
        public void Configure(EntityTypeBuilder<Question> builder)
        {
            builder.HasOne(b => b.Test)
                .WithMany(t => t.Questions)
                .HasForeignKey(b => b.TestId);
        }
    }
}
