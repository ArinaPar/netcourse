﻿using ImportExportUtility.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ImportExportUtility.Data.Mappers
{
    public class TestConfiguration : IEntityTypeConfiguration<Test>
    {
        public void Configure(EntityTypeBuilder<Test> builder)
        {
            builder.HasOne(b => b.Theory)
                .WithOne(t => t.Test)
                .HasForeignKey<Theory>(b => b.TestId);
        }
    }
}