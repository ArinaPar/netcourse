﻿using ImportExportUtility.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ImportExportUtility.Data.Mappers
{
    internal class TheoryConfiguration : IEntityTypeConfiguration<Theory>
    {
        public void Configure(EntityTypeBuilder<Theory> builder)
        {
            builder.HasOne(b => b.Test)
                .WithOne(t => t.Theory)
                .HasForeignKey<Theory>(t => t.TestId);
        }
    }
}
