﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ImportExportUtility.Data.Migrations
{
    public partial class MaxMark : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "MaxMark",
                table: "Tests",
                type: "int",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MaxMark",
                table: "Tests");
        }
    }
}
