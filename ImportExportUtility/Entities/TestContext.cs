﻿#pragma warning disable CS8618
using ImportExportUtility.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace ImportExportUtility.Data
{
    public class TestContext : DbContext
    {
        private const string LogFilePath = "log.txt";

        public DbSet<Test> Tests { get; set; }

        public DbSet<Question> Questions { get; set; }

        public DbSet<AnswerVariant> AnswerVariants { get; set; }

        public DbSet<Theory> Theories { get; set; }

        private readonly StreamWriter writer;

        public TestContext(DbContextOptions<TestContext> options) : base(options)
        {
            writer = new StreamWriter(LogFilePath);
        }

        public override void Dispose()
        {
            base.Dispose();
            writer.Dispose();
        }

        public override async ValueTask DisposeAsync()
        {
            await base.DisposeAsync();
            await writer.DisposeAsync();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Test>();
            modelBuilder.Entity<Question>();
            modelBuilder.Entity<AnswerVariant>();
            modelBuilder.Entity<Theory>();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.LogTo(writer.WriteLine);
        }
    }
}
#pragma warning restore CS8618
