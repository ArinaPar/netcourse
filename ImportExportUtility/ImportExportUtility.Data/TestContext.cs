﻿using ImportExportUtility.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ImportExportUtility.DAL
{
    public class TestContext : DbContext
    {
        public DbSet<Test> Tests { get; set; }

        public DbSet<Question> Questions { get; set; }

        public DbSet<AnswerVariant> AnswerVariants { get; set; }

        public DbSet<Theory> Theories { get; set; }

        public TestContext(DbContextOptions<TestContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Test>();
            modelBuilder.Entity<Question>();
            modelBuilder.Entity<AnswerVariant>();
            modelBuilder.Entity<Theory>();
        }
    }
}