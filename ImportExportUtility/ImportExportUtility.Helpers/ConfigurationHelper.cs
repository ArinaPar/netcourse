﻿using Microsoft.Extensions.Configuration;

namespace ImportExportUtility.Helpers
{
    public class ConfigurationHelper
    {
        private const string fileName = "appsettings.json";

        public static string GetDefaultConnectionString()
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory());
            builder.AddJsonFile(fileName);
            var config = builder.Build();
            return config.GetConnectionString("DefaultConnection");
        }
    }
}