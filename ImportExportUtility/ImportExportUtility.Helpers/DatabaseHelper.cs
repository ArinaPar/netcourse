﻿using ImportExportUtility.Data;
using ImportExportUtility.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace ImportExportUtility.Helpers
{
    public class DatabaseHelper
    {
        public static bool TryGetTestById(int testId, TestContext context, out Test result)
        {
            using var transaction = context.Database.BeginTransaction();
            try
            {
                result = context.Tests.FirstOrDefault(t => t.Id == testId) ?? new Test();
                transaction.Commit();
            }
            catch
            {
                transaction.Rollback();
                result = new Test();
            }

            return result.Id != 0;
        }

        public static void AddToDatabase(Test test, TestContext context)
        {
            ResetIds(test);
            using var transaction = context.Database.BeginTransaction();
            try
            {
                context.Tests.Add(test);
                context.SaveChanges();
                transaction.Commit();
            }
            catch
            {
                transaction.Rollback();
            }
        }

        public static DbContextOptionsBuilder<TestContext> CreateOptionsBuilder()
        {
            var dbContextOptionsBuilder = new DbContextOptionsBuilder<TestContext>();
            var connectionString = ConfigurationHelper.GetDefaultConnectionString();
            dbContextOptionsBuilder.UseLazyLoadingProxies();
            dbContextOptionsBuilder.UseSqlServer(connectionString, opts => opts.CommandTimeout((int)TimeSpan.FromMinutes(10).TotalSeconds));
            return dbContextOptionsBuilder;
        }

        private static void ResetIds(Test test)
        {
            test.Id = default;
            test.Theory.Id = default;
            foreach (var question in test.Questions)
            {
                question.Id = default;
                foreach (var answerVariant in question.AnswerVariants)
                {
                    answerVariant.Id = default;
                }
            }
        }
    }
}
