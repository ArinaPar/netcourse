﻿using System.Text.Json;
using System.Text.Json.Serialization;
using ImportExportUtility.Data.Entities;

namespace ImportExportUtility.Helpers
{
    public class FileHelper
    {
        public static void ExecuteExport(Test test, string fileName)
        {
            var str = JsonSerializer.Serialize(test, new JsonSerializerOptions() { ReferenceHandler = ReferenceHandler.Preserve});
            File.WriteAllText(fileName, str);
        }

        public static bool TryImportTest(string fileName, out Test test)
        {
            var jsonString = File.ReadAllText(fileName);
            test = JsonSerializer.Deserialize<Test>(jsonString, new JsonSerializerOptions() { ReferenceHandler = ReferenceHandler.Preserve }) ?? new Test();
            return test.Id != 0;
        }
    }
}
