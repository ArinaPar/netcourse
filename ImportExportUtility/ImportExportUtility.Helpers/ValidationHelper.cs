﻿using System.Text.RegularExpressions;

namespace ImportExportUtility.Helpers
{
    public class ValidationHelper
    {
        public static bool Check(string jsonFileName)
        {
            const string pattern = @"^\w+.json *$";
            var regex = new Regex(pattern);
            return regex.IsMatch(jsonFileName);
        }
    }
}
