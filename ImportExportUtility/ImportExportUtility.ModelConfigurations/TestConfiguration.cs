﻿using ImportExportUtility.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ImportExportUtility.ModelConfigurations
{
    public class TestConfiguration : IEntityTypeConfiguration<Test>
    {
        public void Configure(EntityTypeBuilder<Test> builder)
        {
            builder.HasOne(b => b.Theory)
                .WithOne(t => t.Test)
                .HasForeignKey<Theory>(b => b.TestId);
        }
    }
}