﻿using ImportExportUtility.Data;
using ImportExportUtility.Helpers;

if (args?.Length > 0)
{
    string fileName;
    if (args?.Length == 3 && args[0] is "-e" or "--export" && int.TryParse(args[1], out var testId) && ValidationHelper.Check(fileName = args[2]))
    {
        var optionsBuilder = DatabaseHelper.CreateOptionsBuilder();
        using var context = new TestContext(optionsBuilder.Options);

        if (DatabaseHelper.TryGetTestById(testId, context, out var test))
        {
            FileHelper.ExecuteExport(test, fileName);
        }
        else
        {
            Console.WriteLine("Test is not found. Check Test Id");
        }
    }
    else if (args?.Length == 2 && args[0] is "-i" or "--import" && ValidationHelper.Check(fileName = args[1]))
    {
        var optionsBuilder = DatabaseHelper.CreateOptionsBuilder();
        using var context = new TestContext(optionsBuilder.Options);

        if (FileHelper.TryImportTest(fileName, out var test))
        {
            DatabaseHelper.AddToDatabase(test, context);
            Console.WriteLine($"TestId: {test.Id}");
            Console.WriteLine($"TestName: {test.TestName}");
            Console.WriteLine($"AnswerCount: {test.AnswerCount}");
            Console.WriteLine($"ExecutionTime: {test.ExecutionTime}");
            Console.WriteLine($"IsTheoryNeeded: {test.IsTheoryNeeded}");
            Console.WriteLine($"QuestionCount: {test.QuestionCount}");
            Console.WriteLine($"Url: {test.Url}");
            Console.WriteLine($"Guid: {test.Guid}");

        }
        else
        {
            Console.WriteLine("Error while reading the file. Check File Name");
        }
    }
}