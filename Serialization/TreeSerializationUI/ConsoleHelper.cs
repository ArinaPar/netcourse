﻿using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using BinarySearchTreeProcessor;

namespace TreeSerializationUI
{
    internal static class ConsoleHelper
    {
        internal static void DemonstrateBinarySerialization<T>(BinarySearchTree<T> tree, string treeName) where T : IComparable<T>
        {
            Console.WriteLine($"Binary serialization of tree of {typeof(T).Name}");
            var formatter = new BinaryFormatter();
            var fileName = string.Concat(treeName, ".dat");
            SerializationHelper.MakeBinarySerialization(tree, formatter, fileName);
            Console.WriteLine(SerializationHelper.MakeBinaryDeserialization<T>(formatter, fileName));
        }

        internal static void DemonstrateXmlSerialization<T>(BinarySearchTree<T> tree, string treeName) where T : IComparable<T>
        {
            Console.WriteLine($"XML serialization of tree of {typeof(T).Name}");
            var formatter = new XmlSerializer(tree.GetType());
            var fileName = string.Concat(treeName, ".xml");
            SerializationHelper.MakeXmlSerialization(tree, formatter, fileName);
            Console.WriteLine(SerializationHelper.MakeXmlDeserialization<T>(formatter, fileName));
        }
    }
}
