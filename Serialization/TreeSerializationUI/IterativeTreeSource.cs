﻿using BinarySearchTreeProcessor;
using Entities;

namespace TreeSerializationUI
{
    internal static class IterativeTreeSource
    {
        internal static IterativeTree<int> IntTree = new() { 4, 2, 46, 8, 12, 9 };

        internal static IterativeTree<string> StringTree = new() { "ab", "uy", "nji", "dd", "aba", "qjeeel", "tet", "ze" };

        internal static IterativeTree<TestResult<int>> TestResultTree = new()
        {
            new TestResult<int>("Olya", "Lukina","SQL-300", new DateTime(2021, 5, 3), 4),
            new TestResult<int>("Alabay", "Abalaev","SQL-200", new DateTime(2020, 3, 1), 2),
            new TestResult<int>("Bob", "Morley","CSS-100", new DateTime(2021, 12, 2), 4),
            new TestResult<int>("Michael", "Bold","CSS-100", new DateTime(2021, 12, 2), 4),
            new TestResult<int>("Alabay", "Abalaev","SQL-200", new DateTime(2021, 4, 4), 3),
            new TestResult<int>("Olya", "Lukina","CSS-200", new DateTime(2021, 5, 21), 4)
        };
    }
}
