﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using BinarySearchTreeProcessor;

namespace TreeSerializationUI
{
    internal static class SerializationHelper
    {
        internal static void MakeBinarySerialization<T>(BinarySearchTree<T> tree, IFormatter formatter, string filePath) where T : IComparable<T>
        {
            using var fs = new FileStream(filePath, FileMode.Create);
            formatter.Serialize(fs, tree);
        }

        internal static BinarySearchTree<T> MakeBinaryDeserialization<T>(IFormatter formatter, string filePath) where T : IComparable<T>
        {
            using var fs = new FileStream(filePath, FileMode.OpenOrCreate);
            return (IterativeTree<T>)formatter.Deserialize(fs);
        }

        internal static void MakeXmlSerialization<T>(BinarySearchTree<T> tree, XmlSerializer formatter, string filePath) where T : IComparable<T>
        {
            using var fs = new FileStream(filePath, FileMode.Create);
            formatter.Serialize(fs, tree);
        }

        internal static BinarySearchTree<T> MakeXmlDeserialization<T>(XmlSerializer formatter, string filePath) where T : IComparable<T>
        {
            using var fs = new FileStream(filePath, FileMode.OpenOrCreate);
            return (IterativeTree<T>)formatter.Deserialize(fs);
        }
    }
}
