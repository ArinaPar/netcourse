﻿using System.Drawing;
using System.Windows.Forms;

namespace Browser.Components
{
    partial class ConditionContainer : FlowLayoutPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            lblProperty = new Label();
            cbProperty = new ComboBox();
            lblOperation = new Label();
            cbOperation = new ComboBox();
            tbValueA = new TextBox();
            lblValueA = new Label();
            tbValueB = new TextBox();
            lblValueB = new Label();
            btnClose = new Button();

            this.Size = new System.Drawing.Size(380, 125);
            this.MinimumSize = new System.Drawing.Size(370, 125);
            this.SuspendLayout();

            // lblProperty
            lblProperty.Text = "Property";
            lblProperty.Name = "lblProperty";
            lblProperty.Anchor = AnchorStyles.Left | AnchorStyles.Top;
            lblProperty.Size = new System.Drawing.Size(178, 20);

            // cbProperty
            cbProperty.Name = "cbProperty";
            cbProperty.Anchor = AnchorStyles.Left;
            cbProperty.Size = new System.Drawing.Size(178, 29);
            cbProperty.DropDownStyle = ComboBoxStyle.DropDownList;
            cbProperty.SelectedIndexChanged += new System.EventHandler(this.OnPropertyChanged);
            //cbProperty.DataSource = properties.Select(p => p.Name);

            // lblOperation
            lblOperation.Text = "Operation";
            lblOperation.Name = "lblOperation";
            lblProperty.Anchor = AnchorStyles.Right | AnchorStyles.Top;
            lblOperation.Size = new System.Drawing.Size(178, 20);

            // cbOperation
            cbOperation.Name = "cbOperation";
            cbOperation.Anchor = AnchorStyles.Right;
            cbOperation.Size = new System.Drawing.Size(178, 29);
            cbOperation.DropDownStyle = ComboBoxStyle.DropDownList;
            cbOperation.Enabled = false;
            cbOperation.SelectedIndexChanged += new System.EventHandler(this.OnOperationChanged);
            cbOperation.Text = "None";

            // lblValueA
            lblValueA.Text = "Value A";
            lblValueA.Name = "lblValueA";
            lblValueA.Anchor = AnchorStyles.Left;
            lblValueA.Size = new System.Drawing.Size(178, 20);

            // lblValueB
            lblValueB.Text = "Value B";
            lblValueB.Name = "lblValueB";
            lblValueB.Anchor = AnchorStyles.Right;
            lblValueB.Size = new System.Drawing.Size(178, 20);

            // tbValueA
            tbValueA.Name = "tbValueA";
            tbValueA.Anchor = AnchorStyles.Left | AnchorStyles.Bottom;
            tbValueA.Size = new System.Drawing.Size(178, 29);
            tbValueA.Enabled = false;

            // tbValueB
            tbValueB.Name = "tbValueB";
            tbValueB.Anchor = AnchorStyles.Right | AnchorStyles.Bottom;
            tbValueB.Size = new System.Drawing.Size(178, 29);
            tbValueB.Enabled = false;

            // btnClose
            btnClose.Image = Image.FromFile("Sources/closeButtonpng.bmp");
            btnClose.Anchor = AnchorStyles.Right | AnchorStyles.Bottom;
            btnClose.Size = new System.Drawing.Size(29, 29);
            btnClose.Click += new System.EventHandler(this.OnBtnCloseClick);

            this.Controls.Add(this.lblProperty);
            this.Controls.Add(this.lblOperation);
            this.Controls.Add(this.cbProperty);
            this.Controls.Add(this.cbOperation);
            this.Controls.Add(this.lblValueA);
            this.Controls.Add(this.lblValueB);
            this.Controls.Add(this.tbValueA);
            this.Controls.Add(this.tbValueB);
            this.Controls.Add(this.btnClose);

            this.Dock = DockStyle.Top;
            this.ResumeLayout(false);
            this.PerformLayout();

            this.BorderStyle = BorderStyle.FixedSingle;
        }

        #endregion

        private Label lblProperty;
        private ComboBox cbProperty;
        private Label lblOperation;
        private ComboBox cbOperation;
        private Label lblValueA;
        private TextBox tbValueA;
        private Label lblValueB;
        private TextBox tbValueB;
        private Button btnClose;
    }
}
