﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using Entities;

namespace Browser.Components
{
    public partial class ConditionContainer : FlowLayoutPanel
    {
        public string Property => cbProperty.Text;

        public string Operation => cbOperation.Text;

        public string ValueA => tbValueA.Text;

        public string ValueB => tbValueB.Text;

        public event EventHandler<EventArgs> PropertyChanged;

        private List<string> properties;

        private List<string> operations;

        private const string none = "None";

        public ConditionContainer(List<string> properties)
        {
            this.properties = new List<string>(properties);
            this.properties.Insert(0, none);
            InitializeComponent();
            cbProperty.Items.AddRange(this.properties.ToArray());
        }

        public ConditionContainer(Condition condition, List<string> properties, List<string> operations) : this(properties)
        {
            this.operations = new List<string>(operations);
            cbProperty.SelectedItem = condition.Property;
            SetUpCbOperation();
            cbOperation.SelectedItem = condition.Operation;

            tbValueA.Text = condition.ValueA;
            tbValueB.Text = condition.ValueB;
        }

        public ConditionContainer(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public void InitOperations(List<string> source)
        {
            operations = new List<string>(source);
        }

        private void OnBtnCloseClick(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void OnPropertyChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Property) && Property != none)
            {
                PropertyChanged?.Invoke(this, EventArgs.Empty);
                operations.Insert(0, none);
                ClearTextBoxes();
                DisableTestBoxes();
                SetUpCbOperation();
            }
        }

        private void SetUpCbOperation()
        {
            cbOperation.Enabled = true;
            cbOperation.Items.Clear();
            cbOperation.Items.AddRange(operations.ToArray());
        }

        private void DisableTestBoxes()
        {
            tbValueA.Enabled = false;
            tbValueB.Enabled = false;
        }

        private void ClearTextBoxes()
        {
            tbValueA.Clear();
            tbValueB.Clear();
        }

        private void OnOperationChanged(object sender, EventArgs e)
        {
            ClearTextBoxes();
            tbValueA.Enabled = true;
            tbValueB.Enabled = cbOperation.Text.Contains("Range");
        }
    }
}
