﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using Entities;

namespace Browser.Components
{
    public partial class DataGridViewConditions : DataGridView
    {
        public DataGridViewConditions()
        {
            InitializeComponent();
        }

        public DataGridViewConditions(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public void SetDataSource(List<Condition>? dataSource)
        {
            DataSource = null;
            DataSource = dataSource ?? DataSource;
        }
    }
}
