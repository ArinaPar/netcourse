﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using Entities;

namespace Browser.Components
{
    public partial class DataGridViewTestResults : DataGridView
    {
        public DataGridViewTestResults()
        {
            InitializeComponent();
        }

        public DataGridViewTestResults(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public void SetDataSource(List<TestResult<int>>? dataSource)
        {
            DataSource = null;
            DataSource = dataSource ?? DataSource;
        }
    }
}
