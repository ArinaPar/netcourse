﻿namespace Browser.Components
{
    partial class FilterListModule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();

            this.lbFilterList = new System.Windows.Forms.ListBox();
            this.btnApplyFilter = new System.Windows.Forms.Button();
            this.btnClearFilterList = new System.Windows.Forms.Button();
            this.btnSaveFilter = new System.Windows.Forms.Button();
            this.btnOpenFilter = new System.Windows.Forms.Button();

            this.Name = "gbFilterList";
            this.Size = new System.Drawing.Size(599, 205);
            this.Text = "FilterList";

            this.Controls.Add(this.btnClearFilterList);
            this.Controls.Add(this.btnSaveFilter);
            this.Controls.Add(this.btnOpenFilter);
            this.Controls.Add(this.lbFilterList);
            this.Controls.Add(this.btnApplyFilter);
            // 
            // lbFilterList
            // 
            this.lbFilterList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbFilterList.FormattingEnabled = true;
            //this.lbFilterList.ItemHeight = 20;
            this.lbFilterList.Location = new System.Drawing.Point(10, 26);
            this.lbFilterList.Name = "lbFilterList";
            this.lbFilterList.Size = new System.Drawing.Size(450, 164);
            // 
            // btnApplyFilter
            // 
            this.btnApplyFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnApplyFilter.Location = new System.Drawing.Point(480, 26);
            this.btnApplyFilter.Name = "btnApplyFilter";
            this.btnApplyFilter.Size = new System.Drawing.Size(94, 29);
            this.btnApplyFilter.Text = "Apply";
            this.btnApplyFilter.UseVisualStyleBackColor = true;
            this.btnApplyFilter.Click += new System.EventHandler(this.OnBtnApplyFilterClick);
            // 
            // btnClearFilterList
            // 
            this.btnClearFilterList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClearFilterList.Location = new System.Drawing.Point(480, 165);
            this.btnClearFilterList.Name = "btnClearFilterList";
            this.btnClearFilterList.Size = new System.Drawing.Size(94, 29);
            this.btnClearFilterList.Text = "Clear";
            this.btnClearFilterList.UseVisualStyleBackColor = true;
            this.btnClearFilterList.Click += new System.EventHandler(this.OnBtnClearFilterListClick);
            // 
            // btnSaveFilter
            // 
            this.btnSaveFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSaveFilter.Location = new System.Drawing.Point(480, 118);
            this.btnSaveFilter.Name = "btnSaveFilter";
            this.btnSaveFilter.Size = new System.Drawing.Size(94, 29);
            this.btnSaveFilter.Text = "Save";
            this.btnSaveFilter.UseVisualStyleBackColor = true;
            this.btnSaveFilter.Click += new System.EventHandler(this.OnBtnSaveFilterClick);
            // 
            // btnOpenFilter
            // 
            this.btnOpenFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOpenFilter.Location = new System.Drawing.Point(480, 72);
            this.btnOpenFilter.Name = "btnOpenFilter";
            this.btnOpenFilter.Size = new System.Drawing.Size(94, 29);
            this.btnOpenFilter.Text = "Open";
            this.btnOpenFilter.UseVisualStyleBackColor = true;
            this.btnOpenFilter.Click += new System.EventHandler(this.OnBtnOpenFilterClick);

        }

        #endregion

        private System.Windows.Forms.ListBox lbFilterList;
        private System.Windows.Forms.Button btnApplyFilter;
        private System.Windows.Forms.Button btnClearFilterList;
        private System.Windows.Forms.Button btnSaveFilter;
        private System.Windows.Forms.Button btnOpenFilter;
    }
}
