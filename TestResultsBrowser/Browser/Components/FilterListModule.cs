﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Browser.Helpers;
using Browser.UserEventArgs;
using Entities;

namespace Browser.Components
{
    public partial class FilterListModule : GroupBox
    {
        internal event EventHandler<ConditionListAddedEventArgs> BtnApplyFilterClick;

        private readonly List<ConditionList> conditionLists;
        public FilterListModule()
        {
            conditionLists = new List<ConditionList>();
            InitializeComponent();
        }

        public FilterListModule(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public void Add(ConditionList? conditionList)
        {
            if (conditionList is not null)
            {
                conditionLists.Add(conditionList);
                SetDataSource(conditionLists);
            }
        }

        public void SaveFilter()
        {
            btnSaveFilter.PerformClick();
        }

        public void OpenFilter()
        {
            btnOpenFilter.PerformClick();
        }

        private void SetDataSource(List<ConditionList> conditionList)
        {
            lbFilterList.DataSource = null;
            lbFilterList.DataSource = conditionList.Select(c => c.Name).ToList();
        }

        private void OnBtnOpenFilterClick(object sender, EventArgs e)
        {
            const string datFileFormat = "DAT file|*.DAT";

            if (DialogHelper.TryOpenFileDialog(datFileFormat, out string fileName))
            {
                if (SerializationHelper.TryDeserializeBinaryFormat(fileName, out ConditionList? conditionList))
                {
                    conditionLists.Add(conditionList);
                    lbFilterList.DataSource = null;
                    lbFilterList.DataSource = conditionLists.Select(c => c.Name).ToList();
                }
                else
                {
                    DialogHelper.ShowMessage("Unable to convert file content to filter format");
                }
            }
        }

        private void OnBtnClearFilterListClick(object sender, EventArgs e)
        {
            conditionLists.Clear();
            lbFilterList.DataSource = null;
            lbFilterList.DataSource = conditionLists;
        }

        private void OnBtnApplyFilterClick(object sender, EventArgs e)
        {
            var selected = lbFilterList.SelectedItem;
            if (selected is not null)
            {
                var conditionListName = lbFilterList.SelectedItem.ToString();
                var conditionList = FindConditionList(conditionListName);
                var conditionListAddedEventArgs = new ConditionListAddedEventArgs
                {
                    ConditionList = conditionList
                };

                BtnApplyFilterClick?.Invoke(this, conditionListAddedEventArgs);
            }
            else
            {
                DialogHelper.ShowMessage("Filter is not selected");
            }
        }

        private void OnBtnSaveFilterClick(object sender, EventArgs e)
        {
            var selected = lbFilterList.SelectedItem;
            if (selected is not null)
            {
                var conditionListName = selected.ToString();
                var conditionList = FindConditionList(conditionListName);
                SerializationHelper.MakeBinarySerialization(conditionList);
            }
            else
            {
                DialogHelper.ShowMessage("Filter is not selected");
            }
        }

        private ConditionList FindConditionList(string? name)
        {
            return conditionLists.First(c => c.Name == name);
        }
    }
}
