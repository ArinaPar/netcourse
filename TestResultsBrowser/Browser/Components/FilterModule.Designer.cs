﻿namespace Browser.Components
{
    partial class FilterModule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            
            this.Name = "gbFilter";
            this.Size = new System.Drawing.Size(438, 438);
            this.Text = "Filter";

            this.pnlConditions = new System.Windows.Forms.Panel();
            this.btnClearFilter = new System.Windows.Forms.Button();
            this.btnAddCondition = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.btnAddToFilterList = new System.Windows.Forms.Button();

            this.Controls.Add(this.pnlConditions);
            this.Controls.Add(this.btnClearFilter);
            this.Controls.Add(this.btnAddCondition);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.btnAddToFilterList);

            // 
            // pnlConditions
            // 
            this.pnlConditions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlConditions.AutoScroll = true;
            this.pnlConditions.Location = new System.Drawing.Point(6, 61);
            this.pnlConditions.Name = "pnlConditions";
            this.pnlConditions.Size = new System.Drawing.Size(426, 336);
            // 
            // btnClearFilter
            // 
            this.btnClearFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClearFilter.Location = new System.Drawing.Point(298, 26);
            this.btnClearFilter.Name = "btnClearFilter";
            this.btnClearFilter.Size = new System.Drawing.Size(134, 29);
            this.btnClearFilter.Text = "Clear";
            this.btnClearFilter.UseVisualStyleBackColor = true;
            this.btnClearFilter.Click += new System.EventHandler(this.OnBtnClearFilterClick);
            // 
            // btnAddCondition
            // 
            this.btnAddCondition.Location = new System.Drawing.Point(6, 26);
            this.btnAddCondition.Name = "btnAddCondition";
            this.btnAddCondition.Size = new System.Drawing.Size(134, 29);
            this.btnAddCondition.TabIndex = 9;
            this.btnAddCondition.Text = "Add";
            this.btnAddCondition.UseVisualStyleBackColor = true;
            this.btnAddCondition.Click += new System.EventHandler(this.OnBtnAddConditionClick);
            // 
            // btnApply
            // 
            this.btnApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnApply.Location = new System.Drawing.Point(6, 403);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(134, 29);
            this.btnApply.TabIndex = 9;
            this.btnApply.Text = "Apply";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.OnBtnApplyClick);
            // 
            // btnAddToFilterList
            // 
            this.btnAddToFilterList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddToFilterList.Location = new System.Drawing.Point(298, 403);
            this.btnAddToFilterList.Name = "btnAddToFilterList";
            this.btnAddToFilterList.Size = new System.Drawing.Size(134, 29);
            this.btnAddToFilterList.TabIndex = 3;
            this.btnAddToFilterList.Text = "Add to Filter List";
            this.btnAddToFilterList.UseVisualStyleBackColor = true;
            this.btnAddToFilterList.Click += new System.EventHandler(this.OnBtnAddToFilterListClick);
        }

        #endregion

        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Button btnAddToFilterList;
        private System.Windows.Forms.Button btnClearFilter;
        private System.Windows.Forms.Button btnAddCondition;
        private System.Windows.Forms.Panel pnlConditions;

    }
}
