﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Browser.Helpers;
using Browser.UserEventArgs;
using Entities;
using GenericFilter;

namespace Browser.Components
{
    internal partial class FilterModule : GroupBox
    {
        public event EventHandler<BtnApplyClickEventArgs> BtnApplyClick;
        public event EventHandler<ConditionListAddedEventArgs> BtnAddToFilterListClick;

        private readonly PropertyInfo[] properties;
        private readonly MethodInfo[] operations;

        public List<Condition> Conditions => ReadConditions();

        private const string none = "None";

        public FilterModule()
        {
            properties = typeof(TestResult<int>).GetProperties();
            operations = typeof(Filter).GetMethods().Where(o => o.Name.Contains("Condition")).ToArray();

            InitializeComponent();
        }

        public FilterModule(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        internal void OnPropertyChanged(object sender, EventArgs e)
        {
            var conditionContainer = (ConditionContainer)sender;
            if (CheckIfValid(conditionContainer.Property))
            {
                var operationNames = GetOperations(conditionContainer.Property);
                conditionContainer.InitOperations(operationNames);
            }
        }

        internal void OnBtnAddConditionClick(object sender, EventArgs e)
        {
            var conditionContainer = new ConditionContainer(properties.Select(p => p.Name).ToList());
            conditionContainer.PropertyChanged += OnPropertyChanged;
            pnlConditions.Controls.Add(conditionContainer);
        }

        internal void FillPnlConditions(ConditionList? conditionList)
        {
            pnlConditions.Controls.Clear();
            if (conditionList != null)
            {
                foreach (var condition in conditionList.Conditions)
                {
                    var conditionContainer = new ConditionContainer(condition, properties.Select(p => p.Name).ToList(), GetOperations(condition.Property));
                    conditionContainer.PropertyChanged += OnPropertyChanged;
                    pnlConditions.Controls.Add(conditionContainer);
                }
            }
        }

        internal List<string> GetOperations(string propertyName)
        {
            var operationNames = operations.Select(o => o.Name).ToList();
            var property = properties.First(p => p.Name == propertyName);
            if (property.PropertyType != typeof(string))
            {
                operationNames.RemoveAll(n => n.Contains("Contains"));
            }

            return operationNames;
        }

        internal void OnBtnClearFilterClick(object sender, EventArgs e)
        {
            pnlConditions.Controls.Clear();
        }

        private void OnBtnApplyClick(object sender, EventArgs e)
        {
            HandleApply();
        }

        private List<Condition> ReadConditions()
        {
            var conditions = new List<Condition>();

            foreach (ConditionContainer conditionContainer in pnlConditions.Controls)
            {
                if (CheckIfFilled(conditionContainer))
                {
                    conditions.Add(new Condition(conditionContainer.Property, conditionContainer.Operation, conditionContainer.ValueA, conditionContainer.ValueB));
                }
                else
                {
                    DialogHelper.ShowMessage("Fill all the enable fields");
                    break;
                }
            }

            return conditions;
        }

        private void HandleApply()
        {
            var filter = new Filter(typeof(TestResult<int>));
            var btnApplyFilterClickEventArgs = new BtnApplyClickEventArgs
            {
                Filter = filter
            };

            if (pnlConditions.Controls.Count > 0)
            {
                foreach (Condition condition in ReadConditions())
                {
                    PropertyInfo property = properties.First(p => p.Name == condition.Property);
                    Type propertyType = property.PropertyType;
                    object? valueA;
                    object? valueB = default;

                    if (TryConvert(condition.ValueA, propertyType, out valueA) && (!condition.Operation.Contains("Range") || TryConvert(condition.ValueB, propertyType, out valueB)))
                    {
                        var operation = operations.First(o => o.Name == condition.Operation);
                        if (operation.IsGenericMethod)
                        {
                            operation = operation.MakeGenericMethod(propertyType);
                        }

                        operation.Invoke(filter, valueB is null ? new[] { property.Name, valueA } : new[] { property.Name, valueA, valueB });
                    }
                    else
                    {
                        DialogHelper.ShowMessage("Invalid input format");
                        break;
                    }
                }
            }

            BtnApplyClick?.Invoke(this, btnApplyFilterClickEventArgs);
        }

        private void OnBtnAddToFilterListClick(object sender, EventArgs e)
            {
                if (DialogHelper.TryNameTheFilter(out string? filterName))
                {
                    var btnAddToFilterListClickEventArgs = new ConditionListAddedEventArgs();
                    btnAddToFilterListClickEventArgs.ConditionList = new ConditionList(filterName, Conditions.ToArray());
                    BtnAddToFilterListClick?.Invoke(this, btnAddToFilterListClickEventArgs);
                }
            }

        private bool TryConvert(string value, Type propertyType, out object? convertedValueA)
        {
            try
            {
                convertedValueA = Convert.ChangeType(value, propertyType);
            }
            catch (FormatException)
            {
                convertedValueA = null;
                return false;
            }

            return true;
        }

        private bool CheckIfFilled(ConditionContainer conditionContainer)
        {
            return CheckIfValid(conditionContainer.Property)
                   && CheckIfValid(conditionContainer.Operation)
                   && !string.IsNullOrEmpty(conditionContainer.ValueA)
                   && (!conditionContainer.Operation.Contains("Range") || !string.IsNullOrEmpty(conditionContainer.ValueB));
        }

        private bool CheckIfValid(string value)
        {
            return !string.IsNullOrEmpty(value) && value != none;
        }
    }
}

