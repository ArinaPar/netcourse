﻿using System;
using System.Windows.Forms;
using Entities;

namespace Browser.Forms
{
    public partial class AddTestResultForm : Form
    {
        private const string emptyFieldMessage = "Please fill all the fields";

        public TestResult<int> TestResult { get; private set; }

        public AddTestResultForm()
        {
            InitializeComponent();
        }

        private void OnBtnCancelClick(object sender, EventArgs e)
        {
            Close();
        }

        private void OnBtnAddClick(object sender, EventArgs e)
        {
            if (CheckIfNotEmpty(tbStudentName, out string? studentName)
                && CheckIfNotEmpty(tbStudentSurname, out string? studentSurname)
                && CheckIfNotEmpty(tbTestName, out string? testName)
                && CheckIfNotEmpty(cbMark, out int mark))
            {
                var testDate = dtpTestDate.Value;
                TestResult = new TestResult<int>(studentName, studentSurname, testName, testDate, mark);
                DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show(emptyFieldMessage);
            }
        }

        private bool CheckIfNotEmpty(TextBox textBox, out string? buf)
        {
            if (textBox.Text == string.Empty)
            {
                buf = null;
                textBox.Focus();
                return false;
            }

            buf = textBox.Text;
            return true;
        }

        private bool CheckIfNotEmpty(ComboBox comboBox, out int buf)
        {
            if (comboBox.SelectedItem == null)
            {
                buf = 0;
                comboBox.Focus();
                return false;
            }

            buf = Convert.ToInt32(comboBox.SelectedItem);
            return true;
        }
    }
}
