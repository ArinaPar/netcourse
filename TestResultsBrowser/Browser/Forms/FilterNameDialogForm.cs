﻿using System;
using System.Windows.Forms;

namespace Browser.Forms
{
    public partial class FilterNameDialogForm : Form
    {
        public string FilterName { get; private set; }
        public FilterNameDialogForm()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            FilterName = tbFilterName.Text;
            DialogResult = DialogResult.OK;
        }
    }
}
