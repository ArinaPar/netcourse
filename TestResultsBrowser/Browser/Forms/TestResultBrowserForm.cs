﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Serialization;
using BinarySearchTreeProcessor;
using Browser.Helpers;
using Browser.UserEventArgs;
using Entities;
using GenericFilter;

namespace Browser.Forms
{
    public partial class TestResultsBrowser : Form
    {
        private IterativeTree<TestResult<int>> iterativeTree;
        private List<TestResult<int>> testResults;
        private string? currentFileName;
        private bool hasUnsavedData;


        public TestResultsBrowser()
        {
            iterativeTree = new IterativeTree<TestResult<int>>();
            testResults = new List<TestResult<int>>();
            
            hasUnsavedData = false;
            InitializeComponent();
        }

        private void OnTestResultsBrowserLoad(object sender, EventArgs e)
        {
            dgvTestResults.SetDataSource(testResults);
        }

        private void OnBtnApplyClick(object sender, BtnApplyClickEventArgs e)
        {
            var newDataSource = e.Filter?.Apply(testResults).ToList();
            dgvTestResults.SetDataSource(newDataSource);
            dgvFilter.SetDataSource(filterModule.Conditions);
        }

        private void OnBtnApplyFilterClick(object sender, ConditionListAddedEventArgs e)
        {
            filterModule.FillPnlConditions(e.ConditionList);
        }

        private void OnBtnAddToFilterListClick(object sender, ConditionListAddedEventArgs e)
        {
            filterListModule.Add(e.ConditionList);
        }

        private void OnOpenFileToolStripMenuItemClick(object sender, EventArgs e)
        {
            const string xmlFileFormat = "XML file|*.XML";

            if (DialogHelper.TryOpenFileDialog(xmlFileFormat, out string fileName))
            {
                FillDgvTestResults(fileName);
                currentFileName = fileName;
            }
        }

        private void FillDgvTestResults(string xmlFileName)
        {
            var formatter = new XmlSerializer(typeof(IterativeTree<TestResult<int>>));
            try
            {
                iterativeTree = SerializationHelper.MakeXmlDeserialization<TestResult<int>>(xmlFileName);
                RefreshDgvTestResultsDataSource();
            }
            catch (InvalidCastException)
            {
                DialogHelper.ShowMessage("Unable to convert file content to test results list");
            }
        }
        
        private void RefreshDgvTestResultsDataSource()
        {
            testResults = iterativeTree.ToList();
            dgvTestResults.SetDataSource(testResults);
        }

        private void OnAddTestResultToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (DialogHelper.TryCreateTestResult(out TestResult<int>? testResult))
            {
                iterativeTree.Add(testResult);
                hasUnsavedData = true;
            }

            RefreshDgvTestResultsDataSource();
        }

        private void WriteIterativeTreeToFile(string xmlFileName)
        {
            SerializationHelper.MakeXmlSerialization(iterativeTree, xmlFileName);
            hasUnsavedData = false;
        }

        private void OnSaveFileToolStripMenuItemClick(object sender, EventArgs e)
        {
            SaveIterativeTree();
        }

        private void SaveIterativeTree()
        {
            if (currentFileName == null)
            {
                SaveIterativeTreeAs();
            }
            else
            {
                WriteIterativeTreeToFile(currentFileName);
            }
        }

        private void SaveIterativeTreeAs()
        {
            if (DialogHelper.TrySaveFileDialog(out string? fileName))
            {
                SerializationHelper.MakeXmlSerialization(iterativeTree, fileName);
                currentFileName = fileName;
                hasUnsavedData = false;
            }
        }

        private void OnSaveAsToolStripMenuItemClick(object sender, EventArgs e)
        {
            SaveIterativeTreeAs();
        }

        private void OnExitToolStripMenuItemClick(object sender, EventArgs e)
        {
            Close();
        }

        private void AskToSaveBeforeExit()
        {
            if (hasUnsavedData && DialogHelper.AskIfNeedToSave())
            {
                SaveIterativeTree();
            }
        }

        private void OnTestResultsBrowserFormClosing(object sender, FormClosingEventArgs e)
        {
            AskToSaveBeforeExit();
        }

        private void OnOpenFilterToolStripMenuItemClick(object sender, EventArgs e)
        {
            filterListModule.OpenFilter();
        }

        private void OnSaveFilterToolStripMenuItemClick(object sender, EventArgs e)
        {
            filterListModule.SaveFilter();
        }
    }
}
