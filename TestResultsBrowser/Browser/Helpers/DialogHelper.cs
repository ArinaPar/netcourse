﻿using System;
using System.Windows.Forms;
using Browser.Forms;
using Entities;

namespace Browser.Helpers
{
    internal static class DialogHelper
    {
        internal static void ShowMessage(string message)
        {
            MessageBox.Show(message);
        }

        internal static bool AskIfNeedToSave()
        {
            const string message = "Do you want to save changes before exit?";
            const string caption = "Form Closing";
            return MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes;
        }

        internal static bool TryOpenFileDialog(string fileFormat, out string fileName)
        {
            var openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = fileFormat;
            openFileDialog.InitialDirectory = Environment.CurrentDirectory;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                fileName = openFileDialog.FileName;
                return true;
            }
            else
            {
                fileName = null;
                return false;
            }
        }

        internal static bool TryCreateTestResult(out TestResult<int>? testResult)
        {
            var addTestResultForm = new AddTestResultForm();
            if (addTestResultForm.ShowDialog() == DialogResult.OK)
            {
                testResult = addTestResultForm.TestResult;
                return true;
            }
            else
            {
                testResult = null;
                return false;
            }
        }

        internal static bool TrySaveFileDialog(out string? fileName)
        {
            var saveFileDialog = new SaveFileDialog();
            const string xmlFileFormat = "XML file|*.XML";
            saveFileDialog.Filter = xmlFileFormat;
            saveFileDialog.FilterIndex = 2;
            saveFileDialog.RestoreDirectory = true;

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                fileName = saveFileDialog.FileName;
                return true;
            }
            else
            {
                fileName = null;
                return false;
            }
        }

        internal static bool TryNameTheFilter(out string? filterName)
        {
            var filterNameDialog = new FilterNameDialogForm();
            if (filterNameDialog.ShowDialog() == DialogResult.OK)
            {
                filterName = filterNameDialog.FilterName;
                return true;
            }
            else
            {
                filterName = null;
                return false;
            }
        }
    }
}
