﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using BinarySearchTreeProcessor;
using Entities;

namespace Browser.Helpers
{
    internal static class SerializationHelper
    {
        internal static void MakeXmlSerialization<T>(BinarySearchTree<T> tree, string filePath) where T : IComparable<T>
        {
            var formatter = new XmlSerializer(typeof(IterativeTree<TestResult<int>>));
            using var fs = new FileStream(filePath, FileMode.OpenOrCreate);
            formatter.Serialize(fs, tree);
        }

        internal static void MakeBinarySerialization(ConditionList conditionList)
        {
            var formatter = new BinaryFormatter();
            using var fs = new FileStream($"{conditionList.Name}.dat", FileMode.OpenOrCreate);
            formatter.Serialize(fs, conditionList);
        }

        internal static IterativeTree<T> MakeXmlDeserialization<T>( string filePath) where T : IComparable<T>
        {
            var formatter = new XmlSerializer(typeof(IterativeTree<TestResult<int>>));
            using var fs = new FileStream(filePath, FileMode.OpenOrCreate);
            return (IterativeTree<T>)formatter.Deserialize(fs);
        }

        internal static bool TryDeserializeBinaryFormat(string fileName, out ConditionList? conditionList)
        {
            var binaryFormatter = new BinaryFormatter();
            {
                try
                {
                    using var fs = new FileStream(fileName, FileMode.Open);
                    conditionList = (ConditionList)binaryFormatter.Deserialize(fs);
                    return true;
                }
                catch (InvalidCastException)
                {
                    conditionList = null;
                    return false;
                }
            }
        }
    }
}
