﻿using GenericFilter;

namespace Browser.UserEventArgs
{
    internal class BtnApplyClickEventArgs : System.EventArgs
    {
        internal Filter? Filter { get; set; }
    }
}
