﻿using Entities;

namespace Browser.UserEventArgs
{
    internal class ConditionListAddedEventArgs
    {
        internal ConditionList? ConditionList { get; set; }
    }
}
