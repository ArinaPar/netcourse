The WinForm application for viewing, filtering and sorting student test results.
A test result is a type that includes information about the student, the test and the mark.
The main form contains:
- menu;
- table for displaying test results;
- filter list display table;
- table of applied filters.

The student test results are stored in a file. User needs to click "File" -> "Open" to read them from file.
The user can add test result by selecting "Add" -> "TestResult" in the menu.
To save changes, the user needs to select "File" -> "Save" or "File" -> "Save As...".

The user must add filters to filter the test results. Click the "Add" button of the filter list module and set the filter up.
Then apply it by clicking the "Apply" button of the filter list module.
The filter list can be saved to be reused later. Click the the "Add" button, and than "Save" button of the filter list module.
The user must open filter list from file by pressing button "Open" of the filter list module.
Then the names of the filter lists will appear in the filter list display table. 
The user can deploy one by clicking the "Apply" button of the filter list module. Then it can be applied and changed.